// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var secondSubcategorySchema = mongoose.Schema({
    categoryId :String,
    subcategoryId : String,
    SecondsubcategoryName :String,
    status : String
    
});


// create the model for users and expose it to our app
module.exports = mongoose.model('SecondSubCategory', secondSubcategorySchema);
