// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var supplierSchema = mongoose.Schema({
     custId : String,
     supName        : String,
     role     : String,
     address        : String,
     country     : String,
     state  : String,
     city   : String,
     pincode     : String,
     phone     : String,
     mobile     : String,
     fax     : String,
     email : String,
     website : String,
     // bankDetail     : String,
     bankName : String,
     holderName : String,
     accountNumber : String,
     ifscCode : String,
     panNo   : String,
     registerType     : String,
     gsit   :String,
     registerSet : String,
     supplier_status : String,
     datetime     : String
 });


// create the model for users and expose it to our app
module.exports = mongoose.model('Supplier', supplierSchema);
