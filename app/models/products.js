// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var productSchema = mongoose.Schema({

     // procatName        : String,
     // prosubcatName     : String,
     // prosecsubcatName        : String,
     // proparentdata     : String,
     proCattId : String,
     partId  : String,
     partName   : String,
     // avilability     : String,
     // quantity     : String,
     shippricemp : String,
     shippriceoutmp : String,
     dimension : String,
     weight : String,
     code     : String,
     mrp     : String,
     price : String,
     stkQty : String,
     tax     : String,
     total   : String,
     brand     : String,
     umState   : String,
     unitMeasure : String,
     altset : String,
     firstSet : String,
     // firstSetVal : String,
     secondSet : String,
     // secondSetVal : String,
     productdescr   :String,
     linkforedit: String,
     stockLocationOne : String,
     stockLocationTwo : String,
     stockLocationThree : String,
     datetime     : String,
     salecostprice : String,
     setPriority : String,
     quantity: String,
     myimage: [{
             path: {
              type: String,
                },
             originalname: {
             type: String
             }
      }],
     // myimage: {
     //         path: {
     //          type: String,
     //            },
     //         originalname: {
     //         type: String
     //         }
     //  },
      addProRole : String,
      productStatus : String,
      salesperprice : String,
      viewStatus : String
     
});


// create the model for users and expose it to our app
module.exports = mongoose.model('Products', productSchema);
