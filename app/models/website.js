// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var websiteSchema = mongoose.Schema({
    webName : String,
    webEmail  :  String,
    webPhone : String ,
    webAddress : String ,
    datetime : String,
    user_role    : String,
    img              : {

        path: {
         type: String,
        },
         originalname: {
         type: String,
        }

    }

});


// create the model for users and expose it to our app
module.exports = mongoose.model('Website', websiteSchema);
