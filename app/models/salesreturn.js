var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var salesreturnSchema = mongoose.Schema({

     supId : String,
     GrandTotalAmnt  : String ,
     SgstTotalAmnt: String,
     CgstTotalAmnt: String,
     IgstTotalAmnt: String,
     FinalTotalAmnt: String,
     TotalDecimalAmnt : String,
     EditAmount :  String,
     creditNo : String,
     voucherNo : String,
     creditDate : String,
     orderId : String,
     salesStatus : String,
     datetime : String

 });

module.exports = mongoose.model('Salesreturn', salesreturnSchema);

