var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var purchasereturnSchema = mongoose.Schema({

     supId : String,
     GrandTotalAmnt : String ,
     SgstTotalAmnt: String,
     CgstTotalAmnt: String,
     IgstTotalAmnt: String,
     FinalTotalAmnt: String,
     TotalDecimalAmnt : String,
     EditAmount : String,
     debitNo : String,
     voucherNo : String,
     debitDate : String,
     orderId : String,
     purchaseReturnStatus : String,
     datetime : String
 });


module.exports = mongoose.model('Purchasereturn', purchasereturnSchema);

