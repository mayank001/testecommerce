var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var salesItemSchema = mongoose.Schema({

     salesId : String, 
     partId : String,
     SellPrice : String,
     TotalAmnt : String,
     Tax : String,
     Discount : String,
     Quantity : String,
     Price  : String,
     Per : String,
     DiscountAmnt : String,
     Sgst : String,
     Cgst: String,
     Igst: String,
     TotalAmntData : String,
     finalqtydata : String,
     part : String,
     voucherNo : String,
     userId : String,
     salesperprice : String,
     salesStatus : String,
     datetime : String

 });

module.exports = mongoose.model('SalesItem', salesItemSchema);
