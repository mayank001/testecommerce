// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var productCategorySchema = mongoose.Schema({

     procatName        : String,
     prosubcatName     : String,
     prosecsubcatName  : String,
     proparentdata     : String,
     proparentdata1 : String,
     proparentdata2 : String,
     partId  : String,
     datetime     : String,
});


// create the model for users and expose it to our app
module.exports = mongoose.model('ProductCategory', productCategorySchema);
