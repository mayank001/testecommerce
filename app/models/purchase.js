var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var purchaseSchema = mongoose.Schema({

     supId : String,
     GrandTotalAmnt  : String,
     SgstTotalAmnt : String,
     CgstTotalAmnt : String,
     IgstTotalAmnt : String,
     FinalTotalAmnt : String,
     TotalDecimalAmnt : String,
     EditAmount :  String,
     invoiceNo : String,
     voucherNo : String,
     SupDate : String,
     orderId : String,
     purchaseStatus : String,
     datetime     : String

 });

module.exports = mongoose.model('Purchase', purchaseSchema);
