// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var thirdSubcategorySchema = mongoose.Schema({
    // categoryId :String,
    // subcategoryId : String,
    // secondsubcategoryId :String,
    // ThirdsubcategoryName :String,
    catNamedata :String,
    subcatNamedata :String,
    secondsubcatNamedata :String,
    parentdata : String,
    role : String,
    CategoryName : String,
    level : String,
    status : String
    
});


// create the model for users and expose it to our app
module.exports = mongoose.model('ThirdSubCategory', thirdSubcategorySchema);
