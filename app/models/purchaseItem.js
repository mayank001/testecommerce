// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var purchaseItemSchema = mongoose.Schema({

     purchaseId : String, 
     partId   : String,
     SellPrice : String,
     TotalAmnt : String,
     Tax     : String,
     Discount : String,
     Quantity  : String,
     Price   : String,
     Per     : String,
     DiscountAmnt : String,
     Sgst : String,
     Cgst: String,
     Igst: String,
     TotalAmntData : String,
     salecostprice : String,
     part : String,
     voucherNo : String,
     salecostprice : String,
     finalqtydata : String,
     purchaseStatus : String,
     supId : String,
     datetime     : String
 });


// create the model for users and expose it to our app
module.exports = mongoose.model('PurchaseItem', purchaseItemSchema);
