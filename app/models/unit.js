// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var unitSchema = mongoose.Schema({

    unitType :String,
    symbol : String,
    formal_name : String,
    code : String,
    decimal_place : String,
    status : String
    
});


// create the model for users and expose it to our app
module.exports = mongoose.model('Unit', unitSchema);
