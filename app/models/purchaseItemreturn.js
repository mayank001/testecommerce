var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var purchaseItemreturnSchema = mongoose.Schema({

     purchaseId : String,
     partId : String,
     SellPrice : String,
     TotalAmnt : String,
     Tax : String,
     Discount : String,
     Quantity : String,
     Price : String,
     Per : String,
     DiscountAmnt : String,
     Sgst : String,
     Cgst : String,
     Igst : String,
     TotalAmntData : String,
     salecostprice : String,
     part : String,
     voucherNo : String,
     salecostprice : String,
     finalqtydata : String,
     purchaseReturnStatus : String,
     supId : String,
     datetime : String

 });

module.exports = mongoose.model('PurchaseItemreturn', purchaseItemreturnSchema);
