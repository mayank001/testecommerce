var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var salesItemreturnSchema = mongoose.Schema({

     salesId : String,
     partId : String,
     SellPrice : String,
     TotalAmnt : String,
     Tax : String,
     Discount : String,
     Quantity : String,
     ReturnQuantity : String,
     Price : String,
     Per : String,
     DiscountAmnt : String,
     Sgst : String,
     Cgst : String,
     Igst : String,
     TotalAmntData : String,
     finalqtydata : String,
     part : String,
     voucherNo : String,
     userId : String,
     salesStatus : String,
     datetime : String
     
 });

module.exports = mongoose.model('SalesItemreturn', salesItemreturnSchema);