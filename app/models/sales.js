// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var salesSchema = mongoose.Schema({

     supId : String,
     GrandTotalAmnt  : String ,
     SgstTotalAmnt: String,
     CgstTotalAmnt: String,
     IgstTotalAmnt: String,
     FinalTotalAmnt     : String,
     TotalDecimalAmnt : String,
     EditAmount :  String,
     invoiceNo : String,
     voucherNo : String,
     SupDate : String,
     orderId : String,
     salesStatus : String,
     datetime : String
 });


// create the model for users and expose it to our app
module.exports = mongoose.model('Sales', salesSchema);