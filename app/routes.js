module.exports = function(app, passport) {

// normal routes ===============================================================

  // show the home page (will also have our login links)
  app.get('/', function(req, res) {
    var i = 0;
       var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.product = null;
        vm.allproduct = [];
        vm.slider = null;   
        vm.allslider = [];
        vm.category = null;
        vm.allcategory = [];
        vm.thirdsubcategory = null;
        vm.allthirdsubcategory = [];
        vm.user = null;
        vm.alluser = [];

     var Website       = require('../app/models/website');

      var Slider       = require('../app/models/slider');

      var Category       = require('../app/models/category');

      var thirdSubCategory       = require('../app/models/thirdsubcategory');

      var User       = require('../app/models/user');


       var Product       = require('../app/models/products');

        Product.find(function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

       var productvalueproduct = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvalueproduct); 

      Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

        thirdSubCategory.find(function (err, thirdsubcategory) {

     for(var i = 0; i < thirdsubcategory.length; i++){
            vm.allthirdsubcategory[i] = thirdsubcategory[i];

        }

       var productvaluethirdsubcategory = JSON.stringify(thirdsubcategory);
       var jsonObjectthirdsubcategory = JSON.parse(productvaluethirdsubcategory); 



     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

        User.find(function (err, user) {

     for(var i = 0; i < user.length; i++){
            vm.alluser[i] = user[i];

        }

       var productvalueuser = JSON.stringify(user);
       var jsonObjectuser = JSON.parse(productvalueuser); 

       Slider.find(function (err, slider) {

     for(var i = 0; i < slider.length; i++){
            vm.allslider[i] = slider[i];

        }

       var productvalueslider = JSON.stringify(slider);
       var jsonObjectslider = JSON.parse(productvalueslider);   

    res.render('index.ejs',{ datauser : jsonObjectuser , message: req.flash('loginMessage'), loadthirdsubcategory : jsonObjectthirdsubcategory, newfinalvalcoup : "0" , loadwebsitedata : jsonObjectwebsite , loadsliderdata : jsonObjectslider , loadcategorydata : jsonObjectcategory ,  loadproductdata : jsonObjectproduct});
    
              });

            });

          });

        });

      });   

    });  

  });  


  app.get('/index4', function(req, res) {
   var i = 0;
    var vm = this;
        vm.product = null;
        vm.allproduct = [];
        vm.thirdsubcategory = null;
        vm.allthirdsubcategory = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
         vm.secondSubCategory = null;
        vm.allsecondSubCategorys = [];

        var Product       = require('../app/models/products');
        var thirdSubCategory       = require('../app/models/thirdsubcategory');
        var Category       = require('../app/models/category');
        var Subcategory       = require('../app/models/subcategory');
        var secondSubCategory       = require('../app/models/secondsubcategory');


        Product.find(function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

       var productvalueproduct = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvalueproduct); 

       Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

        Subcategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 

     secondSubCategory.find(function (err, secondSubCategorys) {
     // docs is an array
       for(var i = 0; i < secondSubCategorys.length; i++){
            vm.allsecondSubCategorys[i] = secondSubCategorys[i];
       }
        var secondSubCategoryvalue = JSON.stringify(secondSubCategorys);
        var jsonObjectsecondSubCategorys = JSON.parse(secondSubCategoryvalue);



        thirdSubCategory.find(function (err, thirdsubcategory) {

     for(var i = 0; i < thirdsubcategory.length; i++){
            vm.allthirdsubcategory[i] = thirdsubcategory[i];

        }

       var productvaluethirdsubcategory = JSON.stringify(thirdsubcategory);
       var jsonObjectthirdsubcategory = JSON.parse(productvaluethirdsubcategory); 


      res.render('index4.ejs' , {secondsubcategorydata : jsonObjectsecondSubCategorys , loadsubcategorydata  : jsonObjectsubcategory ,  loadcategorydata : jsonObjectcategory , loadthirdsubcategory : jsonObjectthirdsubcategory, loadproductdata : jsonObjectproduct});

            });

          });      

        });

      });

    }); 

  });  


  app.get('/checkSalesSupplierState', function (req, res) { 

    var User = require('../app/models/user');
    var _id = req.query.supId;

      User.findOne({"_id" : req.query.supId}, function (err, users) {

        if(users){
            
          res.send(users);
              
        } else {

        }

      });  

  });


  app.get('/home',isLoggedIn, function(req, res) {

    var i = 0;
      var vm = this;
        vm.product = null;
        vm.allproduct = [];
        vm.thirdsubcategory = null;
        vm.allthirdsubcategory = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.secondSubCategory = null;
        vm.allsecondSubCategorys = [];
        vm.website = null;
        vm.allwebsites = [];
        vm.productCategory = null;
        vm.allproductCategory = [];

        var Product       = require('../app/models/products');
        var ProductCategory   = require('../app/models/productCategory');
        var thirdSubCategory  = require('../app/models/thirdsubcategory');
        var Category       = require('../app/models/category');
        var Subcategory       = require('../app/models/subcategory');
        var secondSubCategory       = require('../app/models/secondsubcategory');
        var Website       = require('../app/models/website');

        Product.find(function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

       var productvalueproduct = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvalueproduct); 

         ProductCategory.find(function (err, productCategory) {

     for(var i = 0; i < productCategory.length; i++){
            vm.allproductCategory[i] = productCategory[i];

        }

       var productvalueproductCategory = JSON.stringify(productCategory);
       var jsonObjectproductCategory = JSON.parse(productvalueproductCategory); 

       Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);

          Category.find().sort({categoryName: 1}).exec(function(err, category) {      

        for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

      Subcategory.find().sort({subcategoryName: 1}).exec(function(err, subcategory) {      

        for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 

  
        secondSubCategory.find().sort({SecondsubcategoryName: 1}).exec(function(err, secondSubCategorys) {      

       for(var i = 0; i < secondSubCategorys.length; i++){
            vm.allsecondSubCategorys[i] = secondSubCategorys[i];
       }

      var secondSubCategoryvalue = JSON.stringify(secondSubCategorys);
      var jsonObjectsecondSubCategorys = JSON.parse(secondSubCategoryvalue);
    
      thirdSubCategory.find().sort({CategoryName: 1}).exec(function(err, thirdsubcategory) {      


     for(var i = 0; i < thirdsubcategory.length; i++){
            vm.allthirdsubcategory[i] = thirdsubcategory[i];

        }

       var productvaluethirdsubcategory = JSON.stringify(thirdsubcategory);
       var jsonObjectthirdsubcategory = JSON.parse(productvaluethirdsubcategory); 

      res.render('home.ejs' , {loadproductcategory : jsonObjectproductCategory , user : req.user , loadwebsitedata : jsonObjectwebsite , secondsubcategorydata : jsonObjectsecondSubCategorys , loadsubcategorydata  : jsonObjectsubcategory ,  loadcategorydata : jsonObjectcategory , loadthirdsubcategory : jsonObjectthirdsubcategory, loadproductdata : jsonObjectproduct});


                });

              });

            });      

          });

        });

      }); 

    });         

  }); 



 app.get('/productLanding',isLoggedIn, function(req, res) {

    var i = 0;

      var vm = this;

        vm.product = null;
        vm.allproduct = [];
        vm.thirdsubcategory = null;
        vm.allthirdsubcategory = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.secondSubCategory = null;
        vm.allsecondSubCategorys = [];
        vm.productcategory = null;
        vm.allproductcategory = [];
        vm.website = null;
        vm.allwebsites = [];
        var productddata1 = []; 
        var result=[];
        var jsonObjectproduct = [];
        var partId;
        // var productddata = [];

        var catname = req.query.cat;
        // console.log(catname);
        var subcatname = req.query.subcat;
        // console.log(subcatname);

        var secsubcatname = req.query.secondsubcat;
        // console.log(secsubcatname);

        var thirsubcatname = req.query.thirdsubcat;
        // console.log(thirsubcatname);

        var forthsubcatname = req.query.fourthsubcat;
        // console.log(forthsubcatname);

        var parentdatassname = req.query.parentdata;
        // console.log(parentdatassname);


        var Product = require('../app/models/products');
        var ProductCategory = require('../app/models/productCategory');
        var thirdSubCategory = require('../app/models/thirdsubcategory');
        var Category = require('../app/models/category');
        var Subcategory = require('../app/models/subcategory');
        var secondSubCategory = require('../app/models/secondsubcategory');
        var Website = require('../app/models/website');

        // ProductCategory.find( { $and: [{"procatName":req.query.cat}, {"prosubcatName": req.query.subcat},{"prosecsubcatName":req.query.secondsubcat}]}, function(err, productcategory){
        //       var dcs ;

        //   for(var i = 0; i < productcategory.length; i++){
        //     vm.allproductcategory[i] = productcategory[i];         

        //   Product.find({"partId" : productcategory[i].partId}, function(err, product){

        //       // console.log(product);
        //       // console.log("This Is Product Page Number");

              

        //     // var productvalueproduct = JSON.stringify(product);
        //     // var jsonObjectproduct = JSON.parse(productvalueproduct);

        //     doSomethingElse(product);


        //   });

        // }  


        // function doSomethingElse(product){
        //     console.log(product);
        //     console.log('this is PartId');

        //     } 
       

        //  }); 



          // ProductCategory.find( { $and: [{"procatName":req.query.cat}, {"prosubcatName": req.query.subcat},{"prosecsubcatName":req.query.secondsubcat}]}, function(err, productcategory){
          // // for(var i = 0; i < productcategory.length; i++){               

          // //     dcs.push(productcategory);

          // //  }

          //  console.log(productcategory);



          //   Product.find({"partId" : productcategory[0].partId}, function(err, product){

          //    console.log(product);

    
         ProductCategory.find(function(err, productCategory){

             // console.log(productCategory);     


         Product.find(function(err, product){

             // console.log(product);                    
               
          

        Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

        var productvaluewebsite = JSON.stringify(website);
        var jsonObjectwebsite = JSON.parse(productvaluewebsite);

      Category.find().sort({categoryName: 1}).exec(function(err, category) {      

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

   Subcategory.find().sort({subcategoryName: 1}).exec(function(err, subcategory) {      


        // Subcategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 

  
     secondSubCategory.find().sort({SecondsubcategoryName: 1}).exec(function(err, secondSubCategorys) {      

   
       for(var i = 0; i < secondSubCategorys.length; i++){
            vm.allsecondSubCategorys[i] = secondSubCategorys[i];
       }
        var secondSubCategoryvalue = JSON.stringify(secondSubCategorys);
        var jsonObjectsecondSubCategorys = JSON.parse(secondSubCategoryvalue);


       thirdSubCategory.find().sort({CategoryName: 1}).exec(function(err, thirdsubcategory) {      


     for(var i = 0; i < thirdsubcategory.length; i++){
            vm.allthirdsubcategory[i] = thirdsubcategory[i];

        }

       var productvaluethirdsubcategory = JSON.stringify(thirdsubcategory);
       var jsonObjectthirdsubcategory = JSON.parse(productvaluethirdsubcategory); 

         var statusOrder = ["0", "1", "2" , "3", "4" , "5" , "6" , "7" , "8" , "9"];
 
          product = product.sort(function(a, b) { 
                   return statusOrder.indexOf(a.setPriority) - statusOrder.indexOf(b.setPriority);
          });
          
      res.render('productLanding.ejs' , { catname : req.query.cat, subcatname : req.query.subcat, secsubcatname : req.query.secondsubcat, thirsubcatname : req.query.thirdsubcat, forthsubcatname : req.query.fourthsubcat, parentdatassname : req.query.parentdata,user : req.user , loadwebsitedata : jsonObjectwebsite , secondsubcategorydata : jsonObjectsecondSubCategorys , loadsubcategorydata  : jsonObjectsubcategory ,  loadcategorydata : jsonObjectcategory , loadthirdsubcategory : jsonObjectthirdsubcategory, loadproductdata : product , loadproductcategory : productCategory,});
              
               });
             });
          });      
        });
      });
    });              
   });        
  });

  app.post('/productsearch', function(req, res) {
    var i = 0;
      var vm = this;
        vm.product = null;
        vm.allproduct = [];
        vm.thirdsubcategory = null;
        vm.allthirdsubcategory = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.secondSubCategory = null;
        vm.allsecondSubCategorys = [];
        vm.productcategory = null;
        vm.allproductcategory = [];
        vm.website = null;
        vm.allwebsites = [];
        var productddata1 = []; 
        var result=[];
        var jsonObjectproduct = [];
        var partId;

        var catname = req.body.cat;
        var subcatname = req.body.subcat;

        var secsubcatname = req.body.secondsubcat;

        var thirsubcatname = req.body.thirdsubcat;

        var forthsubcatname = req.body.fourthsubcat;

        var parentdatassname = req.body.parentdata;

        var Product = require('../app/models/products');
        var ProductCategory = require('../app/models/productCategory');
        var thirdSubCategory = require('../app/models/thirdsubcategory');
        var Category = require('../app/models/category');
        var Subcategory = require('../app/models/subcategory');
        var secondSubCategory = require('../app/models/secondsubcategory');
        var Website = require('../app/models/website');

    ProductCategory.find({ $and: [{procatName : req.body.cat }, {prosubcatName : req.body.subcat },{ prosecsubcatName : req.body.secondsubcat } , { proparentdata : req.body.thirdsubcat } , { proparentdata1 : req.body.fourthsubcat }, { proparentdata2 : req.body.parentdata}]},function(err, productCategory){
          // console.log(productCategory);
            let promiseArr = [];
           let datas = [];

          productCategory.forEach(function(element) {
              promiseArr.push(element._id);   
          });


   Product.find({ $and : [{proCattId : promiseArr} ,{productStatus : "1"}]},function(err, product){             
                          
            // res.status(200).send({status : 200 , product});


              product.forEach(function(newelement) {

                // console.log(newelement.setPriority);

                datas.push(newelement);

                var statusOrder = ["0", "1", "2" , "3", "4" , "5" , "6" , "7" , "8" , "9"];
 
               datas = datas.sort(function(a, b) { 
                   return statusOrder.indexOf(a.setPriority) - statusOrder.indexOf(b.setPriority);
               });

                // console.log(datas);
                    
                     

              });


              Promise.all(datas)
    .then((result) => res.status(200).send({status : 200 , datas}))
    .catch((err) => res.send(err));
            
           });
 

         //  for(var i = 0; i < productCategory.length; i++){
         //       // vm.allproductCategory[i] = productCategory[i];

         
         // }

        
             });
          });      
       
    


  // show the home page (will also have our login links)
  app.get('/productshop', function(req, res) {

    var productid = req.query.id;
    var i = 0;
       var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.product = null;
        vm.allproduct = [];
        vm.slider = null;
        vm.allslider = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.review = null;
        vm.allreviews = [];

     var Review       = require('../app/models/review');

     var Website       = require('../app/models/website');

      var Slider       = require('../app/models/slider');

      var Category       = require('../app/models/category');

      var Subcategory       = require('../app/models/subcategory');

       var Product       = require('../app/models/products');

       var Size       = require('../app/models/size');

       var Color       = require('../app/models/color');

        Review.find(function (err, review) {

     for(var i = 0; i < review.length; i++){
            vm.allreviews[i] = review[i];

        }
           var productvaluereview = JSON.stringify(review);
       var jsonObjectreview = JSON.parse(productvaluereview);  


         Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 


        Product.find(function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

       var productvalueproduct = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvalueproduct); 

      Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 


       Subcategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 


     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

       Slider.find(function (err, slider) {

     for(var i = 0; i < slider.length; i++){
            vm.allslider[i] = slider[i];

        }

       var productvalueslider = JSON.stringify(slider);
       var jsonObjectslider = JSON.parse(productvalueslider);   

    res.render('productshop.ejs',{newfinalvalcoup : "0" , loadreviewdata : jsonObjectreview ,loadcolordata:jsonObjectcolor, loadsizedata:jsonObjectsize, loadwebsitedata : jsonObjectwebsite , loadsliderdata : jsonObjectslider , loadcategorydata : jsonObjectcategory , loadsubcategorydata  : jsonObjectsubcategory ,   loadproductdata : jsonObjectproduct , productid : req.query.id});

  });

  });

  });

  });   

   }); 

  });

   });   

   });


   });  


   // show the home page (will also have our login links)
  app.get('/shop-cart', function(req, res) {
    // var productid = req.query.id;
    var i = 0;
       var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.product = null;
        vm.allproduct = [];
        vm.slider = null;
        vm.allslider = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.review = null;
        vm.allreviews = [];

     var Review       = require('../app/models/review');

     var Website       = require('../app/models/website');

      var Slider       = require('../app/models/slider');

      var Category       = require('../app/models/category');

      var Subcategory       = require('../app/models/subcategory');

       var Product       = require('../app/models/products');

       var Size       = require('../app/models/size');

       var Color       = require('../app/models/color');

        Review.find(function (err, review) {

     for(var i = 0; i < review.length; i++){
            vm.allreviews[i] = review[i];

        }
           var productvaluereview = JSON.stringify(review);
       var jsonObjectreview = JSON.parse(productvaluereview);  


         Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 


        Product.find(function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

       var productvalueproduct = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvalueproduct); 

      Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 


       Subcategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 


     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

       Slider.find(function (err, slider) {

     for(var i = 0; i < slider.length; i++){
            vm.allslider[i] = slider[i];

        }

       var productvalueslider = JSON.stringify(slider);
       var jsonObjectslider = JSON.parse(productvalueslider);   

    res.render('shop-cart.ejs',{newfinalvalcoup : "0" , loadreviewdata : jsonObjectreview ,loadcolordata:jsonObjectcolor, loadsizedata:jsonObjectsize, loadwebsitedata : jsonObjectwebsite , loadsliderdata : jsonObjectslider , loadcategorydata : jsonObjectcategory , loadsubcategorydata  : jsonObjectsubcategory ,   loadproductdata : jsonObjectproduct , productid : req.query.id});

  });

  });

  });

  });   

   }); 

  });

   });   

   });

   });     



   // show the home page (will also have our login links)
  app.get('/shop-carts',isLoggedIn, function(req, res) {
    // var productid = req.query.id;
    var i = 0;
       var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.user = null;
        vm.allusers = [];
        vm.product = null;
        vm.allproduct = [];
        vm.slider = null;
        vm.allslider = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.review = null;
        vm.allreviews = [];

     var User       = require('../app/models/user');    

     var Review       = require('../app/models/review');

     var Website       = require('../app/models/website');

      var Slider       = require('../app/models/slider');

      var Category       = require('../app/models/category');

      var Subcategory       = require('../app/models/subcategory');

       var Product       = require('../app/models/products');

       var Size       = require('../app/models/size');

       var Color       = require('../app/models/color');

        Review.find(function (err, review) {

     for(var i = 0; i < review.length; i++){
            vm.allreviews[i] = review[i];

        }
           var productvaluereview = JSON.stringify(review);
       var jsonObjectreview = JSON.parse(productvaluereview);  


         Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 


        Product.find(function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

       var productvalueproduct = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvalueproduct); 


       Product.find({productStatus : "1" , viewStatus : "0"},function (err, productt) {
         
           console.log(productt.length);


         Product.find({addProRole : "manager" ,productStatus : "0" , viewStatus : "0"},function (err, producttdds) {
         
           console.log(producttdds.length);      
          

      Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 


       Subcategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 


     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

       Slider.find(function (err, slider) {

     for(var i = 0; i < slider.length; i++){
            vm.allslider[i] = slider[i];

        }

       var productvalueslider = JSON.stringify(slider);
       var jsonObjectslider = JSON.parse(productvalueslider);   

    res.render('shop-carts.ejs',{ftotalprod : producttdds.length,totalprod : productt.length, newfinalvalcoup : "0" ,user: req.user,loadreviewdata : jsonObjectreview ,loadcolordata:jsonObjectcolor, loadsizedata:jsonObjectsize, loadwebsitedata : jsonObjectwebsite , loadsliderdata : jsonObjectslider , loadcategorydata : jsonObjectcategory , loadsubcategorydata  : jsonObjectsubcategory ,   loadproductdata : jsonObjectproduct , productid : req.query.id});

  });

  });     

  });

  });

  });   

   }); 

  });

   });   

   });

   });  

   });   


     // show the home page (will also have our login links)
  app.get('/checkout', function(req, res) {
    // var productid = req.query.id;
    var i = 0;
       var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.product = null;
        vm.allproduct = [];
        vm.slider = null;
        vm.allslider = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.review = null;
        vm.allreviews = [];

     var Review       = require('../app/models/review');

     var Website       = require('../app/models/website');

      var Slider       = require('../app/models/slider');

      var Category       = require('../app/models/category');

      var Subcategory       = require('../app/models/subcategory');

       var Product       = require('../app/models/products');

       var Size       = require('../app/models/size');

       var Color       = require('../app/models/color');

        Review.find(function (err, review) {

     for(var i = 0; i < review.length; i++){
            vm.allreviews[i] = review[i];

        }
           var productvaluereview = JSON.stringify(review);
       var jsonObjectreview = JSON.parse(productvaluereview);  


         Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 


        Product.find(function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

       var productvalueproduct = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvalueproduct); 

      Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 


       Subcategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 


     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

       Slider.find(function (err, slider) {

     for(var i = 0; i < slider.length; i++){
            vm.allslider[i] = slider[i];

        }

       var productvalueslider = JSON.stringify(slider);
       var jsonObjectslider = JSON.parse(productvalueslider);   

    res.render('checkout.ejs',{ newfinalvalcoup :  "0", loadreviewdata : jsonObjectreview ,loadcolordata:jsonObjectcolor, loadsizedata:jsonObjectsize, loadwebsitedata : jsonObjectwebsite , loadsliderdata : jsonObjectslider ,  loadcategorydata : jsonObjectcategory , loadsubcategorydata  : jsonObjectsubcategory ,   loadproductdata : jsonObjectproduct , productid : req.query.id});

  });

  });

  });

  });   

   }); 

  });

   });   

   });

   });   



     // show the home page (will also have our login links)
  app.post('/checkoutdd', function(req, res) {
    // var productid = req.query.id;
    console.log(req.body);
    var orderprice = req.body.finalPricett;
    console.log(orderprice);
    var i = 0;
       var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.product = null;
        vm.allproduct = [];
        vm.slider = null;
        vm.allslider = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.review = null;
        vm.allreviews = [];

     var Review       = require('../app/models/review');

     var Website       = require('../app/models/website');

      var Slider       = require('../app/models/slider');

      var Category       = require('../app/models/category');

      var Subcategory       = require('../app/models/subcategory');

       var Product       = require('../app/models/products');

       var Size       = require('../app/models/size');

       var Color       = require('../app/models/color');

        Review.find(function (err, review) {

     for(var i = 0; i < review.length; i++){
            vm.allreviews[i] = review[i];

        }
           var productvaluereview = JSON.stringify(review);
       var jsonObjectreview = JSON.parse(productvaluereview);  


         Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 


        Product.find(function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

       var productvalueproduct = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvalueproduct); 

      Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 


       Subcategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 


     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

       Slider.find(function (err, slider) {

     for(var i = 0; i < slider.length; i++){
            vm.allslider[i] = slider[i];

        }

       var productvalueslider = JSON.stringify(slider);
       var jsonObjectslider = JSON.parse(productvalueslider);   

    res.render('checkout.ejs',{ newfinalvalcoup :  "0", loadreviewdata : jsonObjectreview ,loadcolordata:jsonObjectcolor, loadsizedata:jsonObjectsize, loadwebsitedata : jsonObjectwebsite , loadsliderdata : jsonObjectslider ,  loadcategorydata : jsonObjectcategory , loadsubcategorydata  : jsonObjectsubcategory ,   loadproductdata : jsonObjectproduct , productid : req.query.id , orderprice : req.body.finalPricett});

  });

  });

  });

  });   

   }); 

  });

   });   

   });

   });     



     // show the home page (will also have our login links)
  app.post('/checkoutdds', isLoggedIn, function(req, res) {
    // var productid = req.query.id;
    console.log(req.body);
    var orderprice = req.body.finalPricett;
    console.log(orderprice);
    var i = 0;
       var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.product = null;
        vm.allproduct = [];
        vm.slider = null;
        vm.allslider = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.review = null;
        vm.allreviews = [];

     var Review       = require('../app/models/review');

     var Website       = require('../app/models/website');

      var Slider       = require('../app/models/slider');

      var Category       = require('../app/models/category');

      var Subcategory       = require('../app/models/subcategory');

       var Product       = require('../app/models/products');

       var Size       = require('../app/models/size');

       var Color       = require('../app/models/color');

        Review.find(function (err, review) {

     for(var i = 0; i < review.length; i++){
            vm.allreviews[i] = review[i];

        }
           var productvaluereview = JSON.stringify(review);
       var jsonObjectreview = JSON.parse(productvaluereview);  


         Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 


        Product.find(function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

       var productvalueproduct = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvalueproduct); 


       Product.find({productStatus : "1" , viewStatus : "0"},function (err, productt) {
         
           console.log(productt.length);

      Product.find({addProRole : "manager" ,productStatus : "0" , viewStatus : "0"},function (err, producttdds) {
         
           console.log(producttdds.length);      
                
        

      Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 


       Subcategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 


     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

       Slider.find(function (err, slider) {

     for(var i = 0; i < slider.length; i++){
            vm.allslider[i] = slider[i];

        }

       var productvalueslider = JSON.stringify(slider);
       var jsonObjectslider = JSON.parse(productvalueslider);   

    res.render('checkouts.ejs',{ftotalprod : producttdds.length,totalprod : productt.length ,user:req.user, newfinalvalcoup :  "0", loadreviewdata : jsonObjectreview ,loadcolordata:jsonObjectcolor, loadsizedata:jsonObjectsize, loadwebsitedata : jsonObjectwebsite , loadsliderdata : jsonObjectslider ,  loadcategorydata : jsonObjectcategory , loadsubcategorydata  : jsonObjectsubcategory ,   loadproductdata : jsonObjectproduct , productid : req.query.id , orderprice : req.body.finalPricett});

  });

  });

  });

  });   

   }); 

  });

   });   

   });

   }); 

   }); 

    });    

   
     // show the home page (will also have our login links)
  app.post('/check_coupon', function(req, res) {


    var dates=  new Date();
   // var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();
   var strDate = dates.getFullYear()+ "-" + (dates.getMonth()+1) + "-" + dates.getDate();

    // console.log(req.body.couopnCode);
     // console.log(strDate);

     console.log(req.body.hdnCartTotalcart);

    var i = 0;
       var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.category = null;
        vm.allcategory = [];
        vm.coupon = null;
        vm.allcoupon = [];
      

    var Coupon       = require('../app/models/coupon');
  
     var Category       = require('../app/models/category');

     var Website       = require('../app/models/website');

      Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 



     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 


       Coupon.find({"couponCode" : req.body.couopnCode},function (err, coupon) {

        // if(coupon){

           for(var i = 0; i < coupon.length; i++){
            vm.allcoupon[i] = coupon[i];
         // console.log(coupon[i].startDate);
           // console.log(coupon[i].couponComission);
         // console.log(strDate);
            if(strDate >= coupon[i].startDate  && strDate <= coupon[i].endDate){
               
               console.log(coupon);

               var TotalfinalcoupVal = (req.body.hdnCartTotalcart * coupon[i].couponComission) / 100; 

               var finalvalcoup = req.body.hdnCartTotalcart - TotalfinalcoupVal;
                console.log(finalvalcoup);

               req.flash('locals.couponsucess', 'Coupon Applied Sucessfully !!');
               res.render('shop-cart.ejs',{couponsucess : 'Coupon Applied Sucessfully !!' , loadwebsitedata : jsonObjectwebsite ,  loadcategorydata : jsonObjectcategory ,  productid : req.query.id , newfinalvalcoup : finalvalcoup});


            } else {

               req.flash('locals.couponerror', 'Coupon Expired !!');
               // return res.redirect('/checkout');
               res.render('shop-cart.ejs',{couponerror : 'Coupon Expired !!' , loadwebsitedata : jsonObjectwebsite  , loadcategorydata : jsonObjectcategory ,  productid : req.query.id , newfinalvalcoup :  "0"});

 
               }

        }
 

  });

  });

  });

  });  


     // show the home page (will also have our login links)
  app.post('/check_coupon_login', isLoggedIn, function(req, res) {

    var dates=  new Date();
    var strDate = dates.getFullYear()+ "-" + (dates.getMonth()+1) + "-" + dates.getDate();
    var i = 0;
       var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.category = null;
        vm.allcategory = [];
        vm.coupon = null;
        vm.allcoupon = [];
      

      var Coupon = require('../app/models/coupon');
  
      var Category = require('../app/models/category');

      var Website = require('../app/models/website');

      Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 



     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 


       Coupon.find({"couponCode" : req.body.couopnCode},function (err, coupon) {

        // if(coupon){

           for(var i = 0; i < coupon.length; i++){
            vm.allcoupon[i] = coupon[i];
         // console.log(coupon[i].startDate);
           console.log(coupon[i].couponComission);
         // console.log(strDate);
            if(strDate >= coupon[i].startDate  && strDate <= coupon[i].endDate){
              
               var TotalfinalcoupVal = (req.body.hdnCartTotalcart * coupon[i].couponComission) / 100; 

               var finalvalcoup = req.body.hdnCartTotalcart - TotalfinalcoupVal;

               req.flash('locals.couponsucess', 'Coupon Applied Sucessfully !!');
               res.render('shop-carts.ejs',{ user:req.user , couponsucess : 'Coupon Applied Sucessfully !!' , loadwebsitedata : jsonObjectwebsite ,  loadcategorydata : jsonObjectcategory ,  productid : req.query.id , newfinalvalcoup : finalvalcoup});


            } else {

               req.flash('locals.couponerror', 'Coupon Expired !!');
               // return res.redirect('/checkout');
               res.render('shop-carts.ejs',{ user:req.user , couponerror : 'Coupon Expired !!' , loadwebsitedata : jsonObjectwebsite  , loadcategorydata : jsonObjectcategory ,  productid : req.query.id , newfinalvalcoup :  "0"});

 
            }

          }

        });

      });

    });

  });    
    

     // show the home page (will also have our login links)
  app.get('/checkouts', isLoggedIn, function(req, res) {
    // var productid = req.query.id;
    var orderprice = req.body.finalPricett;
      var i = 0;
      var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.user = null;
        vm.allusers = [];
        vm.product = null;
        vm.allproduct = [];
        vm.slider = null;
        vm.allslider = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.review = null;
        vm.allreviews = [];
 
        var Review       = require('../app/models/review');

        var Website       = require('../app/models/website');

        var Slider       = require('../app/models/slider');

        var Category       = require('../app/models/category');

        var Subcategory       = require('../app/models/subcategory');

        var Product       = require('../app/models/products');

        var Size       = require('../app/models/size');

        var Color       = require('../app/models/color');


        Review.find(function (err, review) {

     for(var i = 0; i < review.length; i++){
            vm.allreviews[i] = review[i];

        }
           var productvaluereview = JSON.stringify(review);
       var jsonObjectreview = JSON.parse(productvaluereview);  


         Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 


        Product.find(function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

       var productvalueproduct = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvalueproduct); 



       Product.find({productStatus : "1" , viewStatus : "0"},function (err, productt) {
         
           console.log(productt.length);

      Product.find({addProRole : "manager" ,productStatus : "0" , viewStatus : "0"},function (err, producttdds) {
         
           console.log(producttdds.length);      
                
       
      Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 


       Subcategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 


     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

       Slider.find(function (err, slider) {

     for(var i = 0; i < slider.length; i++){
            vm.allslider[i] = slider[i];

        }

       var productvalueslider = JSON.stringify(slider);
       var jsonObjectslider = JSON.parse(productvalueslider);   

    res.render('checkouts.ejs',{ftotalprod : producttdds.length, totalprod : productt.length, newfinalvalcoup : "0" , user : req.user , loadreviewdata : jsonObjectreview ,loadcolordata:jsonObjectcolor, loadsizedata:jsonObjectsize, loadwebsitedata : jsonObjectwebsite , loadsliderdata : jsonObjectslider , loadcategorydata : jsonObjectcategory , loadsubcategorydata  : jsonObjectsubcategory ,   loadproductdata : jsonObjectproduct , productid : req.query.id ,orderprice : req.body.finalPricett});

  });

  });

  });

  });   

   }); 


  });   

   }); 

  });

   });   

   });

   });     


 
  app.get('/productshops', isLoggedIn, function(req, res) {


    var productid = req.query.id;
    var i = 0;
       var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.product = null;
        vm.allproduct = [];
        vm.slider = null;
        vm.allslider = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.review = null;
        vm.allreviews = [];
        vm.productcategory = null;
        vm.allproductcategory = [];

     var Review       = require('../app/models/review');

     var ProductCategory = require('../app/models/productCategory');

     var Website       = require('../app/models/website');

      var Slider       = require('../app/models/slider');

      var Category       = require('../app/models/category');

      var Subcategory       = require('../app/models/subcategory');

       var Product       = require('../app/models/products');

       var Size       = require('../app/models/size');

       var Color       = require('../app/models/color');

        Review.find(function (err, review) {

     for(var i = 0; i < review.length; i++){
            vm.allreviews[i] = review[i];

        }
           var productvaluereview = JSON.stringify(review);
       var jsonObjectreview = JSON.parse(productvaluereview);  



         Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 


        Product.find({"_id" : req.query.id},function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

       var productvalueproduct = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvalueproduct);

       console.log(jsonObjectproduct); 

       Product.find({productStatus : "1" , viewStatus : "0"},function (err, productt) {
         
         // console.log(productt.length);

         Product.find({addProRole : "manager" ,productStatus : "0" , viewStatus : "0"},function (err, producttdds) {
         
           // console.log(producttdds.length);   

      Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

       ProductCategory.find(function (err, productcategory) {

     for(var i = 0; i < productcategory.length; i++){
            vm.allproductcategory[i] = productcategory[i];

        }

       var productvalueproductcategory = JSON.stringify(productcategory);
       var jsonObjectproductcategory = JSON.parse(productvalueproductcategory); 

       Subcategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

       Slider.find(function (err, slider) {

     for(var i = 0; i < slider.length; i++){
            vm.allslider[i] = slider[i];

        }

       var productvalueslider = JSON.stringify(slider);
       var jsonObjectslider = JSON.parse(productvalueslider);   

    res.render('productshops.ejs',{loadproductcategory : jsonObjectproductcategory,ftotalprod : producttdds.length,totalprod : productt.length, newfinalvalcoup : "0" , user : req.user , loadreviewdata : jsonObjectreview , loadcolordata:jsonObjectcolor, loadsizedata:jsonObjectsize, loadwebsitedata : jsonObjectwebsite , loadsliderdata : jsonObjectslider , loadcategorydata : jsonObjectcategory , loadsubcategorydata : jsonObjectsubcategory ,  loadproductdata : jsonObjectproduct , productid : req.query.id});

                 });

                });

              });

            });

          });   

         }); 

        });   

       });

      }); 

     });  

    });   

  }); 
  

    app.post('/review-insert', isLoggedIn, function(req, res) {
   //console.log(res);
   var Review       = require('../app/models/review');
   var User       = require('../app/models/user');
   var Category       = require('../app/models/category');
   //var Addsymptomsfemales       = require('../app/models/addsymptomfemale');   
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

   var i = 0;
  var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.review = null;
        vm.allreviews = [];

     var Website       = require('../app/models/website');

      Review.find(function (err, review) {

     for(var i = 0; i < review.length; i++){
            vm.allreviews[i] = review[i];

        }


     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

             console.log(req.body.prodId); 

         var set = {
             userId : req.body.userId,
             prodId : req.body.prodId,
             name : req.body.name,
             email : req.body.email,
             rating : req.body.rating,
             comment : req.body.comment,
             status : "0",
             datetime: strDate
             };

           var review = new Review(set);
           review.save(set,
            function (err, doc) {
             //console.log(doc);
            });   
        
        return res.redirect('productshops?id='+req.body.prodId);

      });

   }); 

  });       

    app.post('/category-insert', isLoggedIn, function(req, res) {
   //console.log(res);
  // var Product       = require('../app/models/products');
   var User       = require('../app/models/user');
   var Category       = require('../app/models/category');
   //var Addsymptomsfemales       = require('../app/models/addsymptomfemale');   
   var i = 0;
  var vm = this;
        vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      

         var set = {
             categoryName : req.body.categoryName.trim(),
             status : "0"
             };

           var category = new Category(set);
           category.save(set,
            function (err, doc) {
             //console.log(doc);
            });
        
        
     return res.redirect('managecategory');

      });

   });  

app.post('/unit-insert', isLoggedIn, function(req, res) {
   //console.log(res);
  // var Product       = require('../app/models/products');
   var User       = require('../app/models/user');
   var Category       = require('../app/models/category');
   //var Addsymptomsfemales       = require('../app/models/addsymptomfemale');   
   var i = 0;
  var vm = this;
        vm.website = null;
        vm.allwebsites = [];

     var Unit       = require('../app/models/unit');   
     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      

         var set = {
             unitType : req.body.unitType,
             symbol : req.body.symbol,
             formal_name : req.body.formal_name,
             code : req.body.code,
             decimal_place : req.body.decimal_place,
             status : "0"
             };

           var unit = new Unit(set);
           unit.save(set,
            function (err, doc) {
             //console.log(doc);
            });
        
        
     return res.redirect('manageunit');

      });

   });  


app.post('/make-insert', isLoggedIn, function(req, res) {
   //console.log(res);
  // var Product       = require('../app/models/products');
   var User       = require('../app/models/user');
   var Make       = require('../app/models/make');
   //var Addsymptomsfemales       = require('../app/models/addsymptomfemale');   
   var i = 0;
  var vm = this;
        vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  


      Make.findOne({"makeName" : req.body.makeName}, function (err, makes) {
        
        // console.log(err);
         console.log(makes);

         if(!makes){

           var set = {
             makeName : req.body.makeName,
             status : "0"
             };

           var make = new Make(set);
           make.save(set,
            function (err, doc) {
             //console.log(doc);
            });

            return res.redirect('managemake');

         } else {

          
         return res.redirect('managemake');

         }

      });
       
      });

   });  



app.get('/versionApi', function(req, res) {
   var User       = require('../app/models/user');
   var Version       = require('../app/models/version');

           // var set = {
           //   apiVersion : "1",
           //   downStatus : "False"
           //   };

           // var version = new Version(set);
           // version.save(set,
           //  function (err, doc) {
           //   //console.log(doc);

           //  res.status(200).json({ status :200, doc});

           //  });

           var appVersion = req.query.appVersion;

    Version.findOne({"downStatus" : "False"}, function (err, version) {

      if(version){

         // console.log(version);
          // console.log(version.appVersion);
            
              if(version.appVersion == req.query.appVersion){

                 res.status(200).json({ status :200, response : 0 });

              } else {

                res.status(200).json({ status :200, response : 1 });
              }


      } else {

           res.status(200).json({ status :200, response : 2});
      }
    
    });

       
      });


    app.post('/coupon-insert', isLoggedIn, function(req, res) {
   //console.log(res);
  // var Product       = require('../app/models/products');
   var User       = require('../app/models/user');
   var Coupon       = require('../app/models/coupon');
   //var Addsymptomsfemales       = require('../app/models/addsymptomfemale');   
   var i = 0;
  var vm = this;
        vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      

         var set = {         
             couponName : req.body.couponName,
             couponCode : req.body.couponCode,
             couponComission : req.body.couponComission,
             startDate : req.body.startDate,
             endDate : req.body.endDate,
             status : "0"
             };

           var coupon = new Coupon(set);
           coupon.save(set,
            function (err, doc) {
             //console.log(doc);
            });
        
        
     return res.redirect('managecoupon');

      });

   });  

        app.post('/size-insert', isLoggedIn, function(req, res) {
   //console.log(res);
  // var Product       = require('../app/models/products');
   var User       = require('../app/models/user');
   var Size       = require('../app/models/size');
   //var Addsymptomsfemales       = require('../app/models/addsymptomfemale');   
   var i = 0;
  var vm = this;
        vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      

         var set = {
             sizeName : req.body.sizeName,
             status : "0"
             };

           var size = new Size(set);
           size.save(set,
            function (err, doc) {
             //console.log(doc);
            });
        
        
     return res.redirect('managesize');

      });

   }); 



        app.post('/color-insert', isLoggedIn, function(req, res) {
   //console.log(res);
  // var Product       = require('../app/models/products');
   var User       = require('../app/models/user');
   var Color       = require('../app/models/color');
   //var Addsymptomsfemales       = require('../app/models/addsymptomfemale');   
   var i = 0;
  var vm = this;
        vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      

         var set = {
             colorName : req.body.colorName,
             status : "0"
             };

           var color = new Color(set);
           color.save(set,
            function (err, doc) {
             //console.log(doc);
            });
        
        
     return res.redirect('managecolor');

      });

   }); 

     app.get('/edit_size', isLoggedIn, function(req, res) {
   var Product       = require('../app/models/products');
   var Size       = require('../app/models/size');

         var i = 0;
         var vm = this;
            vm.size = null;
            vm.allsize = [];
            vm.website = null;
            vm.allwebsites = [];

     var Website       = require('../app/models/website');
         var catid = req.query.id;


   Size.find(function (err, size) {
     // docs is an array
       for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];
       }
       var productvaluesize = JSON.stringify(size);
        var jsonObjectsize = JSON.parse(productvaluesize);     

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      // console.log(jsonObject);

   
      res.render('edit_size.ejs', {loadwebsitedata : jsonObjectwebsite , datasize: jsonObjectsize, user : req.user, sizeid : req.query.id});

      });


 });

  });  


 app.get('/edit_color', isLoggedIn, function(req, res) {
   var Product       = require('../app/models/products');
   var Color       = require('../app/models/color');

         var i = 0;
         var vm = this;
            vm.color = null;
            vm.allcolor = [];
            vm.website = null;
            vm.allwebsites = [];

     var Website       = require('../app/models/website');
         var colorid = req.query.id;


   Color.find(function (err, color) {
     // docs is an array
       for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];
       }
       var productvaluecolor = JSON.stringify(color);
        var jsonObjectcolor = JSON.parse(productvaluecolor);     

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      // console.log(jsonObject);

   
      res.render('edit_color.ejs', {loadwebsitedata : jsonObjectwebsite , datacolor: jsonObjectcolor, user : req.user, colorid : req.query.id});

      });


 });

  });  


     app.get('/edit_category', isLoggedIn, function(req, res) {
   var Product       = require('../app/models/products');
   var Category       = require('../app/models/category');
   var Coupon       = require('../app/models/coupon');

         var i = 0;
         var vm = this;
            vm.category = null;
            vm.allcategorys = [];
            vm.website = null;
            vm.allwebsites = [];
            vm.coupon = null;
            vm.allcoupons = [];

     var Website       = require('../app/models/website');
         var catid = req.query.id;


   Category.find(function (err, category) {
     // docs is an array
       for(var i = 0; i < category.length; i++){
            vm.allcategorys[i] = category[i];
       }
       var productvaluecategory = JSON.stringify(category);
        var jsonObjectcategory = JSON.parse(productvaluecategory);     

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      // console.log(jsonObject);

   
      res.render('edit_category.ejs', {loadwebsitedata : jsonObjectwebsite , datacategory : jsonObjectcategory, user : req.user, catid : req.query.id});

      });


 });

  }); 


  //    app.get('/deleteCategory',isLoggedIn, function(req, res) {
  //    var Category       = require('../app/models/category');
  //      Category.remove({_id : req.query.id}, function (err, category) {
  //        // console.log(makes);    
  //   return res.redirect('/managecategory');
  //     });
  // });



     app.get('/edit_coupon', isLoggedIn, function(req, res) {
   var Product       = require('../app/models/products');
   var Coupon       = require('../app/models/coupon');

         var i = 0;
         var vm = this;
            vm.coupon = null;
            vm.allcoupons = [];
            vm.website = null;
            vm.allwebsites = [];

     var Website       = require('../app/models/website');
         var coupid = req.query.id;


   Coupon.find(function (err, coupon) {
     // docs is an array
       for(var i = 0; i < coupon.length; i++){
            vm.allcoupons[i] = coupon[i];
       }
       var productvaluecoupon = JSON.stringify(coupon);
        var jsonObjectcoupon = JSON.parse(productvaluecoupon);     

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      // console.log(jsonObject);

   
      res.render('edit_coupon.ejs', {loadwebsitedata : jsonObjectwebsite , datacoupon: jsonObjectcoupon, user : req.user, coupid : req.query.id});

      });


 });

  }); 

     app.get('/cms',isLoggedIn, function(req, res) {
     var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];
        vm.cms = null;
        vm.allcms = [];


         vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

    var Cms       = require('../app/models/cms');

     Cms.find(function (err, cms) {
      // docs is an array
        for(var i = 0; i < cms.length; i++){
            vm.allcms[i] = cms[i];


        }
 
       var cmsvalue = JSON.stringify(cms);
       var jsonObjectcms = JSON.parse(cmsvalue);

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }
     
       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

    var Consult       = require('../app/models/consults');
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

           //console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
     
        res.render('cms.ejs', { cmsdata: jsonObjectcms, loadwebsitedata : jsonObjectwebsite ,loadconsultdata: jsonObject, user : req.user ,totalreadconsultstatus : finalstatusconsult,});

      });

    });   
  
  });

  });    

       app.post('/cms-insert', isLoggedIn, function(req, res) {
   //console.log(res);
  // var Product       = require('../app/models/products');
   var User       = require('../app/models/user');
   var Cms       = require('../app/models/cms');
   //var Addsymptomsfemales       = require('../app/models/addsymptomfemale');   
   var i = 0;
  var vm = this;
        vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      

         var set = {
             title : req.body.title,
             description : req.body.description,
             status : "0"
             };

           var cms = new Cms(set);
           cms.save(set,
            function (err, doc) {
             //console.log(doc);
            });
        
        
     return res.redirect('cms');

      });

   });  

        app.get('/edit_cms', isLoggedIn, function(req, res) {
   var Product       = require('../app/models/products');
   var Cms       = require('../app/models/cms');

         var i = 0;
         var vm = this;
            vm.cms = null;
            vm.allcms = [];
            vm.website = null;
            vm.allwebsites = [];

     var Website       = require('../app/models/website');
         var cmsid = req.query.id;


   Cms.find(function (err, cms) {
     // docs is an array
       for(var i = 0; i < cms.length; i++){
            vm.allcms[i] = cms[i];
       }
       var productvaluecms = JSON.stringify(cms);
        var jsonObjectcms = JSON.parse(productvaluecms);     

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      // console.log(jsonObject);

   
      res.render('edit_cms.ejs', {loadwebsitedata : jsonObjectwebsite , cmsdata: jsonObjectcms, user : req.user, cmsid : req.query.id});

      });


 });

  }); 

app.get('/paymentmanage',isLoggedIn, function(req, res) {
     var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];
        vm.payment = null;
        vm.allpayment = [];


         vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

    var Payment       = require('../app/models/payment');

     Payment.find(function (err, payment) {
      // docs is an array
        for(var i = 0; i < payment.length; i++){
            vm.allpayment[i] = payment[i];


        }
 
       var paymentvalue = JSON.stringify(payment);
       var jsonObjectpayment = JSON.parse(paymentvalue);

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }
     
       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

    var Consult       = require('../app/models/consults');
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

           //console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
     
        res.render('paymentmanage.ejs', { paymentdata: jsonObjectpayment, loadwebsitedata : jsonObjectwebsite ,loadconsultdata: jsonObject, user : req.user ,totalreadconsultstatus : finalstatusconsult,});

      });

    });   
  
  });

  });  

    app.post('/payment-insert', isLoggedIn, function(req, res) {
   //console.log(res);
  // var Product       = require('../app/models/products');
   var User       = require('../app/models/user');
   var Payment       = require('../app/models/payment');
   //var Addsymptomsfemales       = require('../app/models/addsymptomfemale');   
   var i = 0;
  var vm = this;
        vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

       Payment.findById({_id : "5b7d50b561c1c716301a1d2d"}, function (err, payment) {
              
               Payment.update(
                 { _id: "5b7d50b561c1c716301a1d2d" },
                 { $set: { 
                  clientId :req.body.clientId,
                  clientSecret : req.body.clientSecret,
                  returnUrl : req.body.returnUrl,
                  cancelUrl : req.body.cancelUrl,         
                  status : "0"} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

      

         // var set = {
         //      clientId :req.body.clientId,
         //      clientSecret : req.body.clientSecret,
         //      returnUrl : req.body.returnUrl,
         //      cancelUrl : req.body.cancelUrl,         
         //      status : "0"
         //     };

         //   var payment = new Payment(set);
         //   payment.save(set,
         //    function (err, doc) {
         //     //console.log(doc);
         //    });
        
        
     return res.redirect('paymentmanage');

      });

   });    
  


    app.get('/edit_subcategory', isLoggedIn, function(req, res) {
   var SubCategory       = require('../app/models/subcategory');
   var Category       = require('../app/models/category');

         var i = 0;
         var vm = this;
            vm.category = null;
            vm.allcategorys = [];
            vm.subcategory = null;
            vm.allsubcategorys = [];
            vm.website = null;
            vm.allwebsites = [];

     var Website       = require('../app/models/website');
         var subcatid = req.query.id;

    SubCategory.find(function (err, subcategory) {
     // docs is an array
       for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategorys[i] = subcategory[i];
       }
       var productvaluesubcategorys = JSON.stringify(subcategory);
        var jsonObjectsubcategorys = JSON.parse(productvaluesubcategorys);       


   Category.find(function (err, category) {
     // docs is an array
       for(var i = 0; i < category.length; i++){
            vm.allcategorys[i] = category[i];
       }
       var productvaluecategorys = JSON.stringify(category);
        var jsonObjectcategorys = JSON.parse(productvaluecategorys);     

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      // console.log(jsonObject);

   
      res.render('edit_subcategory.ejs', {loadwebsitedata : jsonObjectwebsite , datacategory: jsonObjectcategorys,  datasubcategory : jsonObjectsubcategorys, user : req.user, subcatid : req.query.id});
 
      });


 });

  }); 

   });    


  //   app.get('/deleteSubCategory',isLoggedIn, function(req, res) {
  //    var SubCategory       = require('../app/models/subcategory');
  //      SubCategory.remove({_id : req.query.id}, function (err, subcategory) {
  //        // console.log(makes);    
  //   return res.redirect('/managesubcategory');
  //     });
  // });

  //   app.get('/deletethirdSubCategory',isLoggedIn, function(req, res) {
  //    var thirdSubCategory       = require('../app/models/thirdsubcategory');
  //      thirdSubCategory.remove({_id : req.query.id}, function (err, thirdsubcategory) {
  //        // console.log(makes);    
  //   return res.redirect('/managethirdsubcategory');
  //     });
  // });



app.get('/categoryStatus', isLoggedIn, function(req, res) {
   var Category       = require('../app/models/category');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

         var _id = req.query.id;
         var statusval = req.query.status;

         // console.log(_id);
    var i = 0;
   var vm = this;
        vm.category = null;
        vm.allcategorys = [];
   Category.find(function (err, categorys) {
     // docs is an array
       for(var i = 0; i < categorys.length; i++){
            vm.allcategorys[i] = categorys[i];
       }
        var categorysvalue = JSON.stringify(categorys);
        var jsonObjectcategorys = JSON.parse(categorysvalue);

         Category.findById({_id : req.query.id}, function (err, category) {
              
             // console.log(category._id);

               Category.update(
                 { _id: req.query.id },
                 { $set: { 
                   status: req.query.status} 
              },
                 function (err, doc) {
                    // console.log(doc);        
                 });  

         });

    return res.redirect('/managecategory');
 });

 });


app.get('/unitStatus', isLoggedIn, function(req, res) {
   var Category       = require('../app/models/category');
   var Unit       = require('../app/models/unit');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

         var _id = req.query.id;
         var statusval = req.query.status;

         console.log(_id);

    var i = 0;
    var vm = this;
        vm.units = null;
        vm.allunits = [];
   Unit.find(function (err, units) {
     // docs is an array
       for(var i = 0; i < units.length; i++){
            vm.allunits[i] = units[i];
       }
        var unitsvalue = JSON.stringify(units);
        var jsonObjectunits = JSON.parse(unitsvalue);

         Unit.findById({_id : req.query.id}, function (err, unit) {
              
             console.log(unit._id);

               Unit.update(
                 { _id: req.query.id },
                 { $set: { 
                   status: req.query.status} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

    return res.redirect('/manageunit');
 });

 });





app.get('/couponStatus', isLoggedIn, function(req, res) {
   var Coupon       = require('../app/models/coupon');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

         var _id = req.query.id;
         var statusval = req.query.status;

         console.log(_id);
    var i = 0;
   var vm = this;
        vm.coupon = null;
        vm.allcoupons = [];
   Coupon.find(function (err, coupon) {
     // docs is an array
       for(var i = 0; i < coupon.length; i++){
            vm.allcoupons[i] = coupon[i];
       }
        var couponvalue = JSON.stringify(coupon);
        var jsonObjectcoupon = JSON.parse(couponvalue);

         Coupon.findById({_id : req.query.id}, function (err, coupon) {
              
             console.log(coupon._id);

               Coupon.update(
                 { _id: req.query.id },
                 { $set: { 
                   status: req.query.status} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

    return res.redirect('/managecoupon');
 });

 });  

app.get('/sizeStatus', isLoggedIn, function(req, res) {
   var Size       = require('../app/models/size');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

         var _id = req.query.id;
         var statusval = req.query.status;

         console.log(_id);
    var i = 0;
   var vm = this;
        vm.size = null;
        vm.allsize = [];
   Size.find(function (err, size) {
     // docs is an array
       for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];
       }
        var sizevalue = JSON.stringify(size);
        var jsonObjectsize = JSON.parse(sizevalue);

         Size.findById({_id : req.query.id}, function (err, size) {
              
             console.log(size._id);

               Size.update(
                 { _id: req.query.id },
                 { $set: { 
                   status: req.query.status} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

    return res.redirect('/managesize');
 });

 }); 
  

  app.get('/colorStatus', isLoggedIn, function(req, res) {
   var Color       = require('../app/models/color');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

         var _id = req.query.id;
         var statusval = req.query.status;

         console.log(_id);
    var i = 0;
   var vm = this;
        vm.color = null;
        vm.allcolor = [];
   Color.find(function (err, color) {
     // docs is an array
       for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];
       }
        var colorvalue = JSON.stringify(color);
        var jsonObjectcolor = JSON.parse(colorvalue);

         Color.findById({_id : req.query.id}, function (err, color) {
              
             console.log(color._id);

               Color.update(
                 { _id: req.query.id },
                 { $set: { 
                   status: req.query.status} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

    return res.redirect('/managecolor');
 });

 }); 

  app.get('/secondsubcategoryStatus', isLoggedIn, function(req, res) {
   var secondSubCategory       = require('../app/models/secondsubcategory');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

         var _id = req.query.id;
         var statusval = req.query.status;

         console.log(_id);
    var i = 0;
    var vm = this;
        vm.secondSubCategory = null;
        vm.allsecondSubCategorys = [];
   secondSubCategory.find(function (err, secondSubCategorys) {
     // docs is an array
       for(var i = 0; i < secondSubCategorys.length; i++){
            vm.allsecondSubCategorys[i] = secondSubCategorys[i];
       }
        var secondSubCategoryvalue = JSON.stringify(secondSubCategorys);
        var jsonObjectsecondSubCategorys = JSON.parse(secondSubCategoryvalue);

         secondSubCategory.findById({_id : req.query.id}, function (err, secondSubCategorys) {
              
             console.log(secondSubCategorys._id);

               secondSubCategory.update(
                 { _id: _id },
                 { $set: { 
                   status: req.query.status} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

    return res.redirect('/managesecondsubcategory');
 });

 }); 
  
  

  app.get('/subcategoryStatus', isLoggedIn, function(req, res) {
   var SubCategory       = require('../app/models/subcategory');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

         var _id = req.query.id;
         var statusval = req.query.status;

         console.log(_id);
    var i = 0;
   var vm = this;
        vm.category = null;
        vm.allcategorys = [];
   SubCategory.find(function (err, subcategorys) {
     // docs is an array
       for(var i = 0; i < subcategorys.length; i++){
            vm.allsubcategorys[i] = subcategorys[i];
       }
        var subcategorysvalue = JSON.stringify(subcategorys);
        var jsonObjectsubcategorys = JSON.parse(subcategorysvalue);

         SubCategory.findById({_id : req.query.id}, function (err, subcategory) {
              
             // console.log(subcategory._id);

               SubCategory.update(
                 { _id: req.query.id },
                 { $set: { 
                   status: req.query.status} 
              },
                 function (err, doc) {
                    // console.log(doc);        
                 });  

         });

    return res.redirect('/managesubcategory');
 });

 }); 
  

app.get('/cmsStatus', isLoggedIn, function(req, res) {
   var Cms       = require('../app/models/cms');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

         var _id = req.query.id;
         var statusval = req.query.status;

         console.log(_id);
    var i = 0;
   var vm = this;
        vm.cms = null;
        vm.allcms = [];
   Cms.find(function (err, cms) {
     // docs is an array
       for(var i = 0; i < cms.length; i++){
            vm.allcms[i] = cms[i];
       }
        var cmsvalue = JSON.stringify(cms);
        var jsonObjectcms = JSON.parse(cmsvalue);

         Cms.findById({_id : req.query.id}, function (err, cms) {
              
             console.log(cms._id);

               Cms.update(
                 { _id: req.query.id },
                 { $set: { 
                   status: req.query.status} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

    return res.redirect('/cms');
 });

 }); 

app.post('/cms-update', isLoggedIn, function(req, res) {

   var Cms       = require('../app/models/cms');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

   var _id = req.body._id;
 //  console.log(_id);
    var i = 0;
   var vm = this;
        vm.cms = null;
        vm.allcms = [];
   Cms.find(function (err, cms) {
     // docs is an array
       for(var i = 0; i < cms.length; i++){
            vm.allcms[i] = cms[i];
       }
        var cmsvalue = JSON.stringify(cms);
        var jsonObjectcms = JSON.parse(cmsvalue);

         Cms.findById(_id, function (err, cmss) {
              
       //      console.log(user._id);

               Cms.update(
                 { _id: _id },
                 { $set: { 
                   title: req.body.title,
                   description: req.body.description,
                 datetime: dates} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

    return res.redirect('/cms');
 });

 }); 

  


 app.post('/category-update', isLoggedIn, function(req, res) {

   var Category       = require('../app/models/category');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

   var _id = req.body._id;
 //  console.log(_id);
    var i = 0;
   var vm = this;
        vm.category = null;
        vm.allcategorys = [];
   Category.find(function (err, categorys) {
     // docs is an array
       for(var i = 0; i < categorys.length; i++){
            vm.allcategorys[i] = categorys[i];
       }
        var categorysvalue = JSON.stringify(categorys);
        var jsonObjectcategorys = JSON.parse(categorysvalue);

         Category.findById(_id, function (err, category) {
              
       //      console.log(user._id);

               Category.update(
                 { _id: _id },
                 { $set: { 
                   categoryName: req.body.categoryName,
                 datetime: dates} 
              },
                 function (err, doc) {
                    // console.log(doc);        
                 });  

         });

    return res.redirect('/managecategory');
 });

 }); 


 app.post('/coupon-update', isLoggedIn, function(req, res) {

   var Coupon       = require('../app/models/coupon');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

   var _id = req.body._id;
   console.log(_id);
    var i = 0;
   var vm = this;
        vm.coupon = null;
        vm.allcoupons = [];
   Coupon.find(function (err, coupon) {
     // docs is an array
       for(var i = 0; i < coupon.length; i++){
            vm.allcoupons[i] = coupon[i];
       }
        var couponsvalue = JSON.stringify(coupon);
        var jsonObjectcoupons = JSON.parse(couponsvalue);

         Coupon.findById(_id, function (err, coupon) {
              
       //      console.log(user._id);

               Coupon.update(
                 { _id: _id },
                 { $set: { 
                   couponName : req.body.couponName,
                   couponCode : req.body.couponCode,
                   couponComission : req.body.couponComission,
                   startDate : req.body.startDate,
                   endDate : req.body.endDate,
                   status : "0",
                   datetime: dates} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

    return res.redirect('/managecoupon');
 });

 }); 




 app.post('/size-update', isLoggedIn, function(req, res) {

   var Size       = require('../app/models/size');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

   var _id = req.body._id;
 //  console.log(_id);
    var i = 0;
   var vm = this;
        vm.category = null;
        vm.allcategorys = [];
   Size.find(function (err, size) {
     // docs is an array
       for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];
       }
        var sizevalue = JSON.stringify(size);
        var jsonObjectsize = JSON.parse(sizevalue);

         Size.findById(_id, function (err, size) {
              
       //      console.log(user._id);

               Size.update(
                 { _id: _id },
                 { $set: { 
                   sizeName: req.body.sizeName,
                 datetime: dates} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

    return res.redirect('/managesize');
 });

 });


 app.post('/color-update', isLoggedIn, function(req, res) {

   var Color       = require('../app/models/color');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

   var _id = req.body._id;
 //  console.log(_id);
    var i = 0;
   var vm = this;
        vm.category = null;
        vm.allcategorys = [];
   Color.find(function (err, color) {
     // docs is an array
       for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];
       }
        var colorvalue = JSON.stringify(color);
        var jsonObjectcolor = JSON.parse(colorvalue);

         Color.findById(_id, function (err, color) {
              
       //      console.log(user._id);

               Color.update(
                 { _id: _id },
                 { $set: { 
                   colorName: req.body.colorName,
                 datetime: dates} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

    return res.redirect('/managecolor');
 });

 }); 



 app.post('/subcategory-update', isLoggedIn, function(req, res) {

   var SubCategory       = require('../app/models/subcategory');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

   var _id = req.body._id;
 //  console.log(_id);
    var i = 0;
   var vm = this;
        vm.subcategory = null;
        vm.allsubcategorys = [];
   SubCategory.find(function (err, subcategorys) {
     // docs is an array
       for(var i = 0; i < subcategorys.length; i++){
            vm.allsubcategorys[i] = subcategorys[i];
       }
        var subcategorysvalue = JSON.stringify(subcategorys);
        var jsonObjectsubcategorys = JSON.parse(subcategorysvalue);

         SubCategory.findById(_id, function (err, subcategory) {
              
       //      console.log(user._id);

               SubCategory.update(
                 { _id: _id },
                 { $set: { 
                  subcategoryName :req.body.subcategoryName,
                  categoryId :req.body.categoryId,
                  datetime: dates} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

    return res.redirect('/managesubcategory');
 });

 }); 



  app.post('/subcategory-insert', isLoggedIn, function(req, res) {
   //console.log(res);
   var User       = require('../app/models/user');
   var Category       = require('../app/models/category');
   var SubCategory       = require('../app/models/subcategory');

   var i = 0;
  var vm = this;
        vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      

         var set = {
             subcategoryName :req.body.subcategoryName.trim(),
             categoryId : req.body.categoryId,
             status : "0"
             };

           var subcategory = new SubCategory(set);
           subcategory.save(set,
            function (err, doc) {
             //console.log(doc);
            });
        
        
     return res.redirect('managesubcategory');

      });


   });  


  app.post('/second-subcategory-insert', isLoggedIn, function(req, res) {
   // console.log(res);
   var User       = require('../app/models/user');
   var Category       = require('../app/models/category');
   var SubCategory       = require('../app/models/subcategory');
   var secondSubCategory       = require('../app/models/secondsubcategory');

   var i = 0;
  var vm = this;
        vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      

         var set = {
             SecondsubcategoryName :req.body.SecondsubcategoryName.trim(),
             subcategoryId :req.body.subcategory,
             categoryId : req.body.categoryId,
             status : "0"
             };

           var secondsubcategory = new secondSubCategory(set);
           secondsubcategory.save(set,
            function (err, doc) {
             //console.log(doc);
            });
        
        
     return res.redirect('managesecondsubcategory');

      });


   });  


  app.post('/third-subcategory-insert', isLoggedIn, function(req, res) {
 
    var User = require('../app/models/user');
    var Category = require('../app/models/category');
    var SubCategory = require('../app/models/subcategory');
    var secondSubCategory = require('../app/models/secondsubcategory');
    var thirdSubCategory = require('../app/models/thirdsubcategory');
    var i = 0;
    var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.thirdSubCategory = null;
        vm.allthirdSubCategorys = [];

    var Website = require('../app/models/website');

      Website.find(function (err, website) {

    for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

    }

    var productvaluewebsite = JSON.stringify(website);
    var jsonObjectwebsite = JSON.parse(productvaluewebsite);
    var newwwsssee = req.body.CategoryName;

    for (var j = 0; j < newwwsssee.length; j++) {  

      if(req.body.parentdata == ""){

        var dattss = req.body.secondsubcatNamedata.trim();
        console.log("yes");
      } else {

        var dattss = req.body.parentdata.trim();
        console.log("no");

      }

      var set = {

        catNamedata :req.body.catNamedata.trim(),
        subcatNamedata :req.body.subcatNamedata.trim(),
        secondsubcatNamedata :req.body.secondsubcatNamedata.trim(),
        parentdata :dattss.trim(),
        role : req.body.role,
        CategoryName : newwwsssee[j].trim(),
        level : req.body.level,
        status : "0"

      };

        var thirdsubCategory = new thirdSubCategory(set);
          thirdsubCategory.save(set,function (err, doc) {
             //console.log(doc);
        });

      }
       
      return res.redirect('managethirdsubcategory');

    });

  });

 // });   




  // app.post('/fourth-subcategory-insert', isLoggedIn, function(req, res) {
  //  console.log(res);
  //  var User       = require('../app/models/user');
  //  var Category       = require('../app/models/category');
  //  var SubCategory       = require('../app/models/subcategory');
  //  var secondSubCategory       = require('../app/models/secondsubcategory');
  //  var thirdSubCategory       = require('../app/models/thirdsubcategory');

  //  var i = 0;
  // var vm = this;
  //       vm.website = null;
  //       vm.allwebsites = [];

  //    var Website       = require('../app/models/website');

  //    Website.find(function (err, website) {

  //    for(var i = 0; i < website.length; i++){
  //           vm.allwebsites[i] = website[i];

  //       }

  //      var productvaluewebsite = JSON.stringify(website);
  //      var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      

  //        var set = {
  //            ThirdsubcategoryName :req.body.ThirdsubcategoryName,
  //            secondsubcategoryId :req.body.secondsubcategory,
  //            subcategoryId :req.body.subcategory,
  //            categoryId : req.body.categoryId,
  //            status : "0"
  //            };

  //          var thirdsubCategory = new thirdSubCategory(set);
  //          thirdsubCategory.save(set,
  //           function (err, doc) {
  //            //console.log(doc);
  //           });
        
        
  //    return res.redirect('managethirdsubcategory');

  //     });


  //  }); 



    app.get('/manageslider',isLoggedIn, function(req, res) {
     var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];

         vm.website = null;
        vm.allwebsites = [];
        vm.slider = null;
        vm.allsliders = [];

     var Slider       = require('../app/models/slider');   
     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }
     
       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

        Slider.find(function (err, slider) {

     for(var i = 0; i < slider.length; i++){
            vm.allsliders[i] = slider[i];

        }
     
       var productvalueslider = JSON.stringify(slider);
       var jsonObjectslider = JSON.parse(productvalueslider);

    var Consult       = require('../app/models/consults');
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

           //console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
     
        res.render('manageslider.ejs', {loadwebsitedata : jsonObjectwebsite ,data: jsonObject,dataslider: jsonObjectslider, user : req.user ,totalreadconsultstatus : finalstatusconsult,});

      });

    });   
  
  });

  });   




    var fs =  require('fs');
    var express = require('express');
    var multer = require('multer');
    var upload = multer({ dest: 'public/uploads/slider'});
    app.post('/slider-add', upload.single('avatar'), function(req, res) {
      // var _id = req.user._id;
      // console.log(_id);
      var file = 'public/uploads/slider/' + req.files.avatar.originalFilename;
        var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];
    var User       = require('../app/models/user');
    var Slider       = require('../app/models/slider');
    var Consult       = require('../app/models/consults');
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

         //  console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
      fs.rename(req.files.avatar.path, file, function(err) {
        if (err) {
          console.log(err);
          res.send(500);
        } else {
     var imageName = req.files.avatar.originalFilename;
     
     var imagepath = {};
     imagepath['originalname'] = imageName;


      //    User.findById(_id, function (err, user) {
      

      //            User.update(
      //             { _id: _id },
      //             { $set: {img:imagepath} 
      //          },
      //             function (err, doc) {
      //                console.log(doc); 
        
      //             });  


      // });


            var set = {
             sliderImg:imagepath,
             status : "0"
             };

           var slider = new Slider(set);
           slider.save(set,
            function (err, doc) {
               console.log(err);
             console.log(doc);
            });


        }

      });
   
          return res.redirect("/manageslider");

  });

  
   });

     var fs =  require('fs');
    var express = require('express');
    var multer = require('multer');
    var upload = multer({ dest: 'public/uploads/slider'});
    app.post('/slider-update', upload.single('avatar'), function(req, res) {
        console.log(res);
        console.log(req.query.id);
        console.log(    req.params.id);
        console.log(req.body.id);
       var _id = req.query.id;
       console.log(_id);
      var file = 'public/uploads/slider/' + req.files.avatar.originalFilename;
        var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];
    var User       = require('../app/models/user');
    var Slider       = require('../app/models/slider');
    var Consult       = require('../app/models/consults');
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

         //  console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
      fs.rename(req.files.avatar.path, file, function(err) {
        if (err) {
          console.log(err);
          res.send(500);
        } else {
     var imageName = req.files.avatar.originalFilename;
     
     var imagepath = {};
     imagepath['originalname'] = imageName;


         Slider.findById(_id, function (err, user) {
      

                 Slider.update(
                  { _id: _id },
                  { $set: {sliderImg:imagepath ,  status : "0"} 
               },
                  function (err, doc) {
                     console.log(doc); 
        
                  });  


      });

        }

      });
   
          return res.redirect("/manageslider");

  });

  
   });
  
  app.get('/edit-slider', isLoggedIn, function(req, res) {
   var SubCategory       = require('../app/models/subcategory');
   var Category       = require('../app/models/category');
   var Slider       = require('../app/models/slider');

         var i = 0;
         var vm = this;
            vm.category = null;
            vm.allcategorys = [];
            vm.subcategory = null;
            vm.allsubcategorys = [];
            vm.website = null;
            vm.allwebsites = [];
            vm.slider = null;
            vm.allsliders = [];

     var Website       = require('../app/models/website');
         var slideid = req.query.id;
         console.log(slideid);

      Slider.find(function (err, slider) {
     // docs is an array
       for(var i = 0; i < slider.length; i++){
            vm.allsliders[i] = slider[i];
       }
       var productvaluesliders = JSON.stringify(slider);
        var jsonObjectsliders = JSON.parse(productvaluesliders);       

    

    SubCategory.find(function (err, subcategory) {
     // docs is an array
       for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategorys[i] = subcategory[i];
       }
       var productvaluesubcategorys = JSON.stringify(subcategory);
        var jsonObjectsubcategorys = JSON.parse(productvaluesubcategorys);       


   Category.find(function (err, category) {
     // docs is an array
       for(var i = 0; i < category.length; i++){
            vm.allcategorys[i] = category[i];
       }
       var productvaluecategorys = JSON.stringify(category);
        var jsonObjectcategorys = JSON.parse(productvaluecategorys);     

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      // console.log(jsonObject);

   
      res.render('edit-slider.ejs', { dataslider : jsonObjectsliders , loadwebsitedata : jsonObjectwebsite , datacategory: jsonObjectcategorys,  datasubcategory : jsonObjectsubcategorys, user : req.user, slideid : req.query.id});
 
      });


 });

  }); 

   }); 

    });    

  

  app.get('/sliderStatus', isLoggedIn, function(req, res) {

    var Slider = require('../app/models/slider');

    var dates=  new Date();

    var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

    var _id = req.query.id;
    var statusval = req.query.status;

         console.log(_id);
    var i = 0;
    var vm = this;
        vm.sliders = null;
        vm.allsliders = [];
    Slider.find(function (err, sliders) {
     // docs is an array
       for(var i = 0; i < sliders.length; i++){
            vm.allsliders[i] = sliders[i];
       }
        var slidersvalue = JSON.stringify(sliders);
        var jsonObjectsliders = JSON.parse(slidersvalue);

         Slider.findById({_id : req.query.id}, function (err, slider) {
              
           // console.log(slider._id);

               Slider.update(
                 { _id: req.query.id },
                 { $set: { 
                   status: req.query.status} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

      return res.redirect('/manageslider');

    });

  }); 
  

  app.get('/order-details', isLoggedIn, function(req, res) {

    var i = 0;
    var vm = this;
        vm.product = null;
        vm.allProducts = [];
        vm.products = null;
        var a = 0;
        vm.consult = null;
        vm.allconsults = [];
         var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;

      var Product       = require('../app/models/products');
      var Consult       = require('../app/models/consults');
      var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  


     Consult.find(function (err, consult) {
        // docs is an array
          for(var a = 0; a < consult.length; a++){
              vm.allconsults[a] = consult[a];
              if(req.user.user_role == 'user'){

                if(req.user._id == consult[a].user_id){

                      var totalreadstatusc = consult[a].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }
          }
        //  console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
   
         var productvalueconsult = JSON.stringify(consult);
         var jsonObjectconsult = JSON.parse(productvalueconsult);

   
      Product.find(function (err, product) {
      // docs is an array
        for(var i = 0; i < product.length; i++){
            vm.allProducts[i] = product[i];


        }
 
       var productvalues = JSON.stringify(product);
       var jsonObjects = JSON.parse(productvalues);

      var id = req.query.id;

        Product.findById(id, function (err, products) {
       
            for(var i = 0; i < products.length; i++){
            vm.allProducts[i] = products[i];
        }


       var productvalue = JSON.stringify(products);
       var jsonObject = JSON.parse(productvalue); 

     res.render('order-details.ejs', {loadwebsitedata : jsonObjectwebsite ,data: jsonObject,datas:jsonObjects,loadconsultdata: jsonObjectconsult,id:req.query.id,user : req.user,totalreadconsultstatus : finalstatusconsult,});


          });

        });

      });

    });

  });   


  app.get('/order', isLoggedIn, function(req, res) {

      var i = 0;
    var vm = this;
        vm.order = null;
        vm.allorders = [];
        vm.website = null;
        vm.allwebsites = [];
        var a = 0;
        vm.consult = null;
        vm.allconsults = [];
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;

      var Consult       = require('../app/models/consults');
      var Order       = require('../app/models/orders');
      var Website       = require('../app/models/website');

   
      Order.find(function (err, order) {
      // docs is an array
        for(var i = 0; i < order.length; i++){
            vm.allorders[i] = order[i];


        }
 
       var productvalue = JSON.stringify(order);
       var jsonObject = JSON.parse(productvalue);

       Website.find(function (err, website) {
      // docs is an array
        for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];


        }
 
       var websitevalue = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(websitevalue);

       
   
      res.render('order.ejs', {data: jsonObject, user : req.user, loadwebsitedata : jsonObjectwebsite});

      });

      }); 

  });


  app.get('/manageorder', isLoggedIn, function(req, res) {
    
    var i = 0;
    var vm = this;
        vm.order = null;
        vm.allorders = [];
        vm.user = null;
        vm.allusers = [];

    var Order = require('../app/models/orders');
    var User = require('../app/models/user');
    var Website = require('../app/models/website');

      User.find(function (err, user) {

    for(var i = 0; i < user.length; i++){
            vm.allusers[i] = user[i];
    }
 
    var uservalue = JSON.stringify(user);
    var jsonObjectuser = JSON.parse(uservalue);

      Website.find(function (err, website) {

    for(var i = 0; i < website.length; i++){
      
      vm.allwebsites[i] = website[i];

    }

    var productvaluewebsite = JSON.stringify(website);
    var jsonObjectwebsite = JSON.parse(productvaluewebsite);

      Order.find(function (err, order) {
    
    for(var i = 0; i < order.length; i++){
      
      vm.allorders[i] = order[i];

    }
  
        var productvalue = JSON.stringify(order);
        var jsonObject = JSON.parse(productvalue);
   
      res.render('manageorder.ejs', {loadwebsitedata : jsonObjectwebsite , data: jsonObject, datauser: jsonObjectuser, user : req.user});

        });

      });

    }); 

  }); 

      app.get('/admin', isLoggedIn, function(req, res) {

      var i = 0;
    var vm = this;
        vm.order = null;
        vm.allorders = [];
        vm.user = null;
        vm.allusers = [];
        vm.website = null;
        vm.allwebsites = [];

    var Order       = require('../app/models/orders');
    var User       = require('../app/models/user');
    var Website       = require('../app/models/website');


     User.find(function (err, user) {
      // docs is an array
        for(var i = 0; i < user.length; i++){
            vm.allusers[i] = user[i];


        }
 
       var uservalue = JSON.stringify(user);
       var jsonObjectuser = JSON.parse(uservalue);

        Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);


   
   
      res.render('admin.ejs', { datauser: jsonObjectuser, loadwebsitedata : jsonObjectwebsite , user : req.user});

      });

    });    

  });


  app.post('/ProductCatUpdate', function (req, res) {

    console.log(res);
      var Product  = require('../app/models/products');

      var _id= req.query.productid;

      var set = {ProductDealCat: req.body.ProductDealCat};
      
      Product.update({ _id: _id },{ $set: set },function (err, doc) {
                    console.log(doc);
                  }); 

           return res.redirect('/product');
         
  });



  app.get('/list-orderitem', isLoggedIn, function(req, res) {
    //  console.log(req.query.order_id);
    var Order       = require('../app/models/orders');
    var Orderitem       = require('../app/models/orderitem');
    var Website       = require('../app/models/website');
    var Product       = require('../app/models/products');
    var User       = require('../app/models/user');

    var i = 0;
    var vm = this;
        vm.order = null;
        vm.allorders = [];
    var a = 0;
        vm.consult = null;
        vm.allconsults = [];
        vm.product = null;
        vm.allproducts = [];
        vm.user = null;
        vm.allusers = [];
    var answerstatuscount = 0;
    var readstatuscount = 0;
    var totalreadconsultstatus = 0;


      var Consult       = require('../app/models/consults');
   
       Consult.find(function (err, consult) {
        // docs is an array
          for(var a = 0; a < consult.length; a++){
              vm.allconsults[a] = consult[a];
            if(req.user.user_role == 'user'){

                if(req.user._id == consult[a].user_id){

                      var totalreadstatusc = consult[a].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }
          }
          var finalstatusconsult = readstatuscount ;
         var productvalueconsult = JSON.stringify(consult);
         var jsonObjectconsult = JSON.parse(productvalueconsult);
       
   
        Order.find(function (err, order) {
      // docs is an array
        for(var i = 0; i < order.length; i++){
            vm.allorders[i] = order[i];


        }
 
       var productvalues = JSON.stringify(order);
       var jsonObjects = JSON.parse(productvalues);

       User.find(function (err, user) {
      // docs is an array
        for(var i = 0; i < user.length; i++){
            vm.allusers[i] = user[i];


        }
 
       var uservalues = JSON.stringify(user);
       var jsonObjectuser = JSON.parse(uservalues);

        Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);

        Product.find(function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproducts[i] = product[i];

        }

       var productvaluedata = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvaluedata);

       var orderid = req.query.order_id;
         //   console.log(orderid);
       Orderitem.find({orderid : req.query.order_id}, function (err, orderitem) {
 
       var productvalue = JSON.stringify(orderitem);
        var jsonObject = JSON.parse(productvalue);

       console.log(jsonObject);

       // console.log(jsonObject.productname);
   
      res.render('list-orderitem.ejs', { loaduserdata : jsonObjectuser , loadproductdata : jsonObjectproduct ,loadwebsitedata : jsonObjectwebsite , data: jsonObject, datas: jsonObjects, loadconsultdata : jsonObjectconsult , user : req.user , orderid : req.query.order_id , totalreadconsultstatus : finalstatusconsult,});

      });

        });



         });

         }); 

     });

       });

     });
    


  app.post('/registerinsert', function(req, res) {
   
      var User = require('../app/models/user');
           
      User.findOne({'username': req.body.username}, function(err, existingUser) {
             
        if(!existingUser){

          var regsitertoken = req.body.regsitertoken;
                 
            var setencode = {  

                regsitertoken : req.body.regsitertoken,
                user_role: req.body.user_role.trim(),
                firstName : req.body.firstName.trim(),
                username : req.body.username.trim(),
                cod : req.body.cod,
                local: {

                    email : req.body.email,
                         
                },

                img : {

                    originalname : "usericon.jpg"

                  },

                address : req.body.address.trim(),
                companyName : req.body.companyName.trim(),

                city : req.body.city.trim(),
                state : req.body.state,
                pinCode : req.body.pinCode.trim(),
                phone : req.body.phone.trim(),
                panNo : req.body.panNo.trim(),
                gsit : req.body.gsit.trim(),
                registerType : req.body.registerType.trim(),
                registerSet : req.body.registerSet.trim(),
                bankName : req.body.bankName.trim(),
                discount : req.body.discount.trim(),
                holderName : req.body.holderName.trim(),
                accountNumber : req.body.accountNumber.trim(),
                ifscCode : req.body.ifscCode.trim(),

                first_time_login : "False"  
                     
              };  

             
                var users = new User(setencode);

                  users.save(function(error, dataconsult){
             
              });

                req.flash('locals.successregister', 'Thanks for Registration');
               
              return res.redirect('/admin');  

          } else {

                req.flash('locals.errorregister', 'Already Registered');
               
              return res.redirect('/admin');

          }

      });
     
  });



 app.get('/verifyotp', function (req, res , done) {
   // console.log(res);
  var User       = require('../app/models/user');
    
    var username= req.query.user_name;

    var Otpdata = Math.floor(100000 + Math.random() * 900000);

            // console.log(Otpdata);

       // User.find({'username': req.query.user_name},function(err,user){
  
       //     console.log(user);     

         User.findOne({username : req.query.user_name}, function (err, user) {
              
              // console.log(user);

             if(user == null){

           req.flash('messaged', 'No User found');
      
           // return res.redirect('/wronglogin');  

             res.send(500); 

             }else{

               var set = { otp: Otpdata,regsitertoken : "" };

                 User.update(
                   { _id : user._id},
                   { $set: set 
                },function (err, doc) {

                     // res.send({ status:200 , Otpdata});


                     console.log(user.local.email); 

                     // console.log(Otpdata);  

                      // console.log(user[0].local.email);

      var http = require('http');
   
   http.get("http://api.msg91.com/api/sendhttp.php?country=91&sender=TESTIN&route=4&mobiles="+user.phone+"&authkey=285021AV9QZhdq5d29866b&message=Your OTP is "+Otpdata, function(res) {
   
   // console.log(res.statusCode);
    //this is headers
     // console.log(res.headers);
   res.setEncoding('utf8');
    res.on("data", function(chunk) {
       //this is body
       // console.log(chunk);
  });
}).on('error', function(e) {
   console.log("Got error: " + e.message);


 });

                  // var aws = require('aws-sdk');
                  //   aws.config.loadFromPath('newconfig.json');
                  //   aws.config.correctClockSkew = true
                  //   var ses = new aws.SES({apiVersion: '2010-12-01'});
                  //       var to = [user.local.email]
                  //       var from = 'jain.akash@novasoftcorps.com'
                  //      // console.log(to);  
                  //       ses.sendEmail( 
                  //         { 
                  //           Source: from, 
                  //           Destination: { ToAddresses: to },
                  //           Message: {
                  //             Subject: {
                  //               Data: 'OTP Verification'
                  //             },
                  //             Body: {
                  //               Html: {
                  //               Data: "<b>"+ "<p>Your OTP is "+Otpdata+"</p>" +"</b>" // html body',
                  //               }
                  //             }
                  //           }
                  //         }).promise();   
                       
                        });

                


      }
    
    });  

  });


  app.get('/checkpartId', function (req, res) {
   
    var Product = require('../app/models/products');
    
    var partId = req.query.partId;

    Product.findOne({partId : req.query.partId}, function (err, product) {
           
      if(product){

        res.send(product);

      } else {

        res.send(500);

      }

    });
  
  });


  app.get('/checkSupplierState', function (req, res) {
 
    var Supplier = require('../app/models/suppliers');
    
    var _id = req.query.supId;

      Supplier.findOne({"_id" : req.query.supId}, function (err, suppliers) {
              
        if(suppliers){

          res.send(suppliers);


        } else {


        }

      });  

  });

  app.get('/checkUserState', function (req, res) {
 
    var User = require('../app/models/user');
    
    var _id = req.query.supId;

      User.findOne({"_id" : req.query.supId}, function (err, suppliers) {
              
        if(suppliers){

          res.send(suppliers);


        } else {


        }

      });  

  });

  
  app.get('/verification_email', function (req, res , done) {
   
    var User = require('../app/models/user');
    
    var username= req.query.email;
   
    var usertoken= req.query.token;
  
    var ids={};

    User.findOne({'local.email': username},function(err,user){
  
      var _id = user._id;
        
        if(user.regsitertoken == usertoken){

        var set = { regsitertoken : ""};
            
      User.update({ _id: _id },{ $set: set },function (err, doc) {

             
            });

        return res.redirect('/home');         

        } else {
 
        }
          
        return res.redirect('/');    
          
      });

  });


  app.get('/purchase-list', isLoggedIn, function(req, res) {

    var Product = require('../app/models/products');
    var Purchase = require('../app/models/purchase');
    var Supplier = require('../app/models/suppliers');
    var i = 0;
    var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.purchase = null;
        vm.allpurchase = [];
        vm.supplier = null;
        vm.allsupplier = [];

       var Website  = require('../app/models/website');

        Website.find(function (err, website) {

        for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

        var productvaluewebsite = JSON.stringify(website);
        var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

        Purchase.find(function (err, purchase) {
       
        for(var i = 0; i < purchase.length; i++){
            vm.allpurchase[i] = purchase[i];
        }

        var purchasevalue = JSON.stringify(purchase);
        var jsonObjectpurchase = JSON.parse(purchasevalue);

        Supplier.find(function (err, supplier) {
       
        for(var i = 0; i < supplier.length; i++){
             vm.allsupplier[i] = supplier[i];
        }

        var suppliervalue = JSON.stringify(supplier);
        var jsonObjectsupplier = JSON.parse(suppliervalue);
 
        res.render('purchase-list.ejs', {  datasupplier : jsonObjectsupplier , loadwebsitedata : jsonObjectwebsite, datapurchase : jsonObjectpurchase , user : req.user});

        });

      });

    });

  });

  app.get('/product-list', isLoggedIn, function(req, res) {

    var Product  = require('../app/models/products');
    var ProductCategory       = require('../app/models/productCategory');
    var User  = require('../app/models/user');

    var i = 0;
    var vm = this;
    vm.users = null;
        vm.allusers = [];
        vm.products = null;
        vm.allproducts = [];
        vm.productCategory = null;
        vm.allproductCategory = [];
        vm.website = null;
        vm.allwebsites = [];


     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  


    User.find(function (err, users) {
      // docs is an array
        for(var i = 0; i < users.length; i++){
            vm.allusers[i] = users[i];
       }
       var productvalueusers = JSON.stringify(users);
        var jsonObjectusers = JSON.parse(productvalueusers);

       //console.log(jsonObjectusers);

      Product.find(function (err, products) {
      // docs is an array
        for(var i = 0; i < products.length; i++){
            vm.allproducts[i] = products[i];
       }
       var productvalue = JSON.stringify(products);
        var jsonObject = JSON.parse(productvalue);

         ProductCategory.find(function (err, productCategory) {
      // docs is an array
        for(var i = 0; i < productCategory.length; i++){
            vm.allproductCategory[i] = productCategory[i];
       }
       var productCategoryvalue = JSON.stringify(productCategory);
        var jsonObjectproductCategory = JSON.parse(productCategoryvalue);

       // console.log(jsonObject);

   
      res.render('product-list.ejs', {loadproductcategory : jsonObjectproductCategory, loadwebsitedata : jsonObjectwebsite, data: jsonObject, datauser: jsonObjectusers, user : req.user});

      });

       });


  });

 });

 });    



  app.get('/supplier-list', isLoggedIn, function(req, res) {
    var Product       = require('../app/models/products');
    var Supplier       = require('../app/models/suppliers');
    var User       = require('../app/models/user');

         var i = 0;
    var vm = this;
        vm.users = null;
        vm.allusers = [];
        vm.products = null;
        vm.allproducts = [];
        vm.website = null;
        vm.allwebsites = [];
        vm.supplier = null;
        vm.allsuppliers = [];


     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  


    User.find(function (err, users) {
      // docs is an array
        for(var i = 0; i < users.length; i++){
            vm.allusers[i] = users[i];
       }
       var productvalueusers = JSON.stringify(users);
        var jsonObjectusers = JSON.parse(productvalueusers);

       //console.log(jsonObjectusers);

      Product.find(function (err, products) {
      // docs is an array
        for(var i = 0; i < products.length; i++){
            vm.allproducts[i] = products[i];
       }
       var productvalue = JSON.stringify(products);
        var jsonObject = JSON.parse(productvalue);

       // console.log(jsonObject);

       Supplier.find(function (err, suppliers) {
      // docs is an array
        for(var i = 0; i < suppliers.length; i++){
            vm.allsuppliers[i] = suppliers[i];
       }
       var suppliervalue = JSON.stringify(suppliers);
        var jsonObjectsuppliers = JSON.parse(suppliervalue);


   
      res.render('supplier-list.ejs', { loadsupplierdata: jsonObjectsuppliers , loadwebsitedata : jsonObjectwebsite, data: jsonObject, datauser: jsonObjectusers, user : req.user});

      });


  });

 });

 }); 

 });    


 app.get('/edit-product', isLoggedIn, function(req, res) {

var Product = require('../app/models/products');
    var ProductCategory = require('../app/models/productCategory');
var User = require('../app/models/user');
    var Category = require('../app/models/category');
    var SubCategory  = require('../app/models/subcategory');
    var thirdSubCategory = require('../app/models/thirdsubcategory');
    var Unit = require('../app/models/unit');
    var Size = require('../app/models/size');
    var Color = require('../app/models/color');

    console.log(req.query);

    var jsonObjectnn = req.query;
    var keyCount  = Object.keys(jsonObjectnn).length;

     console.log(keyCount);

         var i = 0;
var vm = this;
vm.users = null;
        vm.allusers = [];
        vm.products = null;
        vm.allproducts = [];
        vm.website = null;
        vm.allwebsites = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.thirdsubcategory = null;
        vm.allthirdsubcategory = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.unit = null;
        vm.allunit = [];
        vm.productCategory = null;
        vm.allproductCategory = [];

     var Website = require('../app/models/website');

      Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize);

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor);



     Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory);

      SubCategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory);


      thirdSubCategory.find(function (err, thirdsubcategory) {

     for(var i = 0; i < thirdsubcategory.length; i++){
            vm.allthirdsubcategory[i] = thirdsubcategory[i];

        }

       var productvaluethirdsubcategory = JSON.stringify(thirdsubcategory);
       var jsonObjectthirdsubcategory = JSON.parse(productvaluethirdsubcategory);

        Website.find(function (err, website) {

        for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

        var productvaluewebsite = JSON.stringify(website);
        var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

        var productid = req.query.id;

    User.find(function (err, users) {
 
   for(var i = 0; i < users.length; i++){
            vm.allusers[i] = users[i];
        }

        var productvalueusers = JSON.stringify(users);
        var jsonObjectusers = JSON.parse(productvalueusers);

        Product.find(function (err, products) {
 
   for(var i = 0; i < products.length; i++){
            vm.allproducts[i] = products[i];
        }

        var productvalue = JSON.stringify(products);
        var jsonObject = JSON.parse(productvalue);

        Unit.find(function (err, unit) {
   
        for(var i = 0; i < unit.length; i++){
            vm.allunit[i] = unit[i];
        }

        var unitvalue = JSON.stringify(unit);
        var jsonObjectunit = JSON.parse(unitvalue);


        ProductCategory.find(function (err, productCategory) {
   
        for(var i = 0; i < productCategory.length; i++){
            vm.allproductCategory[i] = productCategory[i];
        }

        var productCategoryvalue = JSON.stringify(productCategory);
        var jsonObjectproductCategory = JSON.parse(productCategoryvalue);

   
      res.render('edit-product.ejs', { totalvariable : req.query , totalcountdds : keyCount ,loadproductCatgeory : jsonObjectproductCategory ,loadunitdata : jsonObjectunit ,loadthirdsubcategorydata : jsonObjectthirdsubcategory, loadsizedata: jsonObjectsize, loadcolordata : jsonObjectcolor,loadwebsitedata : jsonObjectwebsite, data: jsonObject, datauser: jsonObjectusers, user : req.user, productid : req.query.id , loadcategorydata : jsonObjectcategory, loadsubcategorydata : jsonObjectsubcategory , catname : req.query.cat , subcatname : req.query.subcat , secsubcatname : req.query.thirdsubcat , thirsubcatname : req.query.parentdatass,});


     });

      });    

  });

    });    

              });

            });

          });

        });

      });
   
    });

  }); 


  app.get('/edit-supplier', isLoggedIn, function(req, res) {
    var Product       = require('../app/models/products');
    var User       = require('../app/models/user');
    var Category       = require('../app/models/category');
    var SubCategory       = require('../app/models/subcategory');
    var thirdSubCategory       = require('../app/models/thirdsubcategory');
    var Supplier       = require('../app/models/suppliers');
    var Size       = require('../app/models/size');
    var Color       = require('../app/models/color');

         var i = 0;
    var vm = this;
        vm.users = null;
        vm.allusers = [];
        vm.products = null;
        vm.allproducts = [];
        vm.website = null;
        vm.allwebsites = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.thirdsubcategory = null;
        vm.allthirdsubcategory = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.supplier = null;
        vm.allsuppliers = [];

     var Website       = require('../app/models/website');

      Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 



     Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

      SubCategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 


      thirdSubCategory.find(function (err, thirdsubcategory) {

     for(var i = 0; i < thirdsubcategory.length; i++){
            vm.allthirdsubcategory[i] = thirdsubcategory[i];

        }

       var productvaluethirdsubcategory = JSON.stringify(thirdsubcategory);
       var jsonObjectthirdsubcategory = JSON.parse(productvaluethirdsubcategory); 



     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

        var productid = req.query.id;


    User.find(function (err, users) {
      // docs is an array
        for(var i = 0; i < users.length; i++){
            vm.allusers[i] = users[i];
       }
       var productvalueusers = JSON.stringify(users);
        var jsonObjectusers = JSON.parse(productvalueusers);

      // console.log(jsonObjectusers);

      Product.find(function (err, products) {
      // docs is an array
        for(var i = 0; i < products.length; i++){
            vm.allproducts[i] = products[i];
       }
       var productvalue = JSON.stringify(products);
        var jsonObject = JSON.parse(productvalue);

       // console.log(jsonObject);

        Supplier.find(function (err, suppliers) {
      // docs is an array
        for(var i = 0; i < suppliers.length; i++){
            vm.allsuppliers[i] = suppliers[i];
       }
       var suppliersvalue = JSON.stringify(suppliers);
        var jsonObjectsuppliers = JSON.parse(suppliersvalue);

   
      res.render('edit-supplier.ejs', {loadsupplierdata : jsonObjectsuppliers , loadthirdsubcategorydata : jsonObjectthirdsubcategory, loadsizedata: jsonObjectsize, loadcolordata : jsonObjectcolor,loadwebsitedata : jsonObjectwebsite, data: jsonObject, datauser: jsonObjectusers, user : req.user, productid : req.query.id , loadcategorydata : jsonObjectcategory, loadsubcategorydata : jsonObjectsubcategory , catname : req.query.cat , subcatname : req.query.subcat , secsubcatname : req.query.thirdsubcat , thirsubcatname : req.query.parentdatass,});


  });

   });      

   });

 });


});

});

 });

 });
 
 });

 });    

  app.get('/deleteslider',isLoggedIn, function(req, res) {
     var Slider       = require('../app/models/slider');
       Slider.remove({_id : req.query.id}, function (err, sliders) {
         console.log(sliders);    
    return res.redirect('/manageslider');
      });
  });

app.get('/deleteproductimage',isLoggedIn, function(req, res) {

    var Product = require('../app/models/products');
    var ProductCategory  = require('../app/models/productCategory');
     
      Product.find({"partId" : req.query.partId}, function (err, products) {

            var newdata = products[0].myimage;
       
      for(var i = 0; i < products[0].myimage.length; i++){
             
        if(products[0].myimage[i]._id == req.query.imageid){

            // console.log("products[0].myimage " + products[0].myimage);

          delete(products[0].myimage.splice(1,1));

          Product.updateMany({partId: req.query.partId},{ $pull: { "myimage" : { _id: req.query.imageid } } }, function (err, producks) {
             
         
            });
   
          }
         
        }
       
        return res.redirect('edit-product'+products[0].linkforedit+"&"+"id="+req.query.productId);      

    });
     
  });

   
app.get('/deleteproduct',isLoggedIn, function(req, res) {

    var Product = require('../app/models/products');
    var ProductCategory = require('../app/models/productCategory');
    var PurchaseItem = require('../app/models/purchaseItem');

      PurchaseItem.find({partId : req.query.id}, function(err, purchaseitemdata){
        
        if(purchaseitemdata.length > 0){


        } else {

          Product.findOne({_id : req.query.id}, function (err, products) {

              Product.remove({_id : req.query.id}, function (err, product) {

                ProductCategory.remove({_id : products.proCattId}, function (err, productcategory) {  
              
                  console.log(productcategory);

              return res.redirect('product-list');

            });

          });

        });

      }

    });
            
  });
  app.get('/deletepurchase',isLoggedIn, function(req, res) {
     var Purchase       = require('../app/models/purchase');
     var PurchaseItem       = require('../app/models/purchaseItem');
     var Product = require('../app/models/products');
       Purchase.remove({_id : req.query.id}, function (err, purchase) {
         // console.log(purchase); 
         PurchaseItem.find({purchaseId : req.query.id}, function (err, purchaseItem) {
         // console.log(purchaseItem); 
         var totalpurchasqty = 0;
           for(var i=0; i<purchaseItem.length; i++){
                // console.log(purchaseItem[i].finalqtydata);
                // console.log(purchaseItem[i].Quantity); 
                // console.log(purchaseItem[i].partId);  
               totalpurchasqty = purchaseItem[i].finalqtydata - purchaseItem[i].Quantity;
                 // console.log(totalpurchasqty);  

       Product.updateMany({partId: purchaseItem[i].part},{ $set: { quantity  : totalpurchasqty}}, function (err, docnn) {
         
          
        });  
             

        PurchaseItem.updateMany({ partId: purchaseItem[i].partId},{ $set: { finalqtydata  : totalpurchasqty}}, function (err, doc) {
          if(err){
            res.json(err);
          } else {
            console.log('update');

          }  

          PurchaseItem.remove({purchaseId : req.query.id}, function (err, purchaseItem) {
               // console.log(purchaseItem); 

          });      
         
        });
           }
   
    return res.redirect('purchase-list');
      });

     });    
  });



    var fs =  require('fs');
    var express = require('express');
    var multer = require('multer');
    var uploads = multer({ dest: 'public/uploads/product/'});

  app.post('/product-insert',isLoggedIn, function(req, res) {

    var Product = require('../app/models/products');
    var ProductCategory = require('../app/models/productCategory');
    var User = require('../app/models/user');
    var dates = new Date();
    var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();
    var token = Math.random();

         var vm = this;
        vm.website = null;
        vm.allwebsites = [];

     var Website = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);
       
      var myimage = {} ;

      var imageUniq = Math.floor(Date.now() / 100);
 
      var file = 'public/uploads/product/' + imageUniq + req.files.myimage.originalFilename;

    if(req.files.myimage.length == undefined){

      var imageName = req.files.myimage.originalFilename;

       var imagePathdata = 'http://poycard.com/uploads/product/'+ imageUniq + req.files.myimage.originalFilename;

       var imagepath = {};
       imagepath['originalname'] = imageName;
       imagepath['path'] = imagePathdata;

            var setProductCatef = {
            procatName: req.body.procatName,
            prosubcatName: req.body.prosubcatName,
            prosecsubcatName: req.body.prosecsubcatName,
            proparentdata: req.body.innerCat4,
            proparentdata1:req.body.innerCat5,
            proparentdata2:req.body.proparentdata,
            partId : req.body.partId,
            datetime: dates
           };

           var productCategory = new ProductCategory(setProductCatef);
        productCategory.save(setProductCatef, function (err, docss) {

          var set = {
           
            proCattId : docss._id,
            partId : req.body.partId.replace(/[\r\n]\s*/g, '\n'),
            partName : req.body.partName.replace(/[\r\n]\s*/g, '\n'),
            code: req.body.code.replace(/[\r\n]\s*/g, '\n'),
            mrp: req.body.mrp.replace(/[\r\n]\s*/g, '\n'),
            price:req.body.price.replace(/[\r\n]\s*/g, '\n'),
            stkQty:req.body.stkQty.replace(/[\r\n]\s*/g, '\n'),
            stockLocationOne : req.body.stockLocationOne.replace(/[\r\n]\s*/g, '\n'),
            stockLocationTwo : req.body.stockLocationTwo.replace(/[\r\n]\s*/g, '\n'),
            stockLocationThree : req.body.stockLocationThree.replace(/[\r\n]\s*/g, '\n'),
            shippricemp : req.body.shippricemp.replace(/[\r\n]\s*/g, '\n'),
            shippriceoutmp : req.body.shippriceoutmp.replace(/[\r\n]\s*/g, '\n'),
            dimension : req.body.dimension.replace(/[\r\n]\s*/g, '\n'),
            weight : req.body.weight.replace(/[\r\n]\s*/g, '\n'),
            tax:req.body.tax,
            setPriority : req.body.setPriority,
            quantity: req.body.quantity,
            brand: req.body.brand.replace(/[\r\n]\s*/g, '\n'),
            umState   : req.body.umState.replace(/[\r\n]\s*/g, '\n'),
            unitMeasure : req.body.unitMeasure.replace(/[\r\n]\s*/g, '\n'),
            altset : req.body.altset.replace(/[\r\n]\s*/g, '\n'),
            firstSet : req.body.firstSet.replace(/[\r\n]\s*/g, '\n'),
            // firstSetVal : req.body.firstSetVal,
            secondSet : req.body.secondSet.replace(/[\r\n]\s*/g, '\n'),
            linkforedit: req.body.linkforedit.replace(/[\r\n]\s*/g, '\n'),
            // secondSetVal : req.body.secondSetVal,
            productdescr : req.body.productdescr.replace(/[\r\n]\s*/g, '\n'),
            datetime: dates,
            myimage: imagepath,
            addProRole : req.user.user_role.replace(/[\r\n]\s*/g, '\n'),
            productStatus : "1",
            viewStatus : "1"
        };

       var products = new Product(set);
          products.save(set,
            function (err, doc) {
               // console.log(doc);
            });

        });



    } else {

        var sub_array = [];

        var imageUniq = Math.floor(Date.now() / 100);
 
      for(var k = 0; k < req.files.myimage.length; k++){

        var file = 'public/uploads/product/' + imageUniq + req.files.myimage[k].originalFilename;

          fs.rename(req.files.myimage[k].path, file , function(err) {

            if (err) {
         
               return res.redirect("product-list");

            }
       
      });

       var imageName = imageUniq + req.files.myimage[k].originalFilename;

       var imagePathdata = 'http://poycard.com/uploads/product/'+ imageUniq + req.files.myimage[k].originalFilename;

       var imagepath = {};
       imagepath['originalname'] = imageName;
       imagepath['path'] = imagePathdata;

        sub_array.push(imagepath);
             
       }    

          var setProductCatef = {
            procatName: req.body.procatName,
            prosubcatName: req.body.prosubcatName,
            prosecsubcatName: req.body.prosecsubcatName,
            proparentdata: req.body.innerCat4,
            proparentdata1:req.body.innerCat5,
            proparentdata2:req.body.proparentdata,
            partId : req.body.partId,
            datetime: dates
           };

           var productCategory = new ProductCategory(setProductCatef);
          productCategory.save(setProductCatef, function (err, docss) {

            var set = {
       
            proCattId : docss._id,
            partId : req.body.partId.replace(/[\r\n]\s*/g, '\n'),
            partName : req.body.partName.replace(/[\r\n]\s*/g, '\n'),
            code: req.body.code.replace(/[\r\n]\s*/g, '\n'),
            mrp: req.body.mrp.replace(/[\r\n]\s*/g, '\n'),
            price:req.body.price.replace(/[\r\n]\s*/g, '\n'),
            stkQty:req.body.stkQty.replace(/[\r\n]\s*/g, '\n'),
            stockLocationOne : req.body.stockLocationOne.replace(/[\r\n]\s*/g, '\n'),
            stockLocationTwo : req.body.stockLocationTwo.replace(/[\r\n]\s*/g, '\n'),
            stockLocationThree : req.body.stockLocationThree.replace(/[\r\n]\s*/g, '\n'),
            shippricemp : req.body.shippricemp.replace(/[\r\n]\s*/g, '\n'),
            shippriceoutmp : req.body.shippriceoutmp.replace(/[\r\n]\s*/g, '\n'),
            dimension : req.body.dimension.replace(/[\r\n]\s*/g, '\n'),
            weight : req.body.weight.replace(/[\r\n]\s*/g, '\n'),
            tax:req.body.tax,
            setPriority : req.body.setPriority,
            quantity: req.body.quantity,
            brand: req.body.brand.replace(/[\r\n]\s*/g, '\n'),
            umState   : req.body.umState.replace(/[\r\n]\s*/g, '\n'),
            unitMeasure : req.body.unitMeasure.replace(/[\r\n]\s*/g, '\n'),
            altset : req.body.altset.replace(/[\r\n]\s*/g, '\n'),
            firstSet : req.body.firstSet.replace(/[\r\n]\s*/g, '\n'),
            secondSet : req.body.secondSet.replace(/[\r\n]\s*/g, '\n'),
            productdescr : req.body.productdescr.replace(/[\r\n]\s*/g, '\n'),
            linkforedit: req.body.linkforedit,
            datetime: dates,
            myimage: sub_array,
            addProRole : req.user.user_role.replace(/[\r\n]\s*/g, '\n'),
            productStatus : "1",
            viewStatus : "1"
        };

       var products = new Product(set);
          products.save(set, function (err, doc) {
               // console.log(doc);
            });

          });
  
        }
   
      });

    return res.redirect('product-list');

  });


  app.get('/productview', isLoggedIn, function(req, res) {

    var Product       = require('../app/models/products');
      var ProductCategory       = require('../app/models/productCategory');
      var User       = require('../app/models/user');
      var Category       = require('../app/models/category');
      var SubCategory       = require('../app/models/subcategory');
      var thirdSubCategory       = require('../app/models/thirdsubcategory');
      var Unit = require('../app/models/unit');
      var Size = require('../app/models/size');
      var Color = require('../app/models/color');
      var jsonObjectnn = req.query;
      var keyCount  = Object.keys(jsonObjectnn).length;
      var i = 0;
      var vm = this;
      vm.users = null;
          vm.allusers = [];
          vm.products = null;
          vm.allproducts = [];
          vm.website = null;
          vm.allwebsites = [];
          vm.category = null;
          vm.allcategory = [];
          vm.subcategory = null;
          vm.allsubcategory = [];
          vm.thirdsubcategory = null;
          vm.allthirdsubcategory = [];
          vm.size = null;
          vm.allsize = [];
          vm.color = null;
          vm.allcolor = [];
          vm.unit = null;
          vm.allunit = [];
          vm.productCategory = null;
          vm.allproductCategory = [];

       var Website       = require('../app/models/website');

      Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 



       Category.find(function (err, category) {

       for(var i = 0; i < category.length; i++){
              vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

        SubCategory.find(function (err, subcategory) {

       for(var i = 0; i < subcategory.length; i++){
              vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 


        thirdSubCategory.find(function (err, thirdsubcategory) {

       for(var i = 0; i < thirdsubcategory.length; i++){
              vm.allthirdsubcategory[i] = thirdsubcategory[i];

        }

       var productvaluethirdsubcategory = JSON.stringify(thirdsubcategory);
       var jsonObjectthirdsubcategory = JSON.parse(productvaluethirdsubcategory); 



       Website.find(function (err, website) {

       for(var i = 0; i < website.length; i++){
              vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

        var productid = req.query.id;


      User.find(function (err, users) {
        // docs is an array
          for(var i = 0; i < users.length; i++){
            vm.allusers[i] = users[i];
       }

        var productvalueusers = JSON.stringify(users);
        var jsonObjectusers = JSON.parse(productvalueusers);

      // console.log(jsonObjectusers);

      Product.find(function (err, products) {
      // docs is an array
        for(var i = 0; i < products.length; i++){
            vm.allproducts[i] = products[i];
       }
       var productvalue = JSON.stringify(products);
        var jsonObject = JSON.parse(productvalue);

        Unit.find(function (err, unit) {
      // docs is an array
        for(var i = 0; i < unit.length; i++){
            vm.allunit[i] = unit[i];
       }
       var unitvalue = JSON.stringify(unit);
        var jsonObjectunit = JSON.parse(unitvalue);


       // console.log(jsonObject);

        ProductCategory.find(function (err, productCategory) {
      // docs is an array
        for(var i = 0; i < productCategory.length; i++){
            vm.allproductCategory[i] = productCategory[i];
       }
       var productCategoryvalue = JSON.stringify(productCategory);
        var jsonObjectproductCategory = JSON.parse(productCategoryvalue);

   
      res.render('productview.ejs', { totalvariable : req.query , totalcountdds : keyCount ,loadproductCatgeory : jsonObjectproductCategory ,loadunitdata : jsonObjectunit ,loadthirdsubcategorydata : jsonObjectthirdsubcategory, loadsizedata: jsonObjectsize, loadcolordata : jsonObjectcolor,loadwebsitedata : jsonObjectwebsite, data: jsonObject, datauser: jsonObjectusers, user : req.user, productid : req.query.id , loadcategorydata : jsonObjectcategory, loadsubcategorydata : jsonObjectsubcategory , catname : req.query.cat , subcatname : req.query.subcat , secsubcatname : req.query.thirdsubcat , thirsubcatname : req.query.parentdatass,});
                  });

                });    

              });

            });

          });

        });

      });

     });

    });
   
    });

  });  


app.post('/supplier-insert',isLoggedIn, function(req, res) {

    var Product = require('../app/models/products');
    var Supplier = require('../app/models/suppliers');
    var User = require('../app/models/user');
    var Category = require('../app/models/category');
    var SubCategory = require('../app/models/subcategory');
    var dates = new Date();
    var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

    var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];

     var Website = require('../app/models/website');

     Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory);

      SubCategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory);

     var Website = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);


            var set = {
            custId : req.body.custId,
            supName: req.body.supName,
            role: req.body.role,
            address: req.body.address,
            country: req.body.country,
            state : req.body.state,
            city : req.body.city,
            pincode: req.body.pincode,
            phone: req.body.phone,
            mobile: req.body.mobile,
            fax: req.body.fax,
            email:req.body.email,
            website:req.body.website,

            bankName : req.body.bankName,
            holderName : req.body.holderName,
            accountNumber : req.body.accountNumber,
            ifscCode : req.body.ifscCode,

            panNo : req.body.panNo,
            registerType : req.body.registerType,
            gsit:req.body.gsit,
            registerSet : req.body.registerSet,
            supplier_status : "0",
            datetime: strDate
        };

       var suppliers = new Supplier(set);
          suppliers.save(set,
            function (err, doc) {
               // console.log(doc);
            });
       
       return res.redirect('supplier');
   
        });

      });

    });

  });

  app.get('/purchase-listDate',isLoggedIn, function(req, res) {

     return res.redirect('/purchase-listDate');

  });

  app.get('/sales-listDate',isLoggedIn, function(req, res) {

     return res.redirect('/sales-listDate');

  });


  app.post('/dateSort',isLoggedIn, function(req, res) {

    var Website       = require('../app/models/website');
    var Product       = require('../app/models/products');
    var User = require('../app/models/user');
    var Category = require('../app/models/category');
    var SubCategory = require('../app/models/subcategory');
    var secondSubCategory = require('../app/models/secondsubcategory');
    var thirdSubCategory = require('../app/models/thirdsubcategory');
    var Size = require('../app/models/size');
    var Color       = require('../app/models/color');
    var Supplier  = require('../app/models/suppliers');
    var Unit = require('../app/models/unit');
    var Order = require('../app/models/orders');
    var Purchase   = require('../app/models/purchase');
    var PurchaseItem   = require('../app/models/purchaseItem');
    var i = 0;
    var vm = this;

       vm.website = null;
        vm.allwebsites = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.secondsubcategory = null;
        vm.allsecondsubcategory = [];
        vm.thirdSubCategory = null;
        vm.allthirdSubCategorys = [];
        vm.product = null;
        vm.allproducts = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.supplier = null;
        vm.allsupplier = [];
        vm.unit = null;
        vm.allunit = [];
        vm.order = null;
        vm.allorder = [];
        vm.purchase = null;
        vm.allpurchase = [];


       Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

      SubCategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 

      secondSubCategory.find(function (err, secondsubcategory) {

     for(var i = 0; i < secondsubcategory.length; i++){
            vm.allsecondsubcategory[i] = secondsubcategory[i];

        }

       var productvaluesecondsubcategory = JSON.stringify(secondsubcategory);
       var jsonObjectsecondsubcategory = JSON.parse(productvaluesecondsubcategory); 
     
     
     // console.log(jsonObjectsubcategorys);
        thirdSubCategory.find(function (err, thirdSubCategory) {
      // docs is an array
        for(var i = 0; i < thirdSubCategory.length; i++){
            vm.allthirdSubCategorys[i] = thirdSubCategory[i];


        }
 
       var thirdSubCategorysvalue = JSON.stringify(thirdSubCategory);
       var jsonObjectthirdSubCategorys = JSON.parse(thirdSubCategorysvalue);




       Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

        // console.log(jsonObjectsubcategorys);
        Product.find(function (err, product) {
      // docs is an array
        for(var i = 0; i < product.length; i++){
            vm.allproducts[i] = product[i];


        }
 
       var productsvalue = JSON.stringify(product);
       var jsonObjectproducts = JSON.parse(productsvalue);


     Supplier.find(function (err, supplier) {

     for(var i = 0; i < supplier.length; i++){
            vm.allsupplier[i] = supplier[i];

        }

       var productvaluesupplier = JSON.stringify(supplier);
       var jsonObjectsupplier = JSON.parse(productvaluesupplier); 


         Unit.find(function (err, unit) {

     for(var i = 0; i < unit.length; i++){
            vm.allunit[i] = unit[i];

        }

       var productvalueunit = JSON.stringify(unit);
       var jsonObjectunit = JSON.parse(productvalueunit); 


         Order.find(function (err, order) {

     for(var i = 0; i < order.length; i++){
            vm.allorder[i] = order[i];

        }

       var productvalueorder = JSON.stringify(order);
       var jsonObjectorder = JSON.parse(productvalueorder); 

       console.log(req.body.startDate);
       console.log(req.body.endDate);

    Purchase.find({$or: [{"datetime": {$gte: req.body.startDate, $lte: req.body.endDate}}]},function (err, purchase) {

           console.log(purchase);

          // return res.render('/purchase-listDate');


       res.render('purchase-listDate.ejs', { datapurchase : purchase , loadorderdata : jsonObjectorder , user : req.user ,loadunitdata: jsonObjectunit , datasupplier : jsonObjectsupplier, loadwebsitedata : jsonObjectwebsite, loadcategorydata : jsonObjectcategory, loadsubcategorydata : jsonObjectsubcategory, loadsecondsubcategorydata : jsonObjectsecondsubcategory, loadthirdsubcategorydata : jsonObjectthirdSubCategorys , loadsizedata:jsonObjectsize, loadcolordata : jsonObjectcolor, catname : req.query.cat , subcatname : req.query.subcat , secsubcatname : req.query.thirdsubcat , thirsubcatname : req.query.parentdatass, dataproduct : jsonObjectproducts,
 
            });
                              
                              });
                        
                            });
                  
                          });
                  
                        });   
                  
                      });
              
                    });
              
                  });
              
                });    

              });

          });    

      });

    });   

  });  

  app.get('/stockqtysearch',isLoggedIn, function(req, res) {

    var Purchase = require('../app/models/purchase');
      var Sales = require('../app/models/sales');
      var SalesItem  = require('../app/models/salesitem');
      var SalesReturn = require('../app/models/salesreturn');
      var PurchaseReturn = require('../app/models/purchasereturn');
      var PurchaseItem = require('../app/models/purchaseItem');
      var SalesItemReturn = require('../app/models/salesitemreturn');
      var PurchaseItemReturn = require('../app/models/purchaseItemreturn');
      var Website = require('../app/models/website');

      var i = 0;
      var vm = this;

      var partid = req.body.partid;
   
        vm.order = null;
        vm.allorder = [];
        vm.purchase = null;
        vm.allpurchase = [];
        vm.allpurchaseitemdata = [];
        vm.purchaseitemdata = null;

        vm.salesItemOpen = null;
        vm.allsalesItemOpen = [];

        vm.purchaseOpen = null;
        vm.allpurchaseOpen = [];

        vm.purchaseReturnOpen = null;
        vm.allpurchaseReturnOpen = [];

        vm.salesReturnOpen = null;
        vm.allsalesReturnOpen = [];    


        PurchaseItem.find({ $and: [{ "part" : req.query.partId },{ "datetime": { $lte : req.query.preDate }}]}, function(err, purchaseOpen){
           

          for(var i = 0; i < purchaseOpen.length; i++){
              vm.allpurchaseOpen[i] = purchaseOpen[i];

          }
 
            var purchaseOpenvalue = JSON.stringify(purchaseOpen);            
            var jsonObjectpurchaseOpen = JSON.parse(purchaseOpenvalue);
           
       
        PurchaseItemReturn.find({ $and: [{ "part" : req.query.partId },{ "datetime": { $lte : req.query.preDate }}]}, function(err, purchaseReturnOpen){
           

          for(var i = 0; i < purchaseReturnOpen.length; i++){
              vm.allpurchaseReturnOpen[i] = purchaseReturnOpen[i];

          }
 
            var purchaseReturnOpenvalue = JSON.stringify(purchaseReturnOpen);            
            var jsonObjectpurchaseReturnOpenvalue = JSON.parse(purchaseReturnOpenvalue);
           
        SalesItem.find({ $and: [{ "part" : req.query.partId },{ "datetime": { $lte : req.query.preDate }}]}, function(err, salesItemOpen){

           
          for(var i = 0; i < salesItemOpen.length; i++){
              vm.allsalesItemOpen[i] = salesItemOpen[i];

          }
 
        var salesItemOpenvalue = JSON.stringify(salesItemOpen);            
        var jsonObjectsalesItemOpenvalue = JSON.parse(salesItemOpenvalue);

     
        SalesItemReturn.find({ $and: [{ "part" : req.query.partId },{ "datetime": { $lte : req.query.preDate }}]}, function(err, salesReturnOpen){
           

          for(var i = 0; i < salesReturnOpen.length; i++){
              vm.allsalesReturnOpen[i] = salesReturnOpen[i];

          }
 
        var salesReturnOpenvalue = JSON.stringify(salesReturnOpen);            
        var jsonObjectsalesReturnOpenvalue = JSON.parse(salesReturnOpenvalue);
         

        Website.find(function (err, website) {

          for(var i = 0; i < website.length; i++){

           vm.allwebsites[i] = website[i];
     
        }

        var productvaluewebsite = JSON.stringify(website);
        var jsonObjectwebsite = JSON.parse(productvaluewebsite);

      PurchaseItem.find({ $and: [{ part : req.query.partId },{ "datetime": { $gte: req.query.firstdate }}, { "datetime": { $lte: req.query.lastdate }}]}, function (err, purchase) {
   
        SalesItem.find({ $and: [{ part : req.query.partId },{ "datetime": { $gte: req.query.firstdate }}, { "datetime": { $lte: req.query.lastdate }}]}, function (err, salesitem) {    
     
      PurchaseItemReturn.find({ $and: [{ part : req.query.partId },{ "datetime": { $gte: req.query.firstdate }}, { "datetime": { $lte: req.query.lastdate }}]}, function (err, purchaseReturn) {
   
        SalesItemReturn.find({ $and: [{ part : req.query.partId },{ "datetime": { $gte: req.query.firstdate }}, { "datetime": { $lte: req.query.lastdate }}]}, function (err, salesReturn) {

      res.render('stockqantityMonthSearch.ejs', { salesRetOpen : jsonObjectsalesReturnOpenvalue, salesOpen : jsonObjectsalesItemOpenvalue , purRetOpen : jsonObjectpurchaseReturnOpenvalue, purOpen : jsonObjectpurchaseOpen, loadwebsitedata : jsonObjectwebsite , purchasereturndata : purchaseReturn, salesreturndata : salesReturn, partid : req.query.partId , namee : req.query.namee, endDate : req.query.lastdate , startDate : req.query.firstdate , datasales : salesitem , datapurchase : purchase , user : req.user ,
               
                    });
                     
                  });

                  });  
                     
                });
                 
              });
                 
            });
                 
          });

         });
       
      });

    });

  });  

  app.post('/monthSearch', isLoggedIn, function(req, res) {

    var i = 0;
    var vm = this;

    vm.salesItemOpen = null;
    vm.allsalesItemOpen = [];

    vm.website = null;
    vm.allwebsites = [];

    vm.purchaseOpen = null;
    vm.allpurchaseOpen = [];
   
    vm.orderitem = null;
    vm.allorderitem = [];

    vm.purchaseReturnOpen = null;
    vm.allpurchaseReturnOpen = [];

    vm.salesReturnOpen = null;
    vm.allsalesReturnOpen = [];

    vm.purOpen = null;      
    vm.allpurOpen = [];  

    vm.purReOpen = null;
    vm.allpurReOpen = [];

    vm.salesOpen = null;
    vm.allsalesOpen = [];

    vm.salesRe = null;
    vm.allsalesRe = [];

    vm.user = null;
    vm.allusers = [];

    vm.suppliers = null;
    vm.allsuppliers = [];      

    var PurchaseItem = require('../app/models/purchaseItem');
    var SalesItem  = require('../app/models/salesitem');
    var PurchaseItemReturn = require('../app/models/purchaseItemreturn');
    var SalesItemReturn = require('../app/models/salesitemreturn');
    var Order = require('../app/models/orders');
    var Product = require('../app/models/products');
    var Orderitem = require('../app/models/orderitem');
    var Website = require('../app/models/website');
    var User = require('../app/models/user');
    var Supplier = require('../app/models/suppliers');

      PurchaseItem.find({ $and: [{ part : req.body.partid },{ "datetime": { $lte : req.body.startDate }}]}, function(err, purOpen){


        for(var i = 0; i < purOpen.length; i++){
         
          vm.allpurOpen[i] = purOpen[i];

        }
         
        var purOpenvalue = JSON.stringify(purOpen);                      
        var jsonPur = JSON.parse(purOpenvalue);
             
      PurchaseItemReturn.find({ $and: [{ part : req.body.partid },{ "datetime": { $lte : req.body.startDate }}]}, function(err, purReOpen){
 
        for(var i = 0; i < purReOpen.length; i++){
         
          vm.allpurReOpen[i] = purReOpen[i];

        }
         
        var purRe = JSON.stringify(purReOpen);            
        var jsonPurRe = JSON.parse(purRe);


      SalesItem.find({ $and: [{ part : req.body.partid },{ "datetime": { $lte : req.body.startDate }}]}, function(err, salesOpen){
         
        for(var i = 0; i < salesOpen.length; i++){
         
          vm.allsalesOpen[i] = salesOpen[i];

        }
         
        var salesvalue = JSON.stringify(salesOpen);            
        var jsonsales = JSON.parse(salesvalue);
     
      SalesItemReturn.find({ $and: [{ part : req.body.partid },{ "datetime": { $lte : req.body.startDate }}]}, function(err, salesRe){

         
        for(var i = 0; i < salesRe.length; i++){
                       
          vm.allsalesRe[i] = salesRe[i];

        }
           
        var salesRevalue = JSON.stringify(salesRe);            
        var jsonsalesRe = JSON.parse(salesRevalue);  
       
      PurchaseItem.find({ $and: [{ part : req.body.partid },{ "datetime": { $gte: req.body.startDate }}, { "datetime": { $lte: req.body.endDate }}]}).sort({datetime: 1}).exec(function(err, purchaseOpen) {

        for(var i = 0; i < purchaseOpen.length; i++){

          vm.allpurchaseOpen[i] = purchaseOpen[i];

        }
   
          var purchaseOpenvalue = JSON.stringify(purchaseOpen);  

          var jsonObjectpurchaseOpen = JSON.parse(purchaseOpenvalue);

      SalesItem.find({ $and: [{ part : req.body.partid },{ "datetime": { $gte: req.body.startDate }}, { "datetime": { $lte: req.body.endDate }}]}).sort({datetime: 1}).exec(function(err, salesItemOpen) {  

        for(var i = 0; i < salesItemOpen.length; i++){
                       
          vm.allsalesItemOpen[i] = salesItemOpen[i];
   
        }

          var salesItemOpenvalue = JSON.stringify(salesItemOpen);            
          var jsonObjectsalesItemOpenvalue = JSON.parse(salesItemOpenvalue);

                 
      PurchaseItemReturn.find({ $and: [{ part : req.body.partid },{ "datetime": { $gte: req.body.startDate }}, { "datetime": { $lte: req.body.endDate }}]}).sort({datetime: 1}).exec(function (err, purchaseReturnOpen) {

        for(var i = 0; i < purchaseReturnOpen.length; i++){
           
          vm.allpurchaseReturnOpen[i] = purchaseReturnOpen[i];

        }

          var purchaseReturnOpenvalue = JSON.stringify(purchaseReturnOpen);            
          var jsonObjectpurchaseReturnOpenvalue = JSON.parse(purchaseReturnOpenvalue);
   
        SalesItemReturn.find({ $and: [{ part : req.body.partid },{ "datetime": { $gte: req.body.startDate }}, { "datetime": { $lte: req.body.endDate }}]}).sort({datetime: 1}).exec(function (err, salesReturnOpen) {

            for(var i = 0; i < salesReturnOpen.length; i++){
                     
              vm.allsalesReturnOpen[i] = salesReturnOpen[i];

            }
         
            var salesReturnOpenvalue = JSON.stringify(salesReturnOpen);            
            var jsonObjectsalesReturnOpenvalue = JSON.parse(salesReturnOpenvalue);

        Orderitem.find({ $and: [{ productname : req.body.namee },{ "datetime": { $gte: req.body.startDate }}, { "datetime": { $lte: req.body.endDate }}]}, function (err, orderitem) {
           
            for(var i = 0; i < orderitem.length; i++){
                     
              vm.allorderitem[i] = orderitem[i];

            }

            var orderitemvalue = JSON.stringify(orderitem);
            var jsonOrderitem = JSON.parse(orderitemvalue);

          Website.find(function (err, website) {

            for(var i = 0; i < website.length; i++){
                   
              vm.allwebsites[i] = website[i];
               
            }

            var productvaluewebsite = JSON.stringify(website);
            var jsonObjectwebsite = JSON.parse(productvaluewebsite);

          Supplier.find(function (err, suppliers) {
          
            for(var i = 0; i < suppliers.length; i++){
                vm.allsuppliers[i] = suppliers[i];
            }

          var suppliersvalue = JSON.stringify(suppliers);
          var jsonObjectsuppliers = JSON.parse(suppliersvalue);

          User.find(function (err, user) {
           
            for(var i = 0; i < user.length; i++){
                vm.allusers[i] = user[i];
            }

          var uservalue = JSON.stringify(user);
          var jsonObjectuser = JSON.parse(uservalue);

        res.render('monthQuantity.ejs', { loadsupplierdata : jsonObjectsuppliers, datauser: jsonObjectuser, salesRet : jsonsalesRe , sales : jsonsales , purchaseRet : jsonPurRe , purchase: jsonPur , loadwebsitedata : jsonObjectwebsite , user : req.user, order : jsonOrderitem, purOpen : jsonObjectpurchaseOpen, salesOpen : jsonObjectsalesItemOpenvalue , purRetOpen : jsonObjectpurchaseReturnOpenvalue , salesRetOpen : jsonObjectsalesReturnOpenvalue , user : req.user, partId : req.body.partid, startD : req.body.startDate , lastD : req.body.endDate, name : req.body.namee });
                          

                            });

                          });

                        });

                      });

                    });

                  });

                });

              });

            });

         });

      });

    });

  });

  app.post('/dateSortSales',isLoggedIn, function(req, res) {

    var Website       = require('../app/models/website');
    var Product       = require('../app/models/products');
    var User       = require('../app/models/user');
    var Category       = require('../app/models/category');
    var SubCategory       = require('../app/models/subcategory');
    var secondSubCategory       = require('../app/models/secondsubcategory');
    var thirdSubCategory       = require('../app/models/thirdsubcategory');
    var Size       = require('../app/models/size');
    var Color       = require('../app/models/color');
    var Supplier       = require('../app/models/suppliers');
    var Unit       = require('../app/models/unit');
    var Order       = require('../app/models/orders');
    var Purchase   = require('../app/models/purchase');
    var PurchaseItem   = require('../app/models/purchaseItem');
    var Sales   = require('../app/models/sales');
    var SalesItem   = require('../app/models/salesitem');
    var i = 0;
    var vm = this;
       vm.website = null;
        vm.allwebsites = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.secondsubcategory = null;
        vm.allsecondsubcategory = [];
        vm.thirdSubCategory = null;
        vm.allthirdSubCategorys = [];
        vm.product = null;
        vm.allproducts = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.supplier = null;
        vm.allsupplier = [];
        vm.unit = null;
        vm.allunit = [];
        vm.order = null;
        vm.allorder = [];
        vm.purchase = null;
        vm.allpurchase = [];
        vm.sales = null;
        vm.allsales = [];

       Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

      SubCategory.find(function (err, subcategory) {

        for(var i = 0; i < subcategory.length; i++){

            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 

          secondSubCategory.find(function (err, secondsubcategory) {

        for(var i = 0; i < secondsubcategory.length; i++){
            vm.allsecondsubcategory[i] = secondsubcategory[i];

        }

        var productvaluesecondsubcategory = JSON.stringify(secondsubcategory);
        var jsonObjectsecondsubcategory = JSON.parse(productvaluesecondsubcategory); 
     
     
        thirdSubCategory.find(function (err, thirdSubCategory) {

      for(var i = 0; i < thirdSubCategory.length; i++){
            vm.allthirdSubCategorys[i] = thirdSubCategory[i];

      }
 
        var thirdSubCategorysvalue = JSON.stringify(thirdSubCategory);
        var jsonObjectthirdSubCategorys = JSON.parse(thirdSubCategorysvalue);

       Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

        // console.log(jsonObjectsubcategorys);
        Product.find(function (err, product) {
      // docs is an array
        for(var i = 0; i < product.length; i++){
            vm.allproducts[i] = product[i];


        }
 
       var productsvalue = JSON.stringify(product);
       var jsonObjectproducts = JSON.parse(productsvalue);


     Supplier.find(function (err, supplier) {

     for(var i = 0; i < supplier.length; i++){
            vm.allsupplier[i] = supplier[i];

        }

       var productvaluesupplier = JSON.stringify(supplier);
       var jsonObjectsupplier = JSON.parse(productvaluesupplier); 


         Unit.find(function (err, unit) {

     for(var i = 0; i < unit.length; i++){
            vm.allunit[i] = unit[i];

        }

       var productvalueunit = JSON.stringify(unit);
       var jsonObjectunit = JSON.parse(productvalueunit); 


         Order.find(function (err, order) {

     for(var i = 0; i < order.length; i++){
            vm.allorder[i] = order[i];

        }

       var productvalueorder = JSON.stringify(order);
       var jsonObjectorder = JSON.parse(productvalueorder); 

      Sales.find({$or: [{"datetime": {$gte: req.body.startDate, $lte: req.body.endDate}}]},function (err, sales) {
    
    res.render('sales-listDate.ejs', { datasales : sales , loadorderdata : jsonObjectorder , user : req.user ,loadunitdata: jsonObjectunit , datasupplier : jsonObjectsupplier, loadwebsitedata : jsonObjectwebsite, loadcategorydata : jsonObjectcategory, loadsubcategorydata : jsonObjectsubcategory, loadsecondsubcategorydata : jsonObjectsecondsubcategory, loadthirdsubcategorydata : jsonObjectthirdSubCategorys , loadsizedata:jsonObjectsize, loadcolordata : jsonObjectcolor, catname : req.query.cat , subcatname : req.query.subcat , secsubcatname : req.query.thirdsubcat , thirsubcatname : req.query.parentdatass, dataproduct : jsonObjectproducts,

            });
                              
                              });
                        
                            });
                  
                          });
                  
                        });   
                  
                      });
              
                    });
              
                  });
              
                });    

              });

          });    

      });

    });   

  });  

  app.get('/deleteCategory',isLoggedIn, function(req, res) {

    var ProductCategory = require('../app/models/productCategory');   
      var Category = require('../app/models/category');
      var SubCategory  = require('../app/models/subcategory');
      var secondSubCategory  = require('../app/models/secondsubcategory');
      var thirdSubCategory = require('../app/models/thirdsubcategory');
   
      Category.find({categoryName : req.query.categoryName}, function (err, category) {
  
          SubCategory.find({categoryId : req.query.categoryName}, function (err, subcategory) {
  
            if(subcategory.length > 0){

              return res.redirect('/managecategory'); 

            } else {

             ProductCategory.find({procatName: req.query.categoryName }, function (err, productcategoriesdata) {

              if(productcategoriesdata.length > 0){

                return res.redirect('/managecategory'); 

              } else {

                Category.remove({categoryName : req.query.categoryName}, function (err, category) {
               
                  return res.redirect('/managecategory'); 

              });

            } 

          });

        }

      });

    });

  });

  app.get('/deleteSubcategorie',isLoggedIn, function(req, res) {
    
    var SubCategory  = require('../app/models/subcategory');
    var secondSubCategory  = require('../app/models/secondsubcategory');
    var thirdSubCategory  = require('../app/models/thirdsubcategory');
    var ProductCategory = require('../app/models/productCategory');

     SubCategory.find({ $and: [{subcategoryName : req.query.subcategoryName},{categoryId : req.query.categoryId}]}, function (err, subcategory) {
  
        secondSubCategory.find({ $and: [{subcategoryId : req.query.subcategoryName},{categoryId : req.query.categoryId}]}, function (err, secondSubCategory) {

           if(secondSubCategory.length > 0){

            console.log("count length");

            return res.redirect('/managesubcategory');


           }else{

             console.log("not count length");

            ProductCategory.find({ $and: [{procatName: req.query.categoryId },{prosubcatName : req.query.subcategoryName}]}, function (err, productcategoriesdata) {

              if(productcategoriesdata.length > 0){
           
                console.log('true');

              return res.redirect('/managesubcategory');

               } else {

               SubCategory.remove({ $and: [{subcategoryName : req.query.subcategoryName},{categoryId : req.query.categoryId}]}, function (err, subcategory) {
                // console.log(makes); 
                console.log('false');

                return res.redirect('/managesubcategory');
             
              });
             
             }

            });


           }

          
          });
      
      });

  });
   
  app.get('/deletesecondSubCategory',isLoggedIn, function(req, res) {
    
    var ProductCategory = require('../app/models/productCategory');
      var thirdSubCategory  = require('../app/models/thirdsubcategory');
      var secondSubCategorys  = require('../app/models/secondsubcategory');

      secondSubCategorys.find({ $and: [{SecondsubcategoryName : req.query.SecondsubcategoryName},{subcategoryId : req.query.subcategoryId},{categoryId : req.query.categoryId}]}, function (err, secondSubCategory) {
      

        thirdSubCategory.find({ $and: [{secondsubcatNamedata : req.query.SecondsubcategoryName},{subcatNamedata : req.query.subcategoryId},{catNamedata : req.query.categoryId}]}, function (err, thirdsubcategory) {
      
          if(thirdsubcategory.length > 0){

             return res.redirect('/managesecondsubcategory');

          } else {

               ProductCategory.find({ $and: [{procatName: req.query.categoryId },{prosubcatName : req.query.subcategoryId},{prosecsubcatName : req.query.SecondsubcategoryName}]}, function (err, productcategoriesdata) {

                 if(productcategoriesdata.length > 0){

                      return res.redirect('/managesecondsubcategory');
                    
                  } else {

                      secondSubCategorys.remove({_id : secondSubCategory[0]._id}, function (err, secondsubcategory) {

                        return res.redirect('/managesecondsubcategory');
                          
                      });
 
                  }

          });
            
        }

      });

    });

  });

  app.get('/deletethirdSubCategory',isLoggedIn, function(req, res) {

    var a = 0;
    var vm = this;
    vm.productcategoriesdata = null;
    vm.allproductcategoriesdata = [];

    var ProductCategory = require('../app/models/productCategory');
    var thirdSubCategorys = require('../app/models/thirdsubcategory');
           
      thirdSubCategorys.find({"parentdata": req.query.CategoryName}, function (err, thirdsubcategorydata) {          

        if(thirdsubcategorydata == ""){

          // console.log("true");

            ProductCategory.find({$and: [{"proparentdata2": req.query.CategoryName},{"procatName" : req.query.catNamedata},{"prosubcatName" : req.query.subcatNamedata}, {"prosecsubcatName" : req.query.secondsubcatNamedata},{"proparentdata1" : req.query.parentdata}]}, function (err, productcategoriesdata) {          

                if(productcategoriesdata == ""){

                  // console.log("no part added on this category");

                    thirdSubCategorys.remove({"_id" : req.query.catids}, function (err, categoryName) {

                      // console.log("Delete form here if statements ");

                   });
                   
                  } else {

                    // console.log("part added on this category");

                  }

              });            
               
          } else {

            // console.log('this is else last last statements ');

            thirdSubCategorys.find({$and: [{"parentdata": req.query.CategoryName},{"catNamedata" : req.query.catNamedata}, {"subcatNamedata" : req.query.subcatNamedata},{"secondsubcatNamedata": req.query.secondsubcatNamedata}]}, function (err, thirddata){

              if (thirddata == "") {

                thirdSubCategorys.remove({"_id" : req.query.catids}, function (err, Name) {

                  // console.log("delete form here");

                });

              } else {

                  // console.log('Match Found');

              }

            });                    

          }
                           
      });
                   
    return  res.redirect('/managethirdsubcategory');
 
  });

  app.post('/purchase-insert',isLoggedIn, function(req, res) {
    
    var Product = require('../app/models/products');
    var Purchase  = require('../app/models/purchase');
    var PurchaseItem  = require('../app/models/purchaseItem');   
    var strDate = (new Date()).toISOString().split('T')[0]; 

    var vch = req.body.voucherNo;

    var supplierID = req.body.supId;
  
        for(var n = 0; n < req.body.part.length; n++){
      
    Product.updateMany({ partId: req.body.browser[n]},{ $set: { salecostprice  : req.body.salecostprice[n]}}, function (err, doc) {
                
         
        });

      } 
 
    var set = {

              supId : req.body.supId,
              GrandTotalAmnt : req.body.GrandTotalAmnt,
              SgstTotalAmnt: req.body.SgstTotalAmnt,
              CgstTotalAmnt: req.body.CgstTotalAmnt,
              IgstTotalAmnt: req.body.IgstTotalAmnt,
              FinalTotalAmnt : req.body.FinalTotalAmnt,
              TotalDecimalAmnt : req.body.TotalDecimalAmnt,
              EditAmount : req.body.EditAmount,
              invoiceNo : req.body.invoiceNo,
              voucherNo : req.body.voucherNo,
              SupDate : req.body.SupDate,
              orderId : req.body.orderId,
              purchaseStatus : "0",
              datetime : strDate

          };

       var purchase = new Purchase(set);
          purchase.save(set, function (err, doc) {

              for(var s = 0; s < req.body.part.length; s++){
      
      Product.updateMany({ partId: req.body.browser[s]},{ $set: { quantity  : req.body.finalqtydata[s]}}, function (err, doc) {
          
       
         
        });

      } 


        for(var n = 0; n < req.body.browser.length; n++){

       var sets = {           

                purchaseId : doc._id, 
                partId : req.body.part[n],
                SellPrice : req.body.SellPrice[n],
                TotalAmnt : req.body.TotalAmnt[n],
                Tax : req.body.Tax[n],
                Discount : req.body.Discount[n],
                Quantity : req.body.Quantity[n],
                Price : req.body.Price[n],
                Per : req.body.Per[n],
                DiscountAmnt : req.body.DiscountAmnt[n],
                Sgst : req.body.Sgst[n],
                Cgst : req.body.Cgst[n],
                Igst : req.body.Igst[n],
                TotalAmntData : req.body.TotalAmntData[n],
                salecostprice : req.body.salecostprice[n],
                finalqtydata : req.body.finalqtydata[n],
                part : req.body.browser[n],
                voucherNo : vch,
                supId : supplierID, 
                purchaseStatus : "0",
                datetime : strDate

             };

      var purchaseItem = new PurchaseItem(sets);

        purchaseItem.save(sets,function (err, docss) {

        PurchaseItem.updateMany({ partId: docss.partId},{ $set: { finalqtydata  : docss.finalqtydata}}, function (err, doc) {
              if(err){

                res.json(err);

              } else {

                // console.log('update');

              }        
             
            });

           });
                         
          }
              
      return res.redirect('purchase');
        
    });

  });


//////////////////  Sales Insert /////////////////////////////

  app.post('/sales-insert',isLoggedIn, function(req, res) {

    var Product = require('../app/models/products');
      var Sales = require('../app/models/sales');
      var SalesItem = require('../app/models/salesitem');
      var strDate = (new Date()).toISOString().split('T')[0]; 
      var vch = req.body.voucherNo;

      var usrid = req.body.supId;


        for(var n = 0; n < req.body.part.length; n++){
     
      Product.updateMany({ partId: req.body.browser[n]},{ $set: { salesperprice  : req.body.salesperprice[n]}}, function (err, doc) {
          
          if(err){

            res.json(err);

          } else {

            // console.log('update');
          }        
         
        });

      }
      
          var set = {

              supId : req.body.supId,
              GrandTotalAmnt : req.body.GrandTotalAmnt,
              SgstTotalAmnt: req.body.SgstTotalAmnt,
              CgstTotalAmnt: req.body.CgstTotalAmnt,
              IgstTotalAmnt: req.body.IgstTotalAmnt,
              FinalTotalAmnt : req.body.FinalTotalAmnt,
              TotalDecimalAmnt : req.body.TotalDecimalAmnt,
              EditAmount : req.body.EditAmount,
              invoiceNo : req.body.invoiceNo,
              voucherNo : req.body.voucherNo,
              SupDate : req.body.SupDate,
              orderId : req.body.orderId,
              salesStatus : "0",
              datetime : strDate

            };

       var sales = new Sales(set);

          sales.save(set, function (err, doc) {

        for(var n = 0; n < req.body.part.length; n++){

          var sets = {          

              salesId : doc._id,
              partId : req.body.part[n],
              SellPrice : req.body.SellPrice[n],
              TotalAmnt : req.body.TotalAmnt[n],
              Tax : req.body.Tax[n],
              Discount : req.body.Discount[n],
              Quantity : req.body.Quantity[n],
              Price : req.body.Price[n],
              Per : req.body.Per[n],
              DiscountAmnt : req.body.DiscountAmnt[n],
              Sgst: req.body.Sgst[n],
              Cgst: req.body.Cgst[n],
              Igst: req.body.Igst[n],
              TotalAmntData : req.body.TotalAmntData[n],
              salesperprice : req.body.salesperprice[n],
              part : req.body.browser[n],
              voucherNo : vch,
              userId : usrid,
              salesStatus : "0",
              datetime  : strDate

            };

          var salesItem = new SalesItem(sets);

            salesItem.save(sets,function (err, docss) {
                 
          });   

        }

      });

    return res.redirect('sales-list');
   
  });

//////////////////////////////  sales ///////////////////////////////////////////////



////////////////////////////////////  Sales Detail //////////////////////////////////////////

  app.get('/salesDetail', isLoggedIn, function(req, res) {
 
    var Product  = require('../app/models/products');
    var User = require('../app/models/user');
    var Sales = require('../app/models/sales');
    var Order = require('../app/models/orders');
    var SalesItem  = require('../app/models/salesitem');

    var i = 0;
    var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.product = null;
        vm.allproducts = [];
        vm.productDetail = null;
        vm.allproductDetails = [];
        vm.sales = null;
        vm.allsales = [];
        vm.salesitem = null;
        vm.allsalesitem = [];
        vm.order = null;
        vm.allorders = [];
        vm.user = null;
        vm.alluser = [];

      var Website  = require('../app/models/website');

      Website.find(function (err, website) {

    for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

        Product.find(function (err, product) {
   
        for(var i = 0; i < product.length; i++){
            vm.allproducts[i] = product[i];

        }
 
       var productsvalue = JSON.stringify(product);
       var jsonObjectproducts = JSON.parse(productsvalue);

       Order.find(function (err, order) {
    
        for(var i = 0; i < order.length; i++){
            vm.allorders[i] = order[i];


        }
 
       var ordervalue = JSON.stringify(order);
       var jsonObjectorder = JSON.parse(ordervalue);


       // console.log(jsonObjectsubcategorys);
        Product.find({_id : req.query.proid},function (err, productDetail) {
      // docs is an array
        for(var i = 0; i < productDetail.length; i++){
            vm.allproductDetails[i] = productDetail[i];
            

        }
 
       var productDetailsvalue = JSON.stringify(productDetail);
       var jsonObjectproductDetails = JSON.parse(productDetailsvalue);


         User.find(function (err, user) {

          for(var i = 0; i < user.length; i++){
                vm.alluser[i] = user[i];

            }

        var productvalueuser = JSON.stringify(user);
        var jsonObjectuser = JSON.parse(productvalueuser);

       Sales.find(function (err, sales) {
      // docs is an array
        for(var i = 0; i < sales.length; i++){
            vm.allsales[i] = sales[i];


        }
 
       var salesvalue = JSON.stringify(sales);
       var jsonObjectsales = JSON.parse(salesvalue);

        SalesItem.find(function (err, salesitem) {
      // docs is an array
        for(var i = 0; i < salesitem.length; i++){
            vm.allsalesitem[i] = salesitem[i];
            
          // console.log('hi its salesitem part');
          // console.log(salesitem[i])
        }
 
       var salesitemvalue = JSON.stringify(salesitem);
       var jsonObjectsalesitem = JSON.parse(salesitemvalue);


        res.render('salesDetail.ejs', { loaduserdata : jsonObjectuser, loadorderdata  : jsonObjectorder,  supId : req.query.id , datasalesitem : jsonObjectsalesitem , datasales : jsonObjectsales , loadproductdetail : jsonObjectproductDetails, user : req.user , loadwebsitedata : jsonObjectwebsite, dataproduct : jsonObjectproducts,
         
                  });

                });

              });

            });

          });  

        });  

      });

    });

  });
 
///////////////////////////////////////////////////////////


  app.get('/sales-invoice', isLoggedIn, function(req, res) {

    var salesId = req.query.proid;
    var catname = req.query.cat;
    var subcatname = req.query.subcat;
    var secsubcatname = req.query.thirdsubcat;
    var thirsubcatname = req.query.parentdatass;
    var supId = req.query.id;

    var Product       = require('../app/models/products');
    var User       = require('../app/models/user');
    var Category       = require('../app/models/category');
    var SubCategory       = require('../app/models/subcategory');
    var secondSubCategory       = require('../app/models/secondsubcategory');
    var thirdSubCategory       = require('../app/models/thirdsubcategory');
    var Size       = require('../app/models/size');
    var Color       = require('../app/models/color');
    var Supplier       = require('../app/models/suppliers');
    var Sales       = require('../app/models/sales');
    var Unit       = require('../app/models/unit');
    var Order       = require('../app/models/orders');
    var SalesItem   = require('../app/models/salesitem');

      var i = 0;
    var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.secondsubcategory = null;
        vm.allsecondsubcategory = [];
        vm.thirdSubCategory = null;
        vm.allthirdSubCategorys = [];
        vm.product = null;
        vm.allproducts = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.productDetail = null;
        vm.allproductDetails = [];
        vm.supplier = null;
        vm.allsuppliers = [];
        vm.unit = null;
        vm.allunit = [];
        vm.sales = null;
        vm.allsales = [];
        vm.salesitem = null;
        vm.allsalesitem = [];
        vm.order = null;
        vm.allorders = [];

        var Website       = require('../app/models/website');

      Category.find(function (err, category) {


      for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

        var productvaluecategory = JSON.stringify(category);
        var jsonObjectcategory = JSON.parse(productvaluecategory); 

        SubCategory.find(function (err, subcategory) {

      for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 

      secondSubCategory.find(function (err, secondsubcategory) {

      for(var i = 0; i < secondsubcategory.length; i++){
            vm.allsecondsubcategory[i] = secondsubcategory[i];

        }

       var productvaluesecondsubcategory = JSON.stringify(secondsubcategory);
       var jsonObjectsecondsubcategory = JSON.parse(productvaluesecondsubcategory); 
     
     
        // console.log(jsonObjectsubcategorys);
        thirdSubCategory.find(function (err, thirdSubCategory) {
       // docs is an array
        for(var i = 0; i < thirdSubCategory.length; i++){
            vm.allthirdSubCategorys[i] = thirdSubCategory[i];


        }
 
        var thirdSubCategorysvalue = JSON.stringify(thirdSubCategory);
        var jsonObjectthirdSubCategorys = JSON.parse(thirdSubCategorysvalue);




        Size.find(function (err, size) {
 
      for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

        var productvaluesize = JSON.stringify(size);
        var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

      for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

        var productvaluecolor = JSON.stringify(color);
        var jsonObjectcolor = JSON.parse(productvaluecolor); 

      Website.find(function (err, website) {

      for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

        // console.log(jsonObjectsubcategorys);
        Product.find(function (err, product) {
      // docs is an array
        for(var i = 0; i < product.length; i++){
            vm.allproducts[i] = product[i];
            
            // console.log('this is productId');
            // console.log(product[i]);

        }
 
       var productsvalue = JSON.stringify(product);
       var jsonObjectproducts = JSON.parse(productsvalue);

       Unit.find(function (err, unit) {
      // docs is an array
        for(var i = 0; i < unit.length; i++){
            vm.allunit[i] = unit[i];


        }
 
       var unitvalue = JSON.stringify(unit);
       var jsonObjectunit = JSON.parse(unitvalue);



       Order.find(function (err, order) {
      // docs is an array
        for(var i = 0; i < order.length; i++){
            vm.allorders[i] = order[i];


        }
 
       var ordervalue = JSON.stringify(order);
       var jsonObjectorder = JSON.parse(ordervalue);


       // console.log(jsonObjectsubcategorys);
        Product.find({_id : req.query.proid},function (err, productDetail) {
      // docs is an array
        for(var i = 0; i < productDetail.length; i++){
            vm.allproductDetails[i] = productDetail[i];
            

        }
 
       var productDetailsvalue = JSON.stringify(productDetail);
       var jsonObjectproductDetails = JSON.parse(productDetailsvalue);


      Supplier.find(function (err, supplier) {
      // docs is an array
        for(var i = 0; i < supplier.length; i++){
            vm.allsuppliers[i] = supplier[i];


        }
 
       var suppliersvalue = JSON.stringify(supplier);
       var jsonObjectsuppliers = JSON.parse(suppliersvalue);

       Sales.find(function (err, sales) {
      // docs is an array
        for(var i = 0; i < sales.length; i++){
            vm.allsales[i] = sales[i];


        }
 
       var salesvalue = JSON.stringify(sales);
       var jsonObjectsales = JSON.parse(salesvalue);

        SalesItem.find(function (err, salesitem) {
      // docs is an array
        for(var i = 0; i < salesitem.length; i++){
            vm.allsalesitem[i] = salesitem[i];
            
          // console.log('hi its salesitem part');
          // console.log(salesitem[i])
        }
 
       var salesitemvalue = JSON.stringify(salesitem);
       var jsonObjectsalesitem = JSON.parse(salesitemvalue);



        res.render('sales-invoice.ejs', { loadorderdata  : jsonObjectorder,  supId : req.query.id ,loadunitdata : jsonObjectunit ,datasalesitem : jsonObjectsalesitem , datasales : jsonObjectsales , loadsupplierdata : jsonObjectsuppliers,  loadproductdetail : jsonObjectproductDetails, user : req.user , loadwebsitedata : jsonObjectwebsite, loadcategorydata : jsonObjectcategory, loadsubcategorydata : jsonObjectsubcategory, loadsecondsubcategorydata : jsonObjectsecondsubcategory, loadthirdsubcategorydata : jsonObjectthirdSubCategorys , loadsizedata:jsonObjectsize, loadcolordata : jsonObjectcolor, catname : req.query.cat , subcatname : req.query.subcat , secsubcatname : req.query.thirdsubcat , thirsubcatname : req.query.parentdatass, dataproduct : jsonObjectproducts,
    
      
        });

        });

       });

      });

      });  

      });  

      });

      });

     });

   });    

            });

          });    

        });

      });   

    }); 

  });  

  app.get('/edit-sales', isLoggedIn, function(req, res) {

    var Product = require('../app/models/products');
    var User  = require('../app/models/user');
    var Supplier   = require('../app/models/suppliers');
    var Sales  = require('../app/models/sales');
    var SalesItem  = require('../app/models/salesitem');
    var Order  = require('../app/models/orders');

    var i = 0;
    var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.product = null;
        vm.allproducts = [];
        vm.productDetail = null;
        vm.allproductDetails = [];
        vm.supplier = null;
        vm.allsuppliers = [];
        vm.sales = null;
        vm.allsales = [];
        vm.salesitem = null;
        vm.allsalesitem = [];
        vm.order = null;
        vm.allorders = [];

       var Website = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

      Product.find({},function (err, product) {
      
        for(var i = 0; i < product.length; i++){

            vm.allproducts[i] = product[i];

        }
 
      var clean = product.filter((product, index, self) => index === self.findIndex((t) => (t.partId === product.partId)));
      var productsvalue = JSON.stringify(clean);
      var jsonObjectproducts = JSON.parse(productsvalue);

        User.find(function (err, user) {

     for(var i = 0; i < user.length; i++){
            vm.alluser[i] = user[i];

       }

       var  productvalueuser = JSON.stringify(user);
       var jsonObjectuser = JSON.parse(productvalueuser); 

        Product.find({_id : req.query.proid},function (err, productDetail) {
   
        for(var i = 0; i < productDetail.length; i++){
            vm.allproductDetails[i] = productDetail[i];


        }
 
        var productDetailsvalue = JSON.stringify(productDetail);
        var jsonObjectproductDetails = JSON.parse(productDetailsvalue);

        Supplier.find(function (err, supplier) {
     
        for(var i = 0; i < supplier.length; i++){
            vm.allsuppliers[i] = supplier[i];


        }
 
       var suppliersvalue = JSON.stringify(supplier);
       var jsonObjectsuppliers = JSON.parse(suppliersvalue);

        Sales.find(function (err, sales) {
    
        for(var i = 0; i < sales.length; i++){
            vm.allsales[i] = sales[i];


        }
 
        var salesvalue = JSON.stringify(sales);
        var jsonObjectsales = JSON.parse(salesvalue);

        SalesItem.find(function (err, salesitem) {
     
        for(var i = 0; i < salesitem.length; i++){
            vm.allsalesitem[i] = salesitem[i];


        }
 
        var salesitemvalue = JSON.stringify(salesitem);             
        var jsonObjectsalesitem = JSON.parse(salesitemvalue);

        Order.find(function (err, order) {
    
        for(var i = 0; i < order.length; i++){
            vm.allorders[i] = order[i];


        }
 
        var ordervalue = JSON.stringify(order);             
        var jsonObjectorder = JSON.parse(ordervalue);

        res.render('edit-sales.ejs', {loaduserdata : jsonObjectuser , loadorderdata : jsonObjectorder , supId : req.query.id , datasales : jsonObjectsales , datasalesdetail : jsonObjectsalesitem, loadsupplierdata : jsonObjectsuppliers,  loadproductdetail : jsonObjectproductDetails, user : req.user , loadwebsitedata : jsonObjectwebsite, dataproduct : jsonObjectproducts,
    
             
                    });

                  });

                });

              });

            });

          });

        });  

      });  

    });

  });

//////////////////////// delete sales ////////////////////////////////

  app.get('/deletesales',isLoggedIn, function(req, res) {

    var Sales  = require('../app/models/sales');

     var SalesItem  = require('../app/models/salesitem');

        Sales.remove({_id : req.query.id}, function (err, sales) {

          SalesItem.remove({salesId : req.query.id}, function (err, salesitem) {

        return res.redirect('sales-list');

      });

    });

  });      

  app.get('/sales-list', isLoggedIn, function(req, res) {

    var Sales = require('../app/models/sales');
    var User = require('../app/models/user');
    var i = 0;
    var vm = this;
    vm.users = null;
        vm.website = null;
        vm.allwebsites = [];
        vm.allusers = [];
        vm.sales = null;
        vm.allsales = [];

        var Website  = require('../app/models/website');

        Website.find(function (err, website) {

        for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

        var productvaluewebsite = JSON.stringify(website);
        var jsonObjectwebsite = JSON.parse(productvaluewebsite);

       User.find(function (err, users) {
     
        for(var i = 0; i < users.length; i++){
            vm.allusers[i] = users[i];
        }

        var productvalueusers = JSON.stringify(users);
        var jsonObjectusers = JSON.parse(productvalueusers);

        Sales.find(function (err, sales) {
    
        for(var i = 0; i < sales.length; i++){
            vm.allsales[i] = sales[i];
        }

        var salesvalue = JSON.stringify(sales);
        var jsonObjectsales = JSON.parse(salesvalue);
   
      res.render('sales-list.ejs', { loadwebsitedata : jsonObjectwebsite, datasales : jsonObjectsales , datauser: jsonObjectusers, user : req.user});
        
        });

      });

    });

  });  


    var upload = require("express-fileupload");
    var csvtojson = require("csvtojson");
    let csvData = "test";
    
  app.post("/filedd", (req, res) => {

    var Product = require('../app/models/products');
 
    var multiparty = require('multiparty');
    var form = new multiparty.Form();
    var fs = require('fs');

    var file = 'public/uploads/csv/' + req.files.ufile.originalFilename;


     fs.rename(req.files.ufile.path, file, function(err) {
        if (err) {
          console.log(err);

        } else {
    
     var imageName = req.files.ufile.originalFilename;
     var newpath = 'http://localhost:2222/uploads/csv/'+ imageName;

        var  products  = []
    var csv = require("fast-csv");
    var dates=  new Date();


    var csvfile = 'public/uploads/csv/' + req.files.ufile.originalFilename;

    var stream = fs.createReadStream(csvfile);

      var i = 0;
    
    var csvStream = csv().on("data", function(data){

        // console.log(i);
        // console.log(data);

         var item = new Product({

          procatName : data[0],
          prosubcatName : data[1],
          prosecsubcatName : data[2],
          proparentdata : data[3],
          partName: data[4],
          code: data[5],
          mrp: data[6],
          price: data[7],
          stkQty:data[8], 
          tax : data[9],
          brand : data[10],
          productdescr : data[11],
          datetime : dates,
          addProRole : "admin",
          productStatus : "1",
          viewStatus : "1",
          myimage : {
              "originalname" : "page-01_img13.jpg",
              "path" : "http://poycard.com/uploads/product/page-01_img13.jpg"
         },
         stockLocationOne : data[12],
         stockLocationTwo : data[13],
         stockLocationThree : data[14]           
         });
         

    if(i >= 1){

          item.save(function(error){
            // console.log(item);
              if(error){
                   throw error;
              }
          }); 

     }
      i++;
  
    }).on("end", function(){
          console.log(" End of file import");
    });
  
     stream.pipe(csvStream);

   // res.json({success : "Data imported successfully.", status : 200});
   
            return res.redirect('product-list');

     // var imagepath = {};
     // imagepath['originalname'] = imageName;
     // imagepath['path'] = newpath;
        
        }

        // console.log(imagepath);

      });

  });


app.get('/import', function(req, res, next) {
  // console.log(res);

  var Product       = require('../app/models/products');

    var  products  = []
    var csv = require("fast-csv");
    var dates=  new Date();


    var csvfile = "public/uploads/csv/productList.csv";

    var stream = fs.createReadStream(csvfile);

      var i = 0;
    
    var csvStream = csv().on("data", function(data){

        console.log(i);
        console.log(data);

         var item = new Product({

          procatName : data[0],
          prosubcatName : data[1],
          prosecsubcatName : data[2],
          proparentdata : data[3],
          partName: data[4],
          code: data[5],
          mrp: data[6],
          price: data[7],
          stkQty:data[8], 
          tax : data[9],
          brand : data[10],
          productdescr : data[11],
          datetime : dates,
          addProRole : "admin",
          productStatus : "1",
          viewStatus : "1",
          myimage : {
              "originalname" : "page-01_img13.jpg",
              "path" : "http://poycard.com/uploads/product/page-01_img13.jpg"
         },
         stockLocationOne : data[12],
         stockLocationTwo : data[13],
         stockLocationThree : data[14]           
         });
         

  if(i >= 1){

          item.save(function(error){
            // console.log(item);
              if(error){
                   throw error;
              }
          }); 

     }
      i++;
  
    }).on("end", function(){
          console.log(" End of file import");
    });
  
    stream.pipe(csvStream);

    res.json({success : "Data imported successfully.", status : 200});
     
  });



var fs =  require('fs');
      var express = require('express');
      var multer = require('multer');
      var uploads = multer({ dest: 'public/uploads/product/'});

  app.post('/product-insert-csv',isLoggedIn, function(req, res) {
    var Product       = require('../app/models/products');
    var User       = require('../app/models/user');
    var Category       = require('../app/models/category');
    var SubCategory       = require('../app/models/subcategory');
    var dates=  new Date();
    var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();
 
    var token = Math.random();

    console.log(req.query.pullData);

     console.log(req.body);

    // console.log(req.user.user_role);

         var vm = this;
       vm.website = null;
        vm.allwebsites = [];
         vm.category = null;
        vm.allcategory = [];
          vm.subcategory = null;
        vm.allsubcategory = [];

     var Website       = require('../app/models/website');

     Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

      SubCategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 


     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 


       // var myimage = {} ;


       // console.log(req.files.myimage.originalFilename);

     // var file = 'public/uploads/product/' + req.files.myimage.originalFilename;


     //    fs.rename(req.files.myimage.path, file, function(err) {
     //    if (err) {
     //      console.log(err);
     //      res.send(500);
     //    } else {

       // if(req.files.myimage.length == undefined){

     // var imageName = req.files.myimage.originalFilename;

     //   var imagePathdata = 'http://poycard.com/uploads/product/'+req.files.myimage.originalFilename;

     //   var imagepath = {};
     //   imagepath['originalname'] = imageName;
     //   imagepath['path'] = imagePathdata;

        // sub_array.push(imagepath);
        // console.log(imagepath);

        if(req.user.user_role == "admin"){

            var set = {
            procatName: req.body.procatName,
            prosubcatName: req.body.prosubcatName,
            prosecsubcatName: req.body.prosecsubcatName,
            proparentdata: req.body.proparentdata,
            partName : req.body.partName,
            // avilability: req.body.avilability,
            // quantity: req.body.quantity,
            code: req.body.code,
            mrp: req.body.mrp,
            price:req.body.price,
            stkQty:req.body.stkQty,
            stockLocationOne : req.body.stockLocationOne,
            stockLocationTwo : req.body.stockLocationTwo,
            stockLocationThree : req.body.stockLocationThree,
            tax:req.body.tax,
            brand: req.body.brand,
            productdescr : req.body.productdescr,
            datetime: dates,
            // myimage: imagepath,
            addProRole : req.user.user_role,
            productStatus : "1",
            viewStatus : "1"
        };

       var products = new Product(set);
          products.save(set,
            function (err, doc) {
               // console.log(doc);
            });


        }else{

            var set = {
            procatName: req.body.procatName,
            prosubcatName: req.body.prosubcatName,
            prosecsubcatName: req.body.prosecsubcatName,
            proparentdata: req.body.proparentdata,
            partName : req.body.partName,
            // avilability: req.body.avilability,
            // quantity: req.body.quantity,
            code: req.body.code,
            mrp: req.body.mrp,
            price:req.body.price,
            stkQty:req.body.stkQty,
            stockLocation : req.body.stockLocation,
            tax:req.body.tax,
            brand: req.body.brand,
            productdescr : req.body.productdescr,
            datetime: dates,
            myimage: imagepath,
            addProRole : req.user.user_role,
            productStatus : "0",
            viewStatus : "0"
        };

       var products = new Product(set);
          products.save(set,
            function (err, doc) {
               // console.log(doc);
            });


        }
      

    

   // }else{

   // for(var k = 0; k < req.files.myimage.length; k++){

   
   //    var file = 'public/uploads/product/' + req.files.myimage[k].originalFilename;


   //    fs.rename(req.files.myimage[k].path, file , function(err) {
   //      if (err) {
   //        // console.log(err);
          
   //        return res.redirect("product");

   //      } 
       
   //    });


   //     var imageName = req.files.myimage[k].originalFilename;

   //     var imagePathdata = 'http://poycard.com/uploads/product/'+req.files.myimage[k].originalFilename;

   //     var imagepath = {};
   //     imagepath['originalname'] = imageName;
   //     imagepath['path'] = imagePathdata;

   //      sub_array.push(imagepath);
       
   //   }    

   // }

          // console.log(JSON.stringify(sub_array));
       
        
       return res.redirect('product-list');
    
      });

     });

       });

     });


var fs =  require('fs');
      var express = require('express');
      var multer = require('multer');
      var uploads = multer({ dest: 'public/uploads/csv/'});

  app.post('/csvUpload', function(req, res) {
    var Product       = require('../app/models/products');
    var Csv       = require('../app/models/csv');
    var User       = require('../app/models/user');
    var Category       = require('../app/models/category');
    var SubCategory       = require('../app/models/subcategory');
    var dates=  new Date();
    var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();
 
    var token = Math.random();

         var vm = this;
       vm.website = null;
        vm.allwebsites = [];
         vm.category = null;
        vm.allcategory = [];
          vm.subcategory = null;
        vm.allsubcategory = [];

     var Website       = require('../app/models/website');

     Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

      SubCategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 


     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 


       var myimage = {} ;

     var imageName = req.files.myimage.originalFilename;

       var imagePathdata = 'http://poycard.com/uploads/csv/'+req.files.myimage.originalFilename;

       var imagepath = {};
       imagepath['originalname'] = imageName;
       imagepath['path'] = imagePathdata;
   
       var set = {
            datetime: dates,
            myimage: imagepath
           
        };

       var csv = new Csv(set);
          csv.save(set,
            function (err, doc) {
               console.log(doc);
            });
        
       return res.redirect('product-list');
    
      });

     });

       });

     });



  app.get('/product', isLoggedIn, function(req, res) {
   
   // var toLowerCase = require("toLowerCase");

    // var match = require("match");  

    // var matchls = match("/ddl/g");

   // var lsss = toLowerCase();

    console.log(req.query);

    var jsonObjectnn = req.query;
    var keyCount  = Object.keys(jsonObjectnn).length;

     console.log(keyCount);

    // var str = req.query;
    // str = str.lsss;
    // var countddl = str.matchls;
    // console.log("a:"+countddl.length);

    var catname = req.query.cat;
    var subcatname = req.query.subcat;
    var secsubcatname = req.query.thirdsubcat;
    var thirsubcatname = req.query.parentdatass;
    var forsubcatname = req.query.ddl1;
    var fvesubcatname = req.query.ddl2;

    var Product       = require('../app/models/products');
    var User       = require('../app/models/user');
    var Category       = require('../app/models/category');
    var SubCategory       = require('../app/models/subcategory');
    var secondSubCategory       = require('../app/models/secondsubcategory');
    var Unit       = require('../app/models/unit');
     var thirdSubCategory       = require('../app/models/thirdsubcategory');
     var Size       = require('../app/models/size');
    var Color       = require('../app/models/color');

      var i = 0;
    var vm = this;
       vm.website = null;
        vm.allwebsites = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.secondsubcategory = null;
        vm.allsecondsubcategory = [];
        vm.thirdSubCategory = null;
        vm.allthirdSubCategorys = [];

        vm.size = null;
        vm.allsize = [];
        vm.unit = null;
        vm.allunit = [];
        vm.color = null;
        vm.allcolor = [];

     var Website       = require('../app/models/website');

     Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

      SubCategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 

      secondSubCategory.find(function (err, secondsubcategory) {

     for(var i = 0; i < secondsubcategory.length; i++){
            vm.allsecondsubcategory[i] = secondsubcategory[i];

        }

       var productvaluesecondsubcategory = JSON.stringify(secondsubcategory);
       var jsonObjectsecondsubcategory = JSON.parse(productvaluesecondsubcategory); 
     
     
     // console.log(jsonObjectsubcategorys);
        thirdSubCategory.find(function (err, thirdSubCategory) {
      // docs is an array
        for(var i = 0; i < thirdSubCategory.length; i++){
            vm.allthirdSubCategorys[i] = thirdSubCategory[i];


        }
 
       var thirdSubCategorysvalue = JSON.stringify(thirdSubCategory);
       var jsonObjectthirdSubCategorys = JSON.parse(thirdSubCategorysvalue);

       Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 


        Unit.find(function (err, unit) {

     for(var i = 0; i < unit.length; i++){
            vm.allunit[i] = unit[i];

        }

       var productvalueunit = JSON.stringify(unit);
       var jsonObjectunit = JSON.parse(productvalueunit); 

        res.render('product.ejs', { 
       forsubcatname : req.query.ddl1,fvesubcatname : req.query.ddl2 ,totalvariable : req.query , totalcountdds : keyCount ,user : req.user , loadunitdata : jsonObjectunit , loadwebsitedata : jsonObjectwebsite, loadcategorydata : jsonObjectcategory, loadsubcategorydata : jsonObjectsubcategory, loadsecondsubcategorydata : jsonObjectsecondsubcategory, loadthirdsubcategorydata : jsonObjectthirdSubCategorys , loadsizedata:jsonObjectsize, loadcolordata : jsonObjectcolor, catname : req.query.cat , subcatname : req.query.subcat , secsubcatname : req.query.thirdsubcat , thirsubcatname : req.query.parentdatass,
     });

      });

    });    

     });

   });    

});

 });    

});

});   

});  




  app.get('/supplier', isLoggedIn, function(req, res) {

    var catname = req.query.cat;
    var subcatname = req.query.subcat;
    var secsubcatname = req.query.thirdsubcat;
    var thirsubcatname = req.query.parentdatass;

    var Product = require('../app/models/products');
    var User = require('../app/models/user');
    var Category = require('../app/models/category');
    var SubCategory = require('../app/models/subcategory');
    var secondSubCategory = require('../app/models/secondsubcategory');
    var thirdSubCategory = require('../app/models/thirdsubcategory');
    var Size = require('../app/models/size');
    var Color = require('../app/models/color');

    var i = 0;
    var vm = this;
      vm.website = null;
        vm.allwebsites = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.secondsubcategory = null;
        vm.allsecondsubcategory = [];
        vm.thirdSubCategory = null;
        vm.allthirdSubCategorys = [];
        vm.product = null;
        vm.allproducts = [];
        vm.size = null;
        vm.allsize = [];
          vm.color = null;
        vm.allcolor = [];

     var Website       = require('../app/models/website');

     Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

      SubCategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 

      secondSubCategory.find(function (err, secondsubcategory) {

     for(var i = 0; i < secondsubcategory.length; i++){
            vm.allsecondsubcategory[i] = secondsubcategory[i];

        }

       var productvaluesecondsubcategory = JSON.stringify(secondsubcategory);
       var jsonObjectsecondsubcategory = JSON.parse(productvaluesecondsubcategory); 
     
     
     // console.log(jsonObjectsubcategorys);
        thirdSubCategory.find(function (err, thirdSubCategory) {
      // docs is an array
        for(var i = 0; i < thirdSubCategory.length; i++){
            vm.allthirdSubCategorys[i] = thirdSubCategory[i];


        }
 
       var thirdSubCategorysvalue = JSON.stringify(thirdSubCategory);
       var jsonObjectthirdSubCategorys = JSON.parse(thirdSubCategorysvalue);




       Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

        // console.log(jsonObjectsubcategorys);
        Product.find(function (err, product) {
      // docs is an array
        for(var i = 0; i < product.length; i++){
            vm.allproducts[i] = product[i];


        }
 
       var productsvalue = JSON.stringify(product);
       var jsonObjectproducts = JSON.parse(productsvalue);

        res.render('supplier.ejs', {

      user : req.user , loadwebsitedata : jsonObjectwebsite, loadcategorydata : jsonObjectcategory, loadsubcategorydata : jsonObjectsubcategory, loadsecondsubcategorydata : jsonObjectsecondsubcategory, loadthirdsubcategorydata : jsonObjectthirdSubCategorys , loadsizedata:jsonObjectsize, loadcolordata : jsonObjectcolor, catname : req.query.cat , subcatname : req.query.subcat , secsubcatname : req.query.thirdsubcat , thirsubcatname : req.query.parentdatass, dataproduct : jsonObjectproducts,
    
                    });

                  });

                });

              });

            });    

          });

        });    

      });

    });   

  });     


//////////////////////////////////sales route///////////////////////////////// 

  app.get('/sales', isLoggedIn, function(req, res) {
    
    var PurchaseItem   = require('../app/models/purchaseItem');
    var Product = require('../app/models/products');
    var Sales  = require('../app/models/sales');
    var User  = require('../app/models/user');
    var Supplier  = require('../app/models/suppliers');
    var Unit  = require('../app/models/unit');
    var Order  = require('../app/models/orders');
    var i = 0;
    var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.product = null;
        vm.allproducts = [];
        vm.supplier = null;
        vm.allsupplier = [];
        vm.order = null;
        vm.allorder = [];
        vm.sales = null;
        vm.allsales = [];
        vm.purchaseitem = null;
        vm.allpurchaseitem = [];

        var Website = require('../app/models/website');

      PurchaseItem.find(function (err, purchaseitem) {
     
        for(var i = 0; i < purchaseitem.length; i++){
            vm.allpurchaseitem[i] = purchaseitem[i];


        }
 
       var purchaseitemvalue = JSON.stringify(purchaseitem);
       var jsonObjectpurchaseitem = JSON.parse(purchaseitemvalue);

        Website.find(function (err, website) {

      for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

      var productvaluewebsite = JSON.stringify(website);
      var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

        Product.find(function (err, product) {

        for(var i = 0; i < product.length; i++){
            vm.allproducts[i] = product[i];

        }

        var clean = product.filter((product, index, self) => index === self.findIndex((t) => (t.partId === product.partId)));
        var productsvalue = JSON.stringify(clean);
        var jsonObjectproducts = JSON.parse(productsvalue);


       Supplier.find(function (err, supplier) {

      for(var i = 0; i < supplier.length; i++){
            vm.allsupplier[i] = supplier[i];

        }

       var productvaluesupplier = JSON.stringify(supplier);
       var jsonObjectsupplier = JSON.parse(productvaluesupplier); 

         User.find(function (err, user) {

     for(var i = 0; i < user.length; i++){
            vm.alluser[i] = user[i];

       }

       var  productvalueuser = JSON.stringify(user);
       var jsonObjectuser = JSON.parse(productvalueuser); 


         Order.find(function (err, order) {

      for(var i = 0; i < order.length; i++){
            vm.allorder[i] = order[i];

        }

        var productvalueorder = JSON.stringify(order);
        var jsonObjectorder = JSON.parse(productvalueorder);
  
    
      Sales.find({}).sort({_id:-1}).limit(1).exec(function(err, sales){
      
        for(var i = 0; i < sales.length; i++){
              vm.allsales[i] = sales[i];

          var invoiceNo = sales[i].invoiceNo;
          var voucharNo = sales[i].voucherNo;  

        }
        
        var salesvalueorder = JSON.stringify(sales);
        var jsonObjectsales = JSON.parse(salesvalueorder); 


        res.render('sales.ejs', { datapurchaseitem : jsonObjectpurchaseitem, loaduserdata : jsonObjectuser , invoiceNodata: invoiceNo , voucherNoData : voucharNo, loadsalesdata: jsonObjectsales , loadorderdata : jsonObjectorder , user : req.user , loadsupplierdata : jsonObjectsupplier, loadwebsitedata : jsonObjectwebsite, thirsubcatname : req.query.parentdatass, dataproduct : jsonObjectproducts,
                                     
                  });

                });
          
              });
    
            });
    
          });   
    
        });

      });

    });

  });    

     

///////////////////////////////////////////Purchase route//////////////////////////////////////////////////////////////////

  app.get('/purchase', isLoggedIn, function(req, res) {

    var Product  = require('../app/models/products');
    var User = require('../app/models/user');
    var Supplier = require('../app/models/suppliers');
    var Order = require('../app/models/orders');
    var Purchase  = require('../app/models/purchase');

    var i = 0;
    var vm = this;
       vm.website = null;
        vm.allwebsites = [];     
        vm.user = null;
        vm.alluser = []; 
        vm.userr = null;
        vm.alluserr = [];    
        vm.product = null;
        vm.allproducts = [];   
        vm.supplier = null;
        vm.allsupplier = [];     
        vm.order = null;
        vm.allorder = [];
   
     var Website = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);    

       Product.find({},function (err, product) {
    
        for(var i = 0; i < product.length; i++){
            vm.allproducts[i] = product[i];

        }
 
        var clean = product.filter((product, index, self) => index === self.findIndex((t) => (t.partId === product.partId)));
        var productsvalue = JSON.stringify(clean);
        var jsonObjectproducts = JSON.parse(productsvalue);


       Supplier.find(function (err, supplier) {

       for(var i = 0; i < supplier.length; i++){
              vm.allsupplier[i] = supplier[i];

        }

       var productvaluesupplier = JSON.stringify(supplier);
       var jsonObjectsupplier = JSON.parse(productvaluesupplier); 


        Order.find(function (err, order) {

      for(var i = 0; i < order.length; i++){
            vm.allorder[i] = order[i];

        }

        var productvalueorder = JSON.stringify(order);
        var jsonObjectorder = JSON.parse(productvalueorder); 

          User.find(function (err, user) {

        for(var i = 0; i < user.length; i++){
            vm.alluser[i] = user[i];

        }

        var productvalueuser = JSON.stringify(user);
        var jsonObjectuser = JSON.parse(productvalueuser); 
        
        Purchase.find({}).sort({_id:-1}).limit(1).exec(function(err, purchase){
 
          
        for(var i = 0; i < purchase.length; i++){

            vm.allpurchase[i] = purchase[i];

            var invoiceNo = purchase[i].invoiceNo;
            var voucharNo = purchase[i].voucherNo;            

        }


        var purchasevalueorder = JSON.stringify(purchase);
        var jsonObjectpurchase = JSON.parse(purchasevalueorder); 

        Product.find({}, {partId:1, _id:0}, function(err, userr) {

        for(var i = 0; i < userr.length; i++){
            vm.alluserr[i] = userr[i];

        }

        var productuserr = JSON.stringify(userr);
        var jsonUser = JSON.parse(productuserr);

      res.render('purchase.ejs', { idd : jsonUser, loaduserdata : jsonObjectuser, invoiceNodata: invoiceNo , voucherNoData : voucharNo , loadpurchasedata : jsonObjectpurchase, loadorderdata : jsonObjectorder , user : req.user , loadsupplierdata : jsonObjectsupplier, loadwebsitedata : jsonObjectwebsite , thirsubcatname : req.query.parentdatass, dataproduct : jsonObjectproducts,
        
                    });

                  });

                });
     
              });

            });   

          });

        });

      });

    });

    app.get('/purchaseDetail', isLoggedIn, function(req, res) {

      var Product = require('../app/models/products');
      var User  = require('../app/models/user');
      var Supplier = require('../app/models/suppliers');
      var Purchase  = require('../app/models/purchase');
      var Order = require('../app/models/orders');
      var PurchaseItem = require('../app/models/purchaseItem');
      var i = 0;
      var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.product = null;
        vm.allproducts = [];
        vm.productDetail = null;
        vm.allproductDetails = [];
        vm.supplier = null;
        vm.allsuppliers = [];
        vm.purchase = null;
        vm.allpurchase = [];
        vm.purchaseitem = null;
        vm.allpurchaseitem = [];
        vm.order = null;
        vm.allorders = [];

       var Website = require('../app/models/website');

          Website.find(function (err, website) {

        for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 
    
        Product.find(function (err, product) {
  
        for(var i = 0; i < product.length; i++){
            vm.allproducts[i] = product[i];


        }
 
       var productsvalue = JSON.stringify(product);
       var jsonObjectproducts = JSON.parse(productsvalue);

  
         Order.find(function (err, order) {
    
        for(var i = 0; i < order.length; i++){
            vm.allorders[i] = order[i];

        }
 
       var ordervalue = JSON.stringify(order);
       var jsonObjectorder = JSON.parse(ordervalue);

        Product.find({_id : req.query.proid},function (err, productDetail) {
   
        for(var i = 0; i < productDetail.length; i++){
            vm.allproductDetails[i] = productDetail[i];

        }
 
        var productDetailsvalue = JSON.stringify(productDetail);
        var jsonObjectproductDetails = JSON.parse(productDetailsvalue);

        Supplier.find(function (err, supplier) {
     
        for(var i = 0; i < supplier.length; i++){
            vm.allsuppliers[i] = supplier[i];

        }
 
       var suppliersvalue = JSON.stringify(supplier);
       var jsonObjectsuppliers = JSON.parse(suppliersvalue);

       Purchase.find(function (err, purchase) {

        for(var i = 0; i < purchase.length; i++){
            vm.allpurchase[i] = purchase[i];

        }
 
       var purchasevalue = JSON.stringify(purchase);
       var jsonObjectpurchase = JSON.parse(purchasevalue);

        PurchaseItem.find(function (err, purchaseitem) {
     
        for(var i = 0; i < purchaseitem.length; i++){
            vm.allpurchaseitem[i] = purchaseitem[i];

        }
 
       var purchaseitemvalue = JSON.stringify(purchaseitem);
       var jsonObjectpurchaseitem = JSON.parse(purchaseitemvalue);

        res.render('purchaseDetail.ejs', { loadorderdata  : jsonObjectorder,  supId : req.query.id, datapurchaseitem : jsonObjectpurchaseitem , datapurchase : jsonObjectpurchase , loadsupplierdata : jsonObjectsuppliers,  loadproductdetail : jsonObjectproductDetails, user : req.user , loadwebsitedata : jsonObjectwebsite, dataproduct : jsonObjectproducts,
         
                    });

                  });

                });

              });

            });  

          });  

        });

      });

    });
    

  app.get('/edit-purchase', isLoggedIn, function(req, res) {

    var supId = req.query.id;

    var Product = require('../app/models/products');
    var User = require('../app/models/user');
    var Supplier = require('../app/models/suppliers');
    var Purchase = require('../app/models/purchase');
    var PurchaseItem = require('../app/models/purchaseItem');
    var Order = require('../app/models/orders');
    var i = 0;
    var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.product = null;
        vm.allproducts = [];
        vm.productDetail = null;
        vm.allproductDetails = [];
        vm.supplier = null;
        vm.allsuppliers = [];
        vm.purchase = null;
        vm.allpurchase = [];
        vm.purchaseitem = null;
        vm.allpurchaseitem = [];
        vm.order = null;
        vm.allorder = [];

      var Website = require('../app/models/website');
     

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

        var productvaluewebsite = JSON.stringify(website);
        var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

        
       Product.find({},function (err, product) {
      
        for(var i = 0; i < product.length; i++){
            vm.allproducts[i] = product[i];


        }
 
        var clean = product.filter((product, index, self) => index === self.findIndex((t) => (t.partId === product.partId)));
        var productsvalue = JSON.stringify(clean);
        var jsonObjectproducts = JSON.parse(productsvalue);


        Product.find({_id : req.query.proid},function (err, productDetail) {
    
        for(var i = 0; i < productDetail.length; i++){
            vm.allproductDetails[i] = productDetail[i];


        }
 
       var productDetailsvalue = JSON.stringify(productDetail);
       var jsonObjectproductDetails = JSON.parse(productDetailsvalue);


      Supplier.find(function (err, supplier) {
      // docs is an array
        for(var i = 0; i < supplier.length; i++){
            vm.allsuppliers[i] = supplier[i];


        }
 
       var suppliersvalue = JSON.stringify(supplier);
       var jsonObjectsuppliers = JSON.parse(suppliersvalue);

       Purchase.find(function (err, purchase) {
      // docs is an array
        for(var i = 0; i < purchase.length; i++){
            vm.allpurchase[i] = purchase[i];


        }
 
       var purchasevalue = JSON.stringify(purchase);
       var jsonObjectpurchase = JSON.parse(purchasevalue);

        PurchaseItem.find(function (err, purchaseitem) {

        for(var i = 0; i < purchaseitem.length; i++){
            vm.allpurchaseitem[i] = purchaseitem[i];


        }
 
        var purchaseitemvalue = JSON.stringify(purchaseitem);
        var jsonObjectpurchaseitem = JSON.parse(purchaseitemvalue);

          Order.find(function (err, order) {

        for(var i = 0; i < order.length; i++){

            vm.allorder[i] = order[i];
        }
 
       var ordervalue = JSON.stringify(order);
       var jsonObjectorder = JSON.parse(ordervalue);

    res.render('edit-purchase.ejs', { loadorderdata : jsonObjectorder , supId : req.query.id , datapurchase : jsonObjectpurchase , datapurchasedetail  : jsonObjectpurchaseitem, loadsupplierdata : jsonObjectsuppliers,  loadproductdetail : jsonObjectproductDetails, user : req.user , loadwebsitedata : jsonObjectwebsite, dataproduct : jsonObjectproducts,
    

                  });

               });

             });

            });

          });  

        });  

      });

    });

  });
 


app.post('/updatequantity', isLoggedIn, function(req, res){
    
    var Product = require('../app/models/products');
    var Quantity = req.body.Qantity;
    var PartId = req.body.PartId;

    // console.log("PartId " + PartId);

    Product.findOne({partId : PartId}, (err, userfindOne) => {

      var reqQuatity = parseInt(userfindOne.quantity) - parseInt(Quantity);

        Product.updateMany({ partId: PartId},{ $set: { quantity  : reqQuatity }}, function (err, doc) {

   
        });

    });

  });

  app.post('/updatesalesquantity', isLoggedIn, function(req, res){

    console.log('hi here i am');
    
    var Product = require('../app/models/products');
    var Quantity = req.body.Qantity;
    var PartId = req.body.PartId;

    console.log("PartId " + PartId);

    Product.findOne({partId : PartId}, (err, userfindOne) => {

      var reqQuatity = parseInt(userfindOne.quantity) + parseInt(Quantity);

        Product.updateMany({ partId: PartId},{ $set: { quantity  : reqQuatity }}, function (err, doc) {

   
        });

    });

  });
 

      var fs =  require('fs');
      var express = require('express');
      var multer = require('multer');
      var uploads = multer({ dest: 'public/uploads/product/'});

    app.post('/updateproduct',isLoggedIn, function(req, res) {

      var Category = require('../app/models/category');
      var SubCategory = require('../app/models/subcategory');
      var Size = require('../app/models/size');
      var Color = require('../app/models/color');
      var Product = require('../app/models/products');
      var dates =  new Date();
      var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();
      var i = 0;
      var vm = this;
          vm.products = null;
          vm.allproducts = [];
          vm.website = null;
          vm.allwebsites = [];
          vm.category = null;
          vm.allcategory = [];
          vm.subcategory = null;
          vm.allsubcategory = [];
          vm.size = null;
          vm.allsize = [];
          vm.color = null;
          vm.allcolor = [];

        var Website = require('../app/models/website');

        Size.find(function (err, size) {

          for(var i = 0; i < size.length; i++){

              vm.allsize[i] = size[i];

          }

          var productvaluesize = JSON.stringify(size);
          var jsonObjectsize = JSON.parse(productvaluesize);

        Color.find(function (err, color) {

          for(var i = 0; i < color.length; i++){
              vm.allcolor[i] = color[i];

          }

          var productvaluecolor = JSON.stringify(color);
          var jsonObjectcolor = JSON.parse(productvaluecolor);

        Category.find(function (err, category) {

          for(var i = 0; i < category.length; i++){
              vm.allcategory[i] = category[i];

          }

            var productvaluecategory = JSON.stringify(category);
            var jsonObjectcategory = JSON.parse(productvaluecategory);

        SubCategory.find(function (err, subcategory) {

        for(var i = 0; i < subcategory.length; i++){
              vm.allsubcategory[i] = subcategory[i];

        }

            var productvaluesubcategory = JSON.stringify(subcategory);
            var jsonObjectsubcategory = JSON.parse(productvaluesubcategory);


        Website.find(function (err, website) {

          for(var i = 0; i < website.length; i++){
              vm.allwebsites[i] = website[i];

          }

            var productvaluewebsite = JSON.stringify(website);
            var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

        Product.find(function (err, products) {
       
          for(var i = 0; i < products.length; i++){
              vm.allproducts[i] = products[i];
         }

          var productvalue = JSON.stringify(products);
          var jsonObject = JSON.parse(productvalue);
          var myimage = {} ;  
          var sub_array = [];
         
        if(req.files.myimage.length == "1"){

          for(var k = 0; k < req.files.myimage.length; k++){
 
            var file = 'public/uploads/product/' +  req.body.Prodid + req.files.myimage[k].originalFilename;
           
            fs.rename(req.files.myimage[k].path, file , function(err) {
              if (err) {
             
              }
             
            });

          var imageName = req.body.Prodid + req.files.myimage[k].originalFilename;  

          var imagePathdata = 'http://poycard.com/uploads/product/'+ req.body.Prodid + req.files.myimage[k].originalFilename;

          var imagepath = {};    

                imagepath['originalname'] = imageName;
                imagepath['path'] = imagePathdata;

              sub_array.push(imagepath);

          }

              var sets = {myimage:imagepath};

              Product.findById({"_id":req.body.Prodid}, function (err, productst) {

                Product.updateMany({"partId" : req.body.partId},{ $set: {

                        partName : req.body.partName,
                        code: req.body.code,
                        mrp: req.body.mrp,
                        price:req.body.price,
                        stkQty:req.body.stkQty,
                        stockLocationOne : req.body.stockLocationOne,
                        stockLocationTwo : req.body.stockLocationTwo,
                        stockLocationThree : req.body.stockLocationThree,
                        shippricemp  : req.body.shippricemp,
                        shippriceoutmp  : req.body.shippriceoutmp,
                        dimension  : req.body.dimension,
                        weight  : req.body.weight,
                        tax:req.body.tax,
                        setPriority : req.body.setPriority,
                        quantity: req.body.quantity,
                        brand: req.body.brand,
                        umState   : req.body.umState,
                        unitMeasure : req.body.unitMeasure,
                        altset : req.body.altset,
                        firstSet : req.body.firstSet,
                        secondSet : req.body.secondSet,
                        productdescr : req.body.productdescr,
                        datetime: dates,
                        myimage: sub_array.concat(productst.myimage)
                       
                      }},function (err, doc) {
                       
                    });  
                 
                  });

                } else {
           
                    for(var k = 0; k < req.files.myimage.length; k++){

                      var file = 'public/uploads/product/' + req.body.Prodid + req.files.myimage[k].originalFilename;
                   
                      fs.rename(req.files.myimage[k].path, file , function(err) {
                        if (err) {
                       
                      }
                     
                    });

                    var imageName =  req.body.Prodid + req.files.myimage[k].originalFilename;
                   
                    var imagePathdata = 'http://poycard.com/uploads/product/'+ req.body.Prodid + req.files.myimage[k].originalFilename;
                     
                      var imagepath = {};

                        imagepath['originalname'] = imageName;

                          imagepath['path'] = imagePathdata;

                      sub_array.push(imagepath);

                      // console.log("sub_array " + sub_array );

                  }

                    var _id = req.body.Prodid;
               
                    var setk = {myimage:imagepath};

                  Product.findById({"_id":req.body.Prodid}, function (err, productst) {

                    Product.updateMany({partId:req.body.partId},{ $set: {

                        partName : req.body.partName,
                        code: req.body.code,
                        mrp: req.body.mrp,
                        price:req.body.price,
                        stkQty:req.body.stkQty,
                        stockLocationOne : req.body.stockLocationOne,
                        stockLocationTwo : req.body.stockLocationTwo,
                        stockLocationThree : req.body.stockLocationThree,
                        shippricemp  : req.body.shippricemp,
                        shippriceoutmp  : req.body.shippriceoutmp,
                        dimension  : req.body.dimension,
                        weight  : req.body.weight,
                        tax:req.body.tax,
                        setPriority : req.body.setPriority,
                        quantity: req.body.quantity,
                        brand: req.body.brand,
                        umState   : req.body.umState,
                        unitMeasure : req.body.unitMeasure,
                        altset : req.body.altset,
                        firstSet : req.body.firstSet,
                        secondSet : req.body.secondSet,
                        productdescr : req.body.productdescr,
                        datetime: dates,
                        myimage: sub_array.concat(productst.myimage)
                       
                      }},function (err, doc) {
                       
                    });  

                  });
               
                }

              });

            return res.redirect('product-list');

            });

          });

        });  

      });

      });

    });



 ////////////////////// Purchase Return Start ///////////////////////////////////
 app.post('/updateReturnQuantity', isLoggedIn, function(req, res){

      var Product = require('../app/models/products');
      var Quantity = req.body.Qantity;
      var PartId = req.body.PartId;

      Product.findOne({partId : PartId}, (err, userfindOne) => {

        var reqQuatity = parseInt(userfindOne.quantity) - parseInt(Quantity);

          Product.updateMany({ partId: PartId},{ $set: { quantity  : reqQuatity }}, function (err, doc) {
           
            if (err) throw err;
     
          });

      });

      res.send('200','true');

    });

    app.post('/updatepurchaseReturn',isLoggedIn, function(req, res) {
     
      var Product = require('../app/models/products');
      var PurchaseReturn = require('../app/models/purchasereturn');
      var PurchaseItemReturn = require('../app/models/purchaseItemreturn');
      var _id = req.body.Prodid;
      var vch = req.body.voucherNo;
      var supplierID = req.body.supId;
      var strDate = (new Date()).toISOString().split('T')[0]; 

        PurchaseItemReturn.find({"purchaseId":req.body.Prodid}, function (err, productst) {
           
            PurchaseItemReturn.remove({"purchaseId":req.body.Prodid}, function (err, purchasedfataid) {
       
          });

        });


        for(var s = 0; s < req.body.partId.length; s++){
     
      Product.updateMany({ partId: req.body.part[s]},{ $set: { quantity  : req.body.finalqtydata[s]}}, function (err, doc) {
          if(err){

            res.json(500);
          } else {
           
          }        
         
        });

      }

      PurchaseReturn.findById({"_id":req.body.Prodid}, function (err, productst) {

        var sets = {

                supId : req.body.supId,
                GrandTotalAmnt : req.body.GrandTotalAmnt,
                SgstTotalAmnt: req.body.SgstTotalAmnt,
                CgstTotalAmnt: req.body.CgstTotalAmnt,
                IgstTotalAmnt: req.body.IgstTotalAmnt,
                FinalTotalAmnt : req.body.FinalTotalAmnt,
                TotalDecimalAmnt : req.body.TotalDecimalAmnt,
                EditAmount : req.body.EditAmount,
                debitNo : req.body.debitNo,
                voucherNo : req.body.voucherNo,
                debitDate : req.body.debitDate,
                orderId : req.body.orderId,
                purchaseStatus : "0",
                datetime : strDate
                   
              };
               
            PurchaseReturn.update({ _id: req.body.Prodid },{ $set: sets},function (err, doc) {

                var parttt = req.body.partId;

              for(var n = 0; n < req.body.partId.length; n++){  

                  var datasetk = {

                          purchaseId : req.body.Prodid,
                          partId : req.body.partId[n],
                          SellPrice : req.body.SellPrice[n],
                          TotalAmnt : req.body.TotalAmnt[n],
                          Tax  : req.body.Tax[n],
                          Discount : req.body.Discount[n],
                          Quantity : req.body.Quantity[n],
                          Price : req.body.Price[n],
                          Per : req.body.Per[n],
                          DiscountAmnt : req.body.DiscountAmnt[n],
                          Sgst : req.body.Sgst[n],
                          Cgst : req.body.Cgst[n],
                          Igst : req.body.Igst[n],
                          TotalAmntData : req.body.TotalAmntData[n],
                          finalqtydata : req.body.finalqtydata[n],
                          part : req.body.part[n],
                          voucherNo : vch,
                          supId : supplierID,
                          purchaseStatus : "0",
                          datetime : strDate

                        };

                    var purchaseItemReturn = new PurchaseItemReturn(datasetk);

                        purchaseItemReturn.save(function(error, doc){

                          if(error) {

                            res.json(error);

                          } else {  

                            // console.log("data inserted");

                }

              });          

            }

          });  
         
        });

      return res.redirect('purchaseReturn-list');
     
    });

    app.post('/dateSortPurchaseReturn',isLoggedIn, function(req, res) {

      var Website = require('../app/models/website');
      var Product = require('../app/models/products');
      var User = require('../app/models/user');
      var Category = require('../app/models/category');
      var SubCategory = require('../app/models/subcategory');
      var secondSubCategory = require('../app/models/secondsubcategory');
      var thirdSubCategory = require('../app/models/thirdsubcategory');
      var Size = require('../app/models/size');
      var Color = require('../app/models/color');
      var Supplier = require('../app/models/suppliers');
      var Unit = require('../app/models/unit');
      var Order = require('../app/models/orders');
      var PurchaseReturn = require('../app/models/purchasereturn');
      var PurchaseItemReturn = require('../app/models/purchaseItemreturn');
      var i = 0;
      var vm = this;
         vm.website = null;
          vm.allwebsites = [];
          vm.category = null;
          vm.allcategory = [];
          vm.subcategory = null;
          vm.allsubcategory = [];
          vm.secondsubcategory = null;
          vm.allsecondsubcategory = [];
          vm.thirdSubCategory = null;
          vm.allthirdSubCategorys = [];
          vm.product = null;
          vm.allproducts = [];
          vm.size = null;
          vm.allsize = [];
          vm.color = null;
          vm.allcolor = [];
          vm.supplier = null;
          vm.allsupplier = [];
          vm.unit = null;
          vm.allunit = [];
          vm.order = null;
          vm.allorder = [];
          vm.purchase = null;
          vm.allpurchase = [];


         Category.find(function (err, category) {

       for(var i = 0; i < category.length; i++){
              vm.allcategory[i] = category[i];

          }

         var productvaluecategory = JSON.stringify(category);
         var jsonObjectcategory = JSON.parse(productvaluecategory);

        SubCategory.find(function (err, subcategory) {

       for(var i = 0; i < subcategory.length; i++){
              vm.allsubcategory[i] = subcategory[i];

          }

         var productvaluesubcategory = JSON.stringify(subcategory);
         var jsonObjectsubcategory = JSON.parse(productvaluesubcategory);

        secondSubCategory.find(function (err, secondsubcategory) {

       for(var i = 0; i < secondsubcategory.length; i++){
              vm.allsecondsubcategory[i] = secondsubcategory[i];

          }

         var productvaluesecondsubcategory = JSON.stringify(secondsubcategory);
         var jsonObjectsecondsubcategory = JSON.parse(productvaluesecondsubcategory);
       
       
       // console.log(jsonObjectsubcategorys);
          thirdSubCategory.find(function (err, thirdSubCategory) {
        // docs is an array
          for(var i = 0; i < thirdSubCategory.length; i++){
              vm.allthirdSubCategorys[i] = thirdSubCategory[i];


          }
   
         var thirdSubCategorysvalue = JSON.stringify(thirdSubCategory);
         var jsonObjectthirdSubCategorys = JSON.parse(thirdSubCategorysvalue);




         Size.find(function (err, size) {

       for(var i = 0; i < size.length; i++){
              vm.allsize[i] = size[i];

          }

         var productvaluesize = JSON.stringify(size);
         var jsonObjectsize = JSON.parse(productvaluesize);

        Color.find(function (err, color) {

       for(var i = 0; i < color.length; i++){
              vm.allcolor[i] = color[i];

          }

         var productvaluecolor = JSON.stringify(color);
         var jsonObjectcolor = JSON.parse(productvaluecolor);

          Website.find(function (err, website) {

        for(var i = 0; i < website.length; i++){
              vm.allwebsites[i] = website[i];

          }

          var productvaluewebsite = JSON.stringify(website);
          var jsonObjectwebsite = JSON.parse(productvaluewebsite);

          Product.find(function (err, product) {
     
        for(var i = 0; i < product.length; i++){
              vm.allproducts[i] = product[i];

          }
   
          var productsvalue = JSON.stringify(product);
          var jsonObjectproducts = JSON.parse(productsvalue);


          Supplier.find(function (err, supplier) {

        for(var i = 0; i < supplier.length; i++){
              vm.allsupplier[i] = supplier[i];

          }

          var productvaluesupplier = JSON.stringify(supplier);
          var jsonObjectsupplier = JSON.parse(productvaluesupplier);


          Unit.find(function (err, unit) {

        for(var i = 0; i < unit.length; i++){
              vm.allunit[i] = unit[i];

          }

          var productvalueunit = JSON.stringify(unit);
          var jsonObjectunit = JSON.parse(productvalueunit);


          Order.find(function (err, order) {

        for(var i = 0; i < order.length; i++){
              vm.allorder[i] = order[i];

          }

          var productvalueorder = JSON.stringify(order);
          var jsonObjectorder = JSON.parse(productvalueorder);


      PurchaseReturn.find({$or: [{"datetime": {$gte: req.body.startDate, $lte: req.body.endDate}}]},function (err, purchase) {

             console.log(purchase);


         res.render('purchaseReturn-listDate.ejs', { datapurchase : purchase , loadorderdata : jsonObjectorder , user : req.user ,loadunitdata: jsonObjectunit , datasupplier : jsonObjectsupplier, loadwebsitedata : jsonObjectwebsite, loadcategorydata : jsonObjectcategory, loadsubcategorydata : jsonObjectsubcategory, loadsecondsubcategorydata : jsonObjectsecondsubcategory, loadthirdsubcategorydata : jsonObjectthirdSubCategorys , loadsizedata:jsonObjectsize, loadcolordata : jsonObjectcolor, catname : req.query.cat , subcatname : req.query.subcat , secsubcatname : req.query.thirdsubcat , thirsubcatname : req.query.parentdatass, dataproduct : jsonObjectproducts,
   
              });
                               
                                });
                         
                              });
                   
                            });
                   
                          });  
                   
                        });
               
                      });
               
                    });
               
                  });    

                });

            });    

        });

      });  

    });
   
    app.post('/purchaseReturn-insert',isLoggedIn, function(req, res) {
     
      var Product = require('../app/models/products');
      var PurchaseReturn = require('../app/models/purchasereturn');
      var PurchaseItemReturn = require('../app/models/purchaseItemreturn');
      var dates =  new Date();
      var strDate = (new Date()).toISOString().split('T')[0]; 
      var vch = req.body.voucherNo;
      var supplierID = req.body.supId;

          for(var s = 0; s < req.body.part.length; s++){
     
        Product.updateMany({ partId: req.body.browser[s]},{ $set: { quantity  : req.body.finalqtydata[s]}}, function (err, doc) {
            if(err){

              res.json(500);
            } else {
             
            }        
           
          });

        }

        var set = {

                supId : req.body.supId,
                GrandTotalAmnt : req.body.GrandTotalAmnt,
                SgstTotalAmnt: req.body.SgstTotalAmnt,
                CgstTotalAmnt: req.body.CgstTotalAmnt,
                IgstTotalAmnt: req.body.IgstTotalAmnt,
                FinalTotalAmnt : req.body.FinalTotalAmnt,
                TotalDecimalAmnt : req.body.TotalDecimalAmnt,
                EditAmount : req.body.EditAmount,
                debitNo : req.body.debitNo,
                voucherNo : req.body.voucherNo,
                debitDate : req.body.debitDate,
                orderId : req.body.orderId,
                purchaseReturnStatus : "0",
                datetime : strDate

               };

         var purchaseReturn = new PurchaseReturn(set);

            purchaseReturn.save(set, function (err, doc) {


          for(var n = 0; n < req.body.browser.length; n++){

              var sets = {          

                  purchaseId : doc._id,
                  partId : req.body.part[n],
                  SellPrice : req.body.SellPrice[n],
                  TotalAmnt : req.body.TotalAmnt[n],
                  Tax : req.body.Tax[n],
                  Discount : req.body.Discount[n],
                  Quantity  : req.body.Quantity[n],
                  Price : req.body.Price[n],
                  Per : req.body.Per[n],
                  DiscountAmnt : req.body.DiscountAmnt[n],
                  Sgst: req.body.Sgst[n],
                  Cgst: req.body.Cgst[n],
                  Igst: req.body.Igst[n],
                  TotalAmntData : req.body.TotalAmntData[n],
                  finalqtydata : req.body.finalqtydata[n],
                  part : req.body.browser[n],
                  voucherNo : vch,
                  supId : supplierID, 
                  purchaseStatus : "0",
                  datetime : strDate

               };

            var purchaseItemReturn = new PurchaseItemReturn(sets);

              purchaseItemReturn.save(sets,function (err, docss) {

               
                });
                         
          }
               
        return res.redirect('purchasereturn');
         
      });

    });


      ////////////////////// Purchase Return Start //////////////////////////////
 
    app.get('/edit-purchaseReturn', isLoggedIn, function(req, res) {

      var Product = require('../app/models/products');
      var User = require('../app/models/user');
      var Supplier = require('../app/models/suppliers');
      var PurchaseReturn = require('../app/models/purchasereturn');
      var PurchaseItemReturn = require('../app/models/purchaseItemreturn');
      var Order = require('../app/models/orders');
      var i = 0;
      var vm = this;
          vm.website = null;
          vm.allwebsites = [];
          vm.product = null;
          vm.allproducts = [];
          vm.productDetail = null;
          vm.allproductDetails = [];
          vm.supplier = null;
          vm.allsuppliers = [];
          vm.purchase = null;
          vm.allpurchase = [];
          vm.purchaseitem = null;
          vm.allpurchaseitem = [];
          vm.order = null;
          vm.allorder = [];

       var Website = require('../app/models/website');


       Website.find(function (err, website) {

       for(var i = 0; i < website.length; i++){
              vm.allwebsites[i] = website[i];

          }

          var productvaluewebsite = JSON.stringify(website);
          var jsonObjectwebsite = JSON.parse(productvaluewebsite);

         
         Product.find({},function (err, product) {
       
          for(var i = 0; i < product.length; i++){
              vm.allproducts[i] = product[i];


          }
   
          var clean = product.filter((product, index, self) => index === self.findIndex((t) => (t.partId === product.partId)));
          var productsvalue = JSON.stringify(clean);
          var jsonObjectproducts = JSON.parse(productsvalue);

          Product.find({_id : req.query.proid},function (err, productDetail) {
       
          for(var i = 0; i < productDetail.length; i++){
              vm.allproductDetails[i] = productDetail[i];
   
          }
   
            var productDetailsvalue = JSON.stringify(productDetail);
            var jsonObjectproductDetails = JSON.parse(productDetailsvalue);

          Supplier.find(function (err, supplier) {
     
          for(var i = 0; i < supplier.length; i++){
              vm.allsuppliers[i] = supplier[i];


          }
   
            var suppliersvalue = JSON.stringify(supplier);
            var jsonObjectsuppliers = JSON.parse(suppliersvalue);

          PurchaseReturn.find(function (err, purchase) {
       
          for(var i = 0; i < purchase.length; i++){
              vm.allpurchase[i] = purchase[i];

          }
   
            var purchasevalue = JSON.stringify(purchase);
            var jsonObjectpurchase = JSON.parse(purchasevalue);

          PurchaseItemReturn.find(function (err, purchaseitem) {
       
          for(var i = 0; i < purchaseitem.length; i++){
              vm.allpurchaseitem[i] = purchaseitem[i];

          }
   
            var purchaseitemvalue = JSON.stringify(purchaseitem);
            var jsonObjectpurchaseitem = JSON.parse(purchaseitemvalue);

          Order.find(function (err, order) {
       
          for(var i = 0; i < order.length; i++){
              vm.allorder[i] = order[i];

          }
   
          var ordervalue = JSON.stringify(order);
          var jsonObjectorder = JSON.parse(ordervalue);


        res.render('edit-purchaseReturn.ejs', { loadorderdata : jsonObjectorder , supId : req.query.id , datapurchase : jsonObjectpurchase , datapurchasedetail  : jsonObjectpurchaseitem, loadsupplierdata : jsonObjectsuppliers,  loadproductdetail : jsonObjectproductDetails, user : req.user , loadwebsitedata : jsonObjectwebsite, dataproduct : jsonObjectproducts,

             
                      });

                    });

                 });

               });

            });

          });  

        });  

      });

    });

      

    app.get('/purchasereturn', isLoggedIn, function(req, res) {

        var Product = require('../app/models/products');
        var User = require('../app/models/user');
        var Supplier = require('../app/models/suppliers');
        var Order = require('../app/models/orders');
        var PurchaseReturn = require('../app/models/purchasereturn');
        var i = 0;
        var vm = this;
          vm.website = null;
            vm.allwebsites = [];
            vm.user = null;
            vm.alluser = [];
            vm.product = null;
            vm.allproducts = [];
            vm.supplier = null;
            vm.allsupplier = [];
            vm.order = null;
            vm.allorder = [];
    
         var Website  = require('../app/models/website');    

         Website.find(function (err, website) {

         for(var i = 0; i < website.length; i++){
                vm.allwebsites[i] = website[i];

            }

           var productvaluewebsite = JSON.stringify(website);
           var jsonObjectwebsite = JSON.parse(productvaluewebsite);


           Product.find({},function (err, product) {
         
            for(var i = 0; i < product.length; i++){
                vm.allproducts[i] = product[i];


            }
     
            var clean = product.filter((product, index, self) => index === self.findIndex((t) => (t.partId === product.partId)));
            var productsvalue = JSON.stringify(clean);
            var jsonObjectproducts = JSON.parse(productsvalue);

           Supplier.find(function (err, supplier) {

           for(var i = 0; i < supplier.length; i++){
                  vm.allsupplier[i] = supplier[i];

            }

           var productvaluesupplier = JSON.stringify(supplier);
           var jsonObjectsupplier = JSON.parse(productvaluesupplier);


              Order.find(function (err, order) {

            for(var i = 0; i < order.length; i++){
                vm.allorder[i] = order[i];

            }

            var productvalueorder = JSON.stringify(order);
            var jsonObjectorder = JSON.parse(productvalueorder);

             User.find(function (err, user) {

          for(var i = 0; i < user.length; i++){
                vm.alluser[i] = user[i];

            }

            var productvalueuser = JSON.stringify(user);
            var jsonObjectuser = JSON.parse(productvalueuser);
           
            PurchaseReturn.find({}).sort({_id:-1}).limit(1).exec(function(err, purchase){
     
             
            for(var i = 0; i < purchase.length; i++){

                vm.allpurchase[i] = purchase[i];

                var invoiceNo = purchase[i].invoiceNo;
                var voucharNo = purchase[i].voucherNo;            

            }

            var purchasevalueorder = JSON.stringify(purchase);
            var jsonObjectpurchase = JSON.parse(purchasevalueorder);

          res.render('purchasereturn.ejs', { loaduserdata : jsonObjectuser, invoiceNodata: invoiceNo , voucherNoData : voucharNo , loadpurchasedata : jsonObjectpurchase, loadorderdata : jsonObjectorder , user : req.user , loadsupplierdata : jsonObjectsupplier, loadwebsitedata : jsonObjectwebsite, thirsubcatname : req.query.parentdatass, dataproduct : jsonObjectproducts,
       
                  });

                });

              });

            });

          });  

        });

      });

    });

     

    app.get('/purchasereturn-list', isLoggedIn, function(req, res) {

        var Purchasereturn = require('../app/models/purchasereturn');
        var Supplier = require('../app/models/suppliers');
        var User = require('../app/models/user');
        var i = 0;
        var vm = this;
          vm.website = null;
          vm.allwebsites = [];
          vm.purchase = null;
          vm.allpurchase = [];
          vm.supplier = null;
          vm.allsupplier = [];

          var Website  = require('../app/models/website');

          Website.find(function (err, website) {

          for(var i = 0; i < website.length; i++){

            vm.allwebsites[i] = website[i];

          }

          var productvaluewebsite = JSON.stringify(website);
          var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

          Purchasereturn.find(function (err, purchase) {
          
          for(var i = 0; i < purchase.length; i++){

              vm.allpurchase[i] = purchase[i];
          }

          var purchasevalue = JSON.stringify(purchase);
          var jsonObjectpurchase = JSON.parse(purchasevalue);

          Supplier.find(function (err, supplier) {
          
          for(var i = 0; i < supplier.length; i++){
              vm.allsupplier[i] = supplier[i];
          }

          var suppliervalue = JSON.stringify(supplier);
          var jsonObjectsupplier = JSON.parse(suppliervalue);

          res.render('purchasereturn-list.ejs', {  datasupplier : jsonObjectsupplier , loadwebsitedata : jsonObjectwebsite, datapurchase : jsonObjectpurchase, user : req.user});

        });

      });

    });

  });


    app.get('/deletepurchasereturn', isLoggedIn, function(req, res) {

      var Purchasereturn = require('../app/models/purchasereturn');

      var PurchaseItemreturn = require('../app/models/purchaseItemreturn');

        Purchasereturn.remove({_id : req.query.id}, function (err, purchase) {
            
          PurchaseItemreturn.remove({purchaseId : req.query.id}, function (err, purchaseItem) {
           
            return res.redirect('purchasereturn-list');

          });

        });

    });
   
  
    app.get('/purchaseReturnDetail', isLoggedIn, function(req, res) {

      var Product = require('../app/models/products');
      var User = require('../app/models/user');
      var Supplier  = require('../app/models/suppliers');
      var Order  = require('../app/models/orders');
      var PurchaseReturn = require('../app/models/purchasereturn');
      var PurchaseItemReturn = require('../app/models/purchaseItemreturn');

      var i = 0;
      var vm = this;
          vm.website = null;
          vm.allwebsites = [];
          vm.product = null;
          vm.allproducts = [];
          vm.productDetail = null;
          vm.allproductDetails = [];
          vm.supplier = null;
          vm.allsuppliers = [];
          vm.purchase = null;
          vm.allpurchase = [];
          vm.purchaseitem = null;
          vm.allpurchaseitem = [];
          vm.order = null;
          vm.allorders = [];

       var Website = require('../app/models/website');


          Website.find(function (err, website) {

        for(var i = 0; i < website.length; i++){
              vm.allwebsites[i] = website[i];

          }

          var productvaluewebsite = JSON.stringify(website);
          var jsonObjectwebsite = JSON.parse(productvaluewebsite);

          Product.find(function (err, product) {
     
        for(var i = 0; i < product.length; i++){

            vm.allproducts[i] = product[i];

          }
   
          var productsvalue = JSON.stringify(product);
          var jsonObjectproducts = JSON.parse(productsvalue);

         Order.find(function (err, order) {
   
        for(var i = 0; i < order.length; i++){
         
          vm.allorders[i] = order[i];
       
        }
   
         var ordervalue = JSON.stringify(order);
         var jsonObjectorder = JSON.parse(ordervalue);

          Product.find({_id : req.query.proid},function (err, productDetail) {
     
        for(var i = 0; i < productDetail.length; i++){
          vm.allproductDetails[i] = productDetail[i];

        }
   
        var productDetailsvalue = JSON.stringify(productDetail);
        var jsonObjectproductDetails = JSON.parse(productDetailsvalue);

        Supplier.find(function (err, supplier) {
       
        for(var i = 0; i < supplier.length; i++){
            vm.allsuppliers[i] = supplier[i];
        }
   
        var suppliersvalue = JSON.stringify(supplier);
        var jsonObjectsuppliers = JSON.parse(suppliersvalue);

        PurchaseReturn.find(function (err, purchase) {
     
        for(var i = 0; i < purchase.length; i++){
            vm.allpurchase[i] = purchase[i];


        }
   
         var purchasevalue = JSON.stringify(purchase);
         var jsonObjectpurchase = JSON.parse(purchasevalue);

        PurchaseItemReturn.find(function (err, purchaseitem) {
   
        for(var i = 0; i < purchaseitem.length; i++){
            vm.allpurchaseitem[i] = purchaseitem[i];

        }
   
        var purchaseitemvalue = JSON.stringify(purchaseitem);
        var jsonObjectpurchaseitem = JSON.parse(purchaseitemvalue);
       
        res.render('purchaseReturnDetail.ejs', { loadorderdata  : jsonObjectorder,  supId : req.query.id, datapurchaseitem : jsonObjectpurchaseitem , datapurchase : jsonObjectpurchase , loadsupplierdata : jsonObjectsuppliers,  loadproductdetail : jsonObjectproductDetails, user : req.user , loadwebsitedata : jsonObjectwebsite, dataproduct : jsonObjectproducts,
                     
                    });

                  });

                });

              });

            });  

          });  

        });

      });

    });

        
  ////////////////////// Purchase Return End ///////////////////////////////////

 ///////////////////////////// Sales Return Start ////////////////////////////////

    app.post('/salesReturn-insert',isLoggedIn, function(req, res) {

      var SalesReturn = require('../app/models/salesreturn');
      var SalesItemReturn = require('../app/models/salesitemreturn');
      var strDate = (new Date()).toISOString().split('T')[0]; 
      var vch = req.body.voucherNo;
      var usrid = req.body.supId;

      var set = {

              supId : req.body.supId,
              GrandTotalAmnt : req.body.GrandTotalAmnt,
              SgstTotalAmnt : req.body.SgstTotalAmnt,
              CgstTotalAmnt : req.body.CgstTotalAmnt,
              IgstTotalAmnt : req.body.IgstTotalAmnt,
              FinalTotalAmnt : req.body.FinalTotalAmnt,
              TotalDecimalAmnt : req.body.TotalDecimalAmnt,
              EditAmount : req.body.EditAmount,
              voucherNo : req.body.voucherNo,
              creditNo : req.body.creditNo,
              creditDate : req.body.creditDate,
              orderId : req.body.orderId,
              salesStatus : "0",
              datetime : strDate

          };

       var salesReturn = new SalesReturn(set);

          salesReturn.save(set, function (err, doc) {

        for(var n = 0; n < req.body.part.length; n++){

          var sets = {          

              salesId : doc._id,
              partId : req.body.part[n],
              SellPrice : req.body.SellPrice[n],
              TotalAmnt : req.body.TotalAmnt[n],
              Tax : req.body.Tax[n],
              Discount : req.body.Discount[n],
              Quantity : req.body.Quantity[n],
              Price : req.body.Price[n],
              Per : req.body.Per[n],
              DiscountAmnt : req.body.DiscountAmnt[n],
              Sgst : req.body.Sgst[n],
              Cgst : req.body.Cgst[n],
              Igst : req.body.Igst[n],
              TotalAmntData : req.body.TotalAmntData[n],
              part : req.body.browser[n],
              voucherNo : vch,
              userId : usrid,
              salesStatus : "0",
              datetime  : strDate

            };

          var salesItemReturn = new SalesItemReturn(sets);

            salesItemReturn.save(sets,function (err, docss) {
                 
            }); 

          }

        });

      return res.redirect('salesreturn-list');
   
    });

    app.get('/edit-salesReturn', isLoggedIn, function(req, res) {

      var Product = require('../app/models/products');
      var User  = require('../app/models/user');  
      var Supplier   = require('../app/models/suppliers');
      var SalesReturn = require('../app/models/salesreturn');
      var SalesItemReturn = require('../app/models/salesitemreturn');
      var Order  = require('../app/models/orders');

      var i = 0;
      var vm = this;
          vm.website = null;
          vm.allwebsites = [];
          vm.product = null;
          vm.allproducts = [];
          vm.productDetail = null;
          vm.allproductDetails = [];
          vm.supplier = null;
          vm.allsuppliers = [];
          vm.sales = null;
          vm.allsales = [];
          vm.salesitem = null;
          vm.allsalesitem = [];
          vm.order = null;
          vm.allorders = [];

         var Website  = require('../app/models/website');

          Website.find(function (err, website) {

          for(var i = 0; i < website.length; i++){
              vm.allwebsites[i] = website[i];

          }

          var productvaluewebsite = JSON.stringify(website);
          var jsonObjectwebsite = JSON.parse(productvaluewebsite);

          Product.find({},function (err, product) {
       
          for(var i = 0; i < product.length; i++){

              vm.allproducts[i] = product[i];

          }
   
          var clean = product.filter((product, index, self) => index === self.findIndex((t) => (t.partId === product.partId)));
          var productsvalue = JSON.stringify(clean);
          var jsonObjectproducts = JSON.parse(productsvalue);

          User.find(function (err, user) {

          for(var i = 0; i < user.length; i++){
              vm.alluser[i] = user[i];

          }

          var  productvalueuser = JSON.stringify(user);
          var jsonObjectuser = JSON.parse(productvalueuser);
   
          Product.find({_id : req.query.proid},function (err, productDetail) {
     
          for(var i = 0; i < productDetail.length; i++){
              vm.allproductDetails[i] = productDetail[i];


          }
   
          var productDetailsvalue = JSON.stringify(productDetail);
          var jsonObjectproductDetails = JSON.parse(productDetailsvalue);

          Supplier.find(function (err, supplier) {
     
          for(var i = 0; i < supplier.length; i++){
              vm.allsuppliers[i] = supplier[i];


          }
   
          var suppliersvalue = JSON.stringify(supplier);
          var jsonObjectsuppliers = JSON.parse(suppliersvalue);

          SalesReturn.find(function (err, sales) {
     
          for(var i = 0; i < sales.length; i++){
              vm.allsales[i] = sales[i];


          }
   
          var salesvalue = JSON.stringify(sales);
          var jsonObjectsales = JSON.parse(salesvalue);

          SalesItemReturn.find(function (err, salesitem) {
     
          for(var i = 0; i < salesitem.length; i++){
              vm.allsalesitem[i] = salesitem[i];


          }
   
          var salesitemvalue = JSON.stringify(salesitem);            
          var jsonObjectsalesitem = JSON.parse(salesitemvalue);

          Order.find(function (err, order) {
     
          for(var i = 0; i < order.length; i++){
              vm.allorders[i] = order[i];


          }
   
          var ordervalue = JSON.stringify(order);            
          var jsonObjectorder = JSON.parse(ordervalue);

          res.render('edit-salesReturn.ejs', {loaduserdata : jsonObjectuser , loadorderdata : jsonObjectorder , supId : req.query.id , datasales : jsonObjectsales , datasalesdetail : jsonObjectsalesitem, loadsupplierdata : jsonObjectsuppliers,  loadproductdetail : jsonObjectproductDetails, user : req.user , loadwebsitedata : jsonObjectwebsite, dataproduct : jsonObjectproducts,
     
                     
                      });

                    });

                  });

                }); 

              });

            });

          });  

        });  

      });

    });

   

    app.get('/salesReturn', isLoggedIn, function(req, res) {

      var PurchaseItem   = require('../app/models/purchaseItem');
      var Product = require('../app/models/products');
      var SalesReturn = require('../app/models/salesreturn');
      var User  = require('../app/models/user');    
      var Supplier  = require('../app/models/suppliers');
      var Order  = require('../app/models/orders');
      var i = 0;
      var vm = this;
          vm.website = null;
          vm.allwebsites = [];
          vm.product = null;
          vm.allproducts = [];
          vm.supplier = null;
          vm.allsupplier = [];
          vm.order = null;
          vm.allorder = [];
          vm.sales = null;
          vm.allsales = [];
          vm.purchaseitem = null;
          vm.allpurchaseitem = [];

          var Website = require('../app/models/website');

          PurchaseItem.find(function (err, purchaseitem) {
         
            for(var i = 0; i < purchaseitem.length; i++){
                vm.allpurchaseitem[i] = purchaseitem[i];


            }
     
          var purchaseitemvalue = JSON.stringify(purchaseitem);
          var jsonObjectpurchaseitem = JSON.parse(purchaseitemvalue);


          Website.find(function (err, website) {
 
        for(var i = 0; i < website.length; i++){
              vm.allwebsites[i] = website[i];

          }

         var productvaluewebsite = JSON.stringify(website);
         var jsonObjectwebsite = JSON.parse(productvaluewebsite);
    
          Product.find(function (err, product) {
      
        for(var i = 0; i < product.length; i++){

            vm.allproducts[i] = product[i];

        }

          var clean = product.filter((product, index, self) => index === self.findIndex((t) => (t.partId === product.partId)));
          var productsvalue = JSON.stringify(clean);
          var jsonObjectproducts = JSON.parse(productsvalue);

          Supplier.find(function (err, supplier) {

        for(var i = 0; i < supplier.length; i++){
              vm.allsupplier[i] = supplier[i];

          }

         var productvaluesupplier = JSON.stringify(supplier);
         var jsonObjectsupplier = JSON.parse(productvaluesupplier);

          User.find(function (err, user) {

        for(var i = 0; i < user.length; i++){
              vm.alluser[i] = user[i];

         }

         var  productvalueuser = JSON.stringify(user);
         var jsonObjectuser = JSON.parse(productvalueuser);

           Order.find(function (err, order) {

        for(var i = 0; i < order.length; i++){
              vm.allorder[i] = order[i];

          }

          var productvalueorder = JSON.stringify(order);
          var jsonObjectorder = JSON.parse(productvalueorder);
   
        SalesReturn.find({}).sort({_id:-1}).limit(1).exec(function(err, sales){
       
          for(var i = 0; i < sales.length; i++){
                vm.allsales[i] = sales[i];

            var invoiceNo = sales[i].invoiceNo;
            var voucharNo = sales[i].voucherNo;  

          }

          var salesvalueorder = JSON.stringify(sales);
          var jsonObjectsales = JSON.parse(salesvalueorder);

          res.render('salesreturn.ejs', { datapurchaseitem : jsonObjectpurchaseitem, loaduserdata : jsonObjectuser , invoiceNodata: invoiceNo , voucherNoData : voucharNo, loadsalesdata: jsonObjectsales , loadorderdata : jsonObjectorder , user : req.user , loadsupplierdata : jsonObjectsupplier, loadwebsitedata : jsonObjectwebsite, dataproduct : jsonObjectproducts,
 

                    });
             
                  });
       
                });
       
              });  
       
            });
   
          });
   
        });
   
      });    

    });


  app.get('/salesreturn-list', isLoggedIn, function(req, res) {

      var SalesReturn = require('../app/models/salesreturn');
      var User = require('../app/models/user');
      var i = 0;
      var vm = this;
      vm.users = null;
        vm.allusers = [];
        vm.website = null;
        vm.allwebsites = [];
        vm.sales = null;
        vm.allsales = [];

        var Website = require('../app/models/website');

        Website.find(function (err, website) {

        for(var i = 0; i < website.length; i++){
              vm.allwebsites[i] = website[i];

        }

        var productvaluewebsite = JSON.stringify(website);
        var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

        User.find(function (err, users) {
     
        for(var i = 0; i < users.length; i++){
            vm.allusers[i] = users[i];
        }

        var productvalueusers = JSON.stringify(users);
        var jsonObjectusers = JSON.parse(productvalueusers);

        SalesReturn.find(function (err, sales) {
   
        for(var i = 0; i < sales.length; i++){
            vm.allsales[i] = sales[i];
        }

        var salesvalue = JSON.stringify(sales);
        var jsonObjectsales = JSON.parse(salesvalue);
 
      res.render('salesreturn-list.ejs', { datasales : jsonObjectsales , loadwebsitedata : jsonObjectwebsite, datauser: jsonObjectusers, user : req.user});


        });

      });

    });

  });  

  
     
    //////////////// sales-update /////////////////////

    app.post('/updatesales',isLoggedIn, function(req, res) {

      var Product = require('../app/models/products');
      var strDate = (new Date()).toISOString().split('T')[0]; 
      var Sales  = require('../app/models/sales');
      var vch = req.body.voucherNo;  
      var usrid =  req.body.supId;  
      var SalesItem  = require('../app/models/salesitem');
     
      var _id = req.body.Prodid;
     
        SalesItem.find({"salesId":req.body.Prodid}, function (err, productst) {

            // console.log("productst");

            SalesItem.remove({"salesId":req.body.Prodid}, function (err, purchasedfataid) {

              // console.log(purchasedfataid);
              // console.log("data removedd");  

          });

        });

        for(var n = 0; n < req.body.partId.length; n++){
     
      Product.updateMany({ partId: req.body.part[n]},{ $set: { salesperprice  : req.body.salesperprice[n]}}, function (err, doc) {
          if(err){
            res.json(err);
          } else {
            console.log('update');
          }        
         
        });

      }

      Sales.findById({"_id":req.body.Prodid}, function (err, productst) {
        
        var sets = {

                      supId : req.body.supId,
                      GrandTotalAmnt : req.body.GrandTotalAmnt,
                      SgstTotalAmnt: req.body.SgstTotalAmnt,
                      CgstTotalAmnt: req.body.CgstTotalAmnt,
                      IgstTotalAmnt: req.body.IgstTotalAmnt,
                      FinalTotalAmnt : req.body.FinalTotalAmnt,
                      TotalDecimalAmnt : req.body.TotalDecimalAmnt,
                      EditAmount : req.body.EditAmount,
                      invoiceNo : req.body.invoiceNo,
                      voucherNo : req.body.voucherNo,
                      SupDate : req.body.SupDate,
                      orderId : req.body.orderId,
                      salesStatus : "0",
                      datetime     : strDate
                   
                  };
               
            Sales.update({ _id: req.body.Prodid },{ $set: sets},function (err, doc) {

                var parttt = req.body.partId;

              for(var n = 0; n < req.body.partId.length; n++){  

                  var datasetk = {

                          salesId : req.body.Prodid,
                          partId : req.body.partId[n],
                          SellPrice : req.body.SellPrice[n],
                          TotalAmnt : req.body.TotalAmnt[n],
                          Tax  : req.body.Tax[n],
                          Discount : req.body.Discount[n],
                          Quantity  : req.body.Quantity[n],
                          Price : req.body.Price[n],
                          Per : req.body.Per[n],
                          DiscountAmnt : req.body.DiscountAmnt[n],
                          Sgst: req.body.Sgst[n],
                          Cgst: req.body.Cgst[n],
                          Igst: req.body.Igst[n],
                          TotalAmntData : req.body.TotalAmntData[n],
                          salesperprice : req.body.salesperprice[n],
                          part : req.body.part[n],
                          voucherNo : vch,
                          userId : usrid,
                          salesStatus : "0",
                          datetime : strDate

                        };

                    var salesItem = new SalesItem(datasetk);

                       salesItem.save(function(error, doc){

                          if(error){

                            res.json(error);

                          } else {  

                            // console.log("data inserted");

                  }

              });          

            }

          });  
                 
        });

      return res.redirect('sales-list');
      
    });

    app.post('/updatesalesReturn',isLoggedIn, function(req, res) {

      var Product = require('../app/models/products');
      var strDate = (new Date()).toISOString().split('T')[0]; 
        var SalesReturn = require('../app/models/salesreturn');
        var SalesItemReturn = require('../app/models/salesitemreturn');
          var _id = req.body.Prodid;
          var vch = req.body.voucherNo;
          var uID =  req.body.supId;
       
          SalesItemReturn.find({"salesId":req.body.Prodid}, function (err, productst) {
           
              SalesItemReturn.remove({"salesId":req.body.Prodid}, function (err, purchasedfataid) {
             
            });

          });


          for(var s = 0; s < req.body.partId.length; s++){
     
        Product.updateMany({ partId: req.body.part[s]},{ $set: { quantity  : req.body.finalqtydata[s]}}, function (err, doc) {
            if(err){

              res.json(500);
            } else {
             
            }        
           
          });

        };

        SalesReturn.findById({"_id":req.body.Prodid}, function (err, productst) {

            var sets = {

                        supId : req.body.supId,
                        GrandTotalAmnt : req.body.GrandTotalAmnt,
                        SgstTotalAmnt: req.body.SgstTotalAmnt,
                        CgstTotalAmnt: req.body.CgstTotalAmnt,
                        IgstTotalAmnt: req.body.IgstTotalAmnt,
                        FinalTotalAmnt : req.body.FinalTotalAmnt,
                        TotalDecimalAmnt : req.body.TotalDecimalAmnt,
                        EditAmount : req.body.EditAmount,
                        creditNo : req.body.creditNo,
                        creditDate : req.body.creditDate,
                        orderId : req.body.orderId,
                        voucherNo : req.body.voucherNo,
                        salesStatus : "0",
                        datetime : strDate
                     
                    };
                 
              SalesReturn.update({ _id: req.body.Prodid },{ $set: sets},function (err, doc) {

                for(var n = 0; n < req.body.partId.length; n++){  

                    var datasetk = {

                            salesId : req.body.Prodid,
                            partId : req.body.partId[n],
                            SellPrice : req.body.SellPrice[n],
                            TotalAmnt : req.body.TotalAmnt[n],
                            Tax : req.body.Tax[n],
                            Discount : req.body.Discount[n],
                            Quantity : req.body.Quantity[n],
                            Price : req.body.Price[n],
                            Per : req.body.Per[n],
                            DiscountAmnt : req.body.DiscountAmnt[n],
                            Sgst: req.body.Sgst[n],
                            Cgst: req.body.Cgst[n],
                            Igst: req.body.Igst[n],
                            TotalAmntData : req.body.TotalAmntData[n],
                            finalqtydata : req.body.finalqtydata[n],
                            part : req.body.part[n],
                            voucherNo : vch,
                            userId : uID,
                            salesStatus : "0",
                            datetime : strDate

                          };
              
              var salesItemReturn = new SalesItemReturn(datasetk);

                  salesItemReturn.save(function(error, doc){

                if(error) {

                    res.json(error);

                } else {  

                         
                }

              });          

            }

          });  
                   
        });            

        return res.redirect('salesreturn-list');
     
    });

     //////sales-return insert ////////////
 
    app.post('/salesReturn-insert',isLoggedIn, function(req, res) {

      var Product = require('../app/models/products');
      var SalesReturn = require('../app/models/salesreturn');
      var SalesItemReturn = require('../app/models/salesitemreturn');
      var User = require('../app/models/user');
      var dates =  new Date();
      var strDate = (new Date()).toISOString().split('T')[0];
      var vch = req.body.voucherNo; 
      var usrid = req.body.orderId;

      var vm = this;
          vm.website = null;
          vm.allwebsites = [];
        
       var Website = require('../app/models/website');

          for(var s = 0; s < req.body.part.length; s++){
     
        Product.updateMany({ partId: req.body.browser[s]},{ $set: { quantity  : req.body.finalqtydata[s]}}, function (err, doc) {
            if(err){

              res.json(500);
            } else {
             
            }        
           
          });

        }

       var Website = require('../app/models/website');

       Website.find(function (err, website) {

       for(var i = 0; i < website.length; i++){
              vm.allwebsites[i] = website[i];

          }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);
         
          var set = {

              supId : req.body.supId,
              GrandTotalAmnt : req.body.GrandTotalAmnt,
              SgstTotalAmnt : req.body.SgstTotalAmnt,
              CgstTotalAmnt : req.body.CgstTotalAmnt,
              IgstTotalAmnt : req.body.IgstTotalAmnt,
              FinalTotalAmnt : req.body.FinalTotalAmnt,
              TotalDecimalAmnt : req.body.TotalDecimalAmnt,
              EditAmount : req.body.EditAmount,
              voucherNo : req.body.voucherNo,
              creditNo : req.body.creditNo,
              creditDate : req.body.creditDate,
              orderId : req.body.orderId,
              salesStatus : "0",
              datetime : strDate

          };

       var salesReturn = new SalesReturn(set);

          salesReturn.save(set, function (err, doc) {

        for(var n = 0; n < req.body.part.length; n++){

          var sets = {          

              salesId : doc._id,
              partId : req.body.part[n],
              SellPrice : req.body.SellPrice[n],
              TotalAmnt : req.body.TotalAmnt[n],
              Tax : req.body.Tax[n],
              Discount : req.body.Discount[n],
              Quantity : req.body.Quantity[n],
              Price : req.body.Price[n],
              Per : req.body.Per[n],
              DiscountAmnt : req.body.DiscountAmnt[n],
              Sgst : req.body.Sgst[n],
              Cgst : req.body.Cgst[n],
              Igst : req.body.Igst[n],
              TotalAmntData : req.body.TotalAmntData[n],
              finalqtydata : req.body.finalqtydata[n],         
              part : req.body.browser[n],
              voucherNo : vch,
              userID : usrid,
              salesStatus : "0",
              datetime  : strDate

            };

          var salesItemReturn = new SalesItemReturn(sets);

            salesItemReturn.save(sets,function (err, docss) {
                 
            }); 

          }

          });

        return res.redirect('salesreturn-list');
     
      });

    });

    app.get('/deletesalesReturn',isLoggedIn, function(req, res) {
   
      var SalesReturn = require('../app/models/salesreturn');

        var SalesItemReturn = require('../app/models/salesitemreturn');

          SalesReturn.remove({_id : req.query.id}, function (err, sales) {

           SalesItemReturn.remove({salesId : req.query.id}, function (err, salesitem) {

             return res.redirect('salesreturn-list');

          });

      });

    });

    app.get('/salesDetailReturn', isLoggedIn, function(req, res) {

      var Product  = require('../app/models/products');
      var User = require('../app/models/user');
      var Order = require('../app/models/orders');
      var SalesReturn = require('../app/models/salesreturn');
      var SalesItemReturn = require('../app/models/salesitemreturn');
      var i = 0;
      var vm = this;
          vm.website = null;
          vm.allwebsites = [];
          vm.product = null;
          vm.allproducts = [];
          vm.productDetail = null;
          vm.allproductDetails = [];
          vm.sales = null;
          vm.allsales = [];
          vm.salesitem = null;
          vm.allsalesitem = [];
          vm.order = null;
          vm.allorders = [];

          var Website = require('../app/models/website');


          Website.find(function (err, website) {

          for(var i = 0; i < website.length; i++){
                vm.allwebsites[i] = website[i];

          }

          var productvaluewebsite = JSON.stringify(website);
          var jsonObjectwebsite = JSON.parse(productvaluewebsite);

          Product.find(function (err, product) {
   
          for(var i = 0; i < product.length; i++){
              vm.allproducts[i] = product[i];
             

          }
   
          var productsvalue = JSON.stringify(product);
          var jsonObjectproducts = JSON.parse(productsvalue);

          Order.find(function (err, order) {
     
          for(var i = 0; i < order.length; i++){
              vm.allorders[i] = order[i];

          }
   
          var ordervalue = JSON.stringify(order);
          var jsonObjectorder = JSON.parse(ordervalue);

          Product.find({_id : req.query.proid},function (err, productDetail) {
     
          for(var i = 0; i < productDetail.length; i++){
              vm.allproductDetails[i] = productDetail[i];
             

          }
   
          var productDetailsvalue = JSON.stringify(productDetail);
          var jsonObjectproductDetails = JSON.parse(productDetailsvalue);

          User.find(function (err, user) {

            for(var i = 0; i < user.length; i++){
                vm.alluser[i] = user[i];

            }

            var productvalueuser = JSON.stringify(user);
            var jsonObjectuser = JSON.parse(productvalueuser);

         SalesReturn.find(function (err, sales) {
     
          for(var i = 0; i < sales.length; i++){
              vm.allsales[i] = sales[i];


          }
   
            var salesvalue = JSON.stringify(sales);
            var jsonObjectsales = JSON.parse(salesvalue);

          SalesItemReturn.find(function (err, salesitem) {
        
          for(var i = 0; i < salesitem.length; i++){
              vm.allsalesitem[i] = salesitem[i];
          
          }
   
            var salesitemvalue = JSON.stringify(salesitem);
            var jsonObjectsalesitem = JSON.parse(salesitemvalue);

          res.render('salesDetailReturn.ejs', { loaduserdata : jsonObjectuser, loadorderdata  : jsonObjectorder,  supId : req.query.id,datasalesitem : jsonObjectsalesitem , datasales : jsonObjectsales ,  loadproductdetail : jsonObjectproductDetails, user : req.user , loadwebsitedata : jsonObjectwebsite, dataproduct : jsonObjectproducts,
           
                    });

                  });

                });

              });
 
            });  

          });  

        });

      });

    });


    app.post('/dateSortSalesReturn',isLoggedIn, function(req, res) {

      var Website = require('../app/models/website');
      var Product = require('../app/models/products');
      var User = require('../app/models/user');
      var Category  = require('../app/models/category');
      var SubCategory  = require('../app/models/subcategory');
      var secondSubCategory  = require('../app/models/secondsubcategory');
      var thirdSubCategory = require('../app/models/thirdsubcategory');
      var Size = require('../app/models/size');
      var Color  = require('../app/models/color');
      var Supplier  = require('../app/models/suppliers');
      var Unit = require('../app/models/unit');
      var Order  = require('../app/models/orders');
      var Purchase = require('../app/models/purchase');
      var PurchaseItem = require('../app/models/purchaseItem');
      var SalesReturn = require('../app/models/salesreturn');
      var SalesItemReturn = require('../app/models/salesitemreturn');
      var i = 0;
      var vm = this;
         vm.website = null;
          vm.allwebsites = [];
          vm.category = null;
          vm.allcategory = [];
          vm.subcategory = null;
          vm.allsubcategory = [];
          vm.secondsubcategory = null;
          vm.allsecondsubcategory = [];
          vm.thirdSubCategory = null;
          vm.allthirdSubCategorys = [];
          vm.product = null;
          vm.allproducts = [];
          vm.size = null;
          vm.allsize = [];
          vm.color = null;
          vm.allcolor = [];
          vm.supplier = null;
          vm.allsupplier = [];
          vm.unit = null;
          vm.allunit = [];
          vm.order = null;
          vm.allorder = [];
          vm.purchase = null;
          vm.allpurchase = [];
          vm.sales = null;
          vm.allsales = [];

         Category.find(function (err, category) {

       for(var i = 0; i < category.length; i++){
              vm.allcategory[i] = category[i];

          }

         var productvaluecategory = JSON.stringify(category);
         var jsonObjectcategory = JSON.parse(productvaluecategory);

        SubCategory.find(function (err, subcategory) {

       for(var i = 0; i < subcategory.length; i++){
              vm.allsubcategory[i] = subcategory[i];

          }

         var productvaluesubcategory = JSON.stringify(subcategory);
         var jsonObjectsubcategory = JSON.parse(productvaluesubcategory);

        secondSubCategory.find(function (err, secondsubcategory) {

       for(var i = 0; i < secondsubcategory.length; i++){
              vm.allsecondsubcategory[i] = secondsubcategory[i];

          }

         var productvaluesecondsubcategory = JSON.stringify(secondsubcategory);
         var jsonObjectsecondsubcategory = JSON.parse(productvaluesecondsubcategory);
       
       
       // console.log(jsonObjectsubcategorys);
          thirdSubCategory.find(function (err, thirdSubCategory) {
        // docs is an array
          for(var i = 0; i < thirdSubCategory.length; i++){
              vm.allthirdSubCategorys[i] = thirdSubCategory[i];


          }
   
         var thirdSubCategorysvalue = JSON.stringify(thirdSubCategory);
         var jsonObjectthirdSubCategorys = JSON.parse(thirdSubCategorysvalue);

         Size.find(function (err, size) {

       for(var i = 0; i < size.length; i++){
              vm.allsize[i] = size[i];

          }

         var productvaluesize = JSON.stringify(size);
         var jsonObjectsize = JSON.parse(productvaluesize);

        Color.find(function (err, color) {

       for(var i = 0; i < color.length; i++){
              vm.allcolor[i] = color[i];

          }

         var productvaluecolor = JSON.stringify(color);
         var jsonObjectcolor = JSON.parse(productvaluecolor);

       Website.find(function (err, website) {

       for(var i = 0; i < website.length; i++){
              vm.allwebsites[i] = website[i];

          }

         var productvaluewebsite = JSON.stringify(website);
         var jsonObjectwebsite = JSON.parse(productvaluewebsite);

          // console.log(jsonObjectsubcategorys);
          Product.find(function (err, product) {
        // docs is an array
          for(var i = 0; i < product.length; i++){
              vm.allproducts[i] = product[i];


          }
   
         var productsvalue = JSON.stringify(product);
         var jsonObjectproducts = JSON.parse(productsvalue);


       Supplier.find(function (err, supplier) {

       for(var i = 0; i < supplier.length; i++){
              vm.allsupplier[i] = supplier[i];

          }

         var productvaluesupplier = JSON.stringify(supplier);
         var jsonObjectsupplier = JSON.parse(productvaluesupplier);


           Unit.find(function (err, unit) {

       for(var i = 0; i < unit.length; i++){
              vm.allunit[i] = unit[i];

          }

         var productvalueunit = JSON.stringify(unit);
         var jsonObjectunit = JSON.parse(productvalueunit);


           Order.find(function (err, order) {

       for(var i = 0; i < order.length; i++){
              vm.allorder[i] = order[i];

          }

         var productvalueorder = JSON.stringify(order);
         var jsonObjectorder = JSON.parse(productvalueorder);


      SalesReturn.find({$or: [{"datetime": {$gte: req.body.startDate, $lte: req.body.endDate}}]},function (err, sales) {

             console.log(sales);

      res.render('sales-listDate.ejs', { datasales : sales , loadorderdata : jsonObjectorder , user : req.user ,loadunitdata: jsonObjectunit , datasupplier : jsonObjectsupplier, loadwebsitedata : jsonObjectwebsite, loadcategorydata : jsonObjectcategory, loadsubcategorydata : jsonObjectsubcategory, loadsecondsubcategorydata : jsonObjectsecondsubcategory, loadthirdsubcategorydata : jsonObjectthirdSubCategorys , loadsizedata:jsonObjectsize, loadcolordata : jsonObjectcolor, catname : req.query.cat , subcatname : req.query.subcat , secsubcatname : req.query.thirdsubcat , thirsubcatname : req.query.parentdatass, dataproduct : jsonObjectproducts,

              });
                               
                                });
                         
                              });
                   
                            });
                   
                          });  
                   
                        });
               
                      });
               
                    });
               
                  });    

                });

            });    

        });

      });  

    });  

  ///////////////////////////// Sales Return End //////////////////////////////////

    app.post('/updatesupplier',isLoggedIn, function(req, res) {

      var Category = require('../app/models/category');
      var Supplier = require('../app/models/suppliers');
      var SubCategory = require('../app/models/subcategory');
      var Size = require('../app/models/size');
      var Color = require('../app/models/color');
      var Product       = require('../app/models/products');
      var dates=  new Date();
      var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();
      // console.log(req.body);
      var _id = req.body.SupId;
       // console.log(_id);
      var i = 0;
      var vm = this;
          vm.products = null;
          vm.allproducts = [];
          vm.website = null;
          vm.allwebsites = [];
          vm.category = null;
          vm.allcategory = [];
          vm.subcategory = null;
          vm.allsubcategory = [];
          vm.size = null;
          vm.allsize = [];
          vm.color = null;
          vm.allcolor = [];
          vm.supplier = null;
          vm.allsuppliers = [];

       var Website       = require('../app/models/website');

        Size.find(function (err, size) {

       for(var i = 0; i < size.length; i++){
              vm.allsize[i] = size[i];

          }

         var productvaluesize = JSON.stringify(size);
         var jsonObjectsize = JSON.parse(productvaluesize);

        Color.find(function (err, color) {

       for(var i = 0; i < color.length; i++){
              vm.allcolor[i] = color[i];

          }

         var productvaluecolor = JSON.stringify(color);
         var jsonObjectcolor = JSON.parse(productvaluecolor);

       Category.find(function (err, category) {

       for(var i = 0; i < category.length; i++){
              vm.allcategory[i] = category[i];

          }

         var productvaluecategory = JSON.stringify(category);
         var jsonObjectcategory = JSON.parse(productvaluecategory);

        SubCategory.find(function (err, subcategory) {

       for(var i = 0; i < subcategory.length; i++){
              vm.allsubcategory[i] = subcategory[i];

          }

         var productvaluesubcategory = JSON.stringify(subcategory);
         var jsonObjectsubcategory = JSON.parse(productvaluesubcategory);


       Website.find(function (err, website) {

       for(var i = 0; i < website.length; i++){
              vm.allwebsites[i] = website[i];

          }

         var productvaluewebsite = JSON.stringify(website);
         var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      Supplier.find(function (err, supplier) {
        // docs is an array
          for(var i = 0; i < supplier.length; i++){
              vm.allsuppliers[i] = supplier[i];
         }
          var suppliervalue = JSON.stringify(supplier);
          var jsonObjectsuppliers = JSON.parse(suppliervalue);


            var _id = req.body.SupId;
            console.log(_id);

           Supplier.findById({_id:req.body.SupId}, function (err, suppliers) {
               
              Supplier.update({ _id: req.body.SupId }, { $set: {

                          custId : req.body.custId,
                          supName: req.body.supName,
                          role: req.body.role,
                          address: req.body.address,
                          country: req.body.country,
                          state : req.body.state,
                          city : req.body.city,
                          pincode: req.body.pincode,
                          phone: req.body.phone,
                          mobile: req.body.mobile,
                          fax: req.body.fax,
                          email:req.body.email,
                          website:req.body.website,

                          bankName : req.body.bankName,
                          holderName : req.body.holderName,
                          accountNumber : req.body.accountNumber,
                          ifscCode : req.body.ifscCode,

                          panNo : req.body.panNo,
                          registerType : req.body.registerType,
                          gsit : req.body.gsit,
                          registerSet : req.body.registerSet
                 
                  }}, function (err, doc) {

                          //console.log(doc);  

                      });                

                    });
                 
                  });
                   
                return res.redirect('supplier-list');

              });

            });

          });  

        });

      });

    }); 


    app.post('/updatepurchase',isLoggedIn, function(req, res) {

      console.log("req.body.part " + req.body.part );

      console.log("req.body.browser " + req.body.browser ); 
             
      var Product = require('../app/models/products');
      var strDate = (new Date()).toISOString().split('T')[0]; 
      var Purchase = require('../app/models/purchase');
      var PurchaseItem  = require('../app/models/purchaseItem');
      var vch = req.body.voucherNo;

      var _id = req.body.Prodid;

      var supplierID = req.body.supId;

        PurchaseItem.find({"purchaseId":req.body.Prodid}, function (err, productst) {
            
            PurchaseItem.remove({"purchaseId":req.body.Prodid}, function (err, purchasedfataid) {
       
          });

        });

     
          for(var n = 0; n < req.body.part.length; n++){
        
        Product.updateMany({ partId: req.body.browser[n]},{ $set: { salecostprice  : req.body.salecostprice[n]}}, function (err, doc) {
             
            // console.log("doc " + doc); 
        
          });

        } 

           
          for(var s = 0; s < req.body.part.length; s++){
        
        Product.updateMany({ partId: req.body.browser[s]},{ $set: { quantity  : req.body.finalqtydata[s]}}, function (err, doc) {
            
         
          });

        } 
                  

        Purchase.findById({"_id":req.body.Prodid}, function (err, productst) {

          var sets = { 

                      supId : req.body.supId,
                      GrandTotalAmnt : req.body.GrandTotalAmnt,
                      SgstTotalAmnt: req.body.SgstTotalAmnt,
                      CgstTotalAmnt: req.body.CgstTotalAmnt,
                      IgstTotalAmnt: req.body.IgstTotalAmnt,
                      FinalTotalAmnt : req.body.FinalTotalAmnt,
                      TotalDecimalAmnt : req.body.TotalDecimalAmnt,
                      EditAmount : req.body.EditAmount,
                      invoiceNo : req.body.invoiceNo,
                      voucherNo : req.body.voucherNo,
                      SupDate : req.body.SupDate,
                      orderId : req.body.orderId,
                      purchaseStatus : "0",
                      datetime : strDate
                    
                  };
                
            Purchase.update({ _id: req.body.Prodid },{ $set: sets},function (err, doc) {

              for(var n = 0; n < req.body.browser.length; n++){  

                  var datasetk = {

                          purchaseId : req.body.Prodid,
                          partId : req.body.part[n],
                          SellPrice : req.body.SellPrice[n],
                          TotalAmnt : req.body.TotalAmnt[n],
                          Tax  : req.body.Tax[n],
                          Discount : req.body.Discount[n],
                          Quantity : req.body.Quantity[n],
                          Price : req.body.Price[n],
                          Per : req.body.Per[n],
                          DiscountAmnt : req.body.DiscountAmnt[n],
                          Sgst : req.body.Sgst[n],
                          Cgst : req.body.Cgst[n],
                          Igst : req.body.Igst[n],
                          TotalAmntData : req.body.TotalAmntData[n],
                          salecostprice : req.body.salecostprice[n],
                          finalqtydata : req.body.finalqtydata[n],
                          part : req.body.browser[n],
                          voucherNo : vch,
                          supId : supplierID,
                          purchaseStatus : "0",
                          datetime : strDate

                        };

              var purchaseItem = new PurchaseItem(datasetk);

                purchaseItem.save(function(error, doc){

                  if(error){

                    res.json(error);

                  } else {   

                    // console.log("data inserted");

                } 

              });           

            }

          });  
                  
        });

      return res.redirect('purchase-list');

    });
  
    app.post('/updateorderstatus', isLoggedIn, function(req, res){
   
      var Order = require('../app/models/orders');
      var id = req.body.dataid;
      var status = req.body.selectedValdata;

      Order.update({ _id: id }, { $set: {o_status : status } }, function (err, doc) {

        if(err) {

          res.jsonp({success : false});

        } else {

          res.jsonp({success : true});

        }

      });
   
    });

    app.post('/ordercancle', isLoggedIn, function(req, res){
     
      var Order = require('../app/models/orders');
      var id = req.body.dataid;
      var statusdata = req.body.Status;

      Order.update({ _id: id }, { $set: {o_status : statusdata } }, function (err, doc) {

        if(err) {

          res.jsonp({success : false});

        } else {

          res.jsonp({success : true});

        }

      });
   
    }); 

  app.get('/updateorder',isLoggedIn, function(req, res, next) {

    var Website = require('../app/models/website');
    var Unit = require('../app/models/unit');
    var Orderitem = require('../app/models/orderitem');
    var Product = require('../app/models/products');
    var Order = require('../app/models/orders');

      var mongodId = req.query.mongodId;

        var objectId = mongodId;
        newvalue = objectId;

      var id = req.query.iddd 

      var array = JSON.parse("[" + id + "]");

      var qunttit = req.query.qunttity
        
      var quntity = JSON.parse("[" + qunttit + "]");

            
      for (var j = 0; j < array.length; j++) { 

        var abs = make(array[j], quntity[j]);

      }

      function make( array, quntity){

        Product.findOne({partId : array}, function(err , answer) { 
             
            var ans = Number(answer.quantity) + Number(quntity);   
       
              
            Product.updateMany({ partId: array},{ $set: { quantity  : ans}}, function (err, doc) {

              if (err) throw(err); 


            });                   

        }); 

      }                

      Order.update({ _id: newvalue },{ $set: { o_status: "Cancelled" } }, function (err, doccc) {

        // console.log("Response second " + doccc);    

        if (err) throw(err);
            
          // callback();
            
      });
   
  }); 

    app.get('/deleteorder',isLoggedIn, function(req, res) {

       var Order = require('../app/models/orders');

         Order.remove({_id : req.query.order_id}, function (err, orders) {

          console.log(orders);  

            return res.redirect('manageorder');

        });

    });

    app.get('/account',isLoggedIn, function(req, res) {
    
      var i = 0;
      var vm = this;
        vm.website = null;
        vm.allwebsites = [];

       var Website = require('../app/models/website');

          Website.find(function (err, website) {

          for(var i = 0; i < website.length; i++){

              vm.allwebsites[i] = website[i];

          }
       
          var productvaluewebsite = JSON.stringify(website);
          var jsonObjectwebsite = JSON.parse(productvaluewebsite); 
      
        res.render('account.ejs', {loadwebsitedata : jsonObjectwebsite , user : req.user });

      });

    });   
  




  app.get('/managemake',isLoggedIn, function(req, res) {

    var i = 0;
      var vm = this;
        vm.consult = null;
        vm.allconsults = [];
        vm.make = null;
        vm.allmakes = [];
        vm.website = null;
        vm.allwebsites = [];

      var Website = require('../app/models/website');

      var Make = require('../app/models/make');

      Make.find(function (err, makes) {
      
        for(var i = 0; i < makes.length; i++){
            vm.allmakes[i] = makes[i];


        }
 
       var makesvalue = JSON.stringify(makes);
       var jsonObjectmakes = JSON.parse(makesvalue);

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }
     
       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

    var Consult       = require('../app/models/consults');
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

           //console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
     
        res.render('managemake.ejs', { makedata: jsonObjectmakes, loadwebsitedata : jsonObjectwebsite ,loadconsultdata: jsonObject, user : req.user ,totalreadconsultstatus : finalstatusconsult,});

      });

    });   
  
  });

  });

  app.get('/makeStatus', isLoggedIn, function(req, res) {
    var Make       = require('../app/models/make');
      var dates=  new Date();
      var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

        var _id = req.query.id;
        var statusval = req.query.status;

      var i = 0;
      var vm = this;
        vm.make = null;
        vm.allmakes = [];

        Make.find(function (err, makes) {
     
        for(var i = 0; i < makes.length; i++){
            vm.allmakes[i] = makes[i];
        }

        var makesvalue = JSON.stringify(makes);
        var jsonObjectmakes = JSON.parse(makesvalue);

         Make.findById({_id : req.query.id}, function (err, make) {
              
             console.log(make._id);

               Make.update(
                 { _id: req.query.id },
                 { $set: { 
                   status: req.query.status} 
              },
                 function (err, doc) {
                    // console.log(doc);        
                 });  

        });

      return res.redirect('/managemake');

    });

  });

  app.get('/deletemake',isLoggedIn, function(req, res) {

    var Make = require('../app/models/make');

      Make.remove({_id : req.query.id}, function (err, makes) {
           
        return res.redirect('/managemake');

      });

  });

  ///////////////////////////make ///////////////////////////////////

  app.get('/managemodel',isLoggedIn, function(req, res) {
     var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];
        vm.make = null;
        vm.allmakes = [];
        vm.model = null;
        vm.allmodels = [];
        vm.website = null;
        vm.allwebsites = [];

    var Website = require('../app/models/website');

    var Make = require('../app/models/make');

    var Model = require('../app/models/model');

     Make.find(function (err, makes) {
      // docs is an array
        for(var i = 0; i < makes.length; i++){
            vm.allmakes[i] = makes[i];


        }
 
       var makesvalue = JSON.stringify(makes);
       var jsonObjectmakes = JSON.parse(makesvalue);

        Model.find(function (err, models) {
      // docs is an array
        for(var i = 0; i < models.length; i++){
            vm.allmodels[i] = models[i];


        }
 
       var modelsvalue = JSON.stringify(models);
       var jsonObjectmodels = JSON.parse(modelsvalue);

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }
     
       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

    var Consult       = require('../app/models/consults');
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

           //console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
     
        res.render('managemodel.ejs', { modeldata : jsonObjectmodels , makedata: jsonObjectmakes, loadwebsitedata : jsonObjectwebsite ,loadconsultdata: jsonObject, user : req.user ,totalreadconsultstatus : finalstatusconsult,});

      });

    });   
  
  });

  });

 });    


app.post('/model-insert', isLoggedIn, function(req, res) {
   //console.log(res);
   var User       = require('../app/models/user');
   var Make       = require('../app/models/make');
   var Model       = require('../app/models/model');

   var i = 0;
  var vm = this;
        vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      

         var set = {
             modelname :req.body.modelname,
             makeId : req.body.makeId,
             status : "0"
             };

           var model = new Model(set);
           model.save(set,
            function (err, doc) {
             //console.log(doc);
            });
        
        
     return res.redirect('managemodel');

      });


   });  

 app.get('/deletmodel',isLoggedIn, function(req, res) {
     var Model       = require('../app/models/model');
       Model.remove({_id : req.query.id}, function (err, models) {
         // console.log(makes);    
    return res.redirect('/managemodel');
      });
  });


app.get('/modelStatus', isLoggedIn, function(req, res) {
   var Model       = require('../app/models/model');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

         var _id = req.query.id;
         var statusval = req.query.status;

         // console.log(_id);
    var i = 0;
   var vm = this;
        vm.model = null;
        vm.allmodels = [];
   Model.find(function (err, models) {
     // docs is an array
       for(var i = 0; i < models.length; i++){
            vm.allmodels[i] = models[i];
       }
        var modelsvalue = JSON.stringify(models);
        var jsonObjectmodels = JSON.parse(modelsvalue);

         Model.findById({_id : req.query.id}, function (err, model) {
              
             console.log(model._id);

               Model.update(
                 { _id: req.query.id },
                 { $set: { 
                   status: req.query.status} 
              },
                 function (err, doc) {
                    // console.log(doc);        
                 });  

         });

    return res.redirect('/managemodel');
 });

 });


  app.get('/managecategory',isLoggedIn, function(req, res) {

    var i = 0;
       var vm = this;
        vm.category = null;
        vm.allcategorys = [];
        vm.website = null;
        vm.allwebsites = [];

        var Website = require('../app/models/website');

        var Category = require('../app/models/category');

        Category.find(function (err, categorys) {
    
        for(var i = 0; i < categorys.length; i++){
            vm.allcategorys[i] = categorys[i];

        }
 
        var categorysvalue = JSON.stringify(categorys);
        var jsonObjectcategorys = JSON.parse(categorysvalue);

          Website.find(function (err, website) {

        for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }
     
        var productvaluewebsite = JSON.stringify(website);
        var jsonObjectwebsite = JSON.parse(productvaluewebsite); 
     
        res.render('managecategory.ejs', { categorydata: jsonObjectcategorys, loadwebsitedata : jsonObjectwebsite , user : req.user });

      });   
  
    });

  });    


  app.get('/manageunit',isLoggedIn, function(req, res) {

     var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];
        vm.unit = null;
        vm.allunits = [];
        vm.website = null;
        vm.allwebsites = [];

      var Website  = require('../app/models/website');

      var Unit = require('../app/models/unit');

       Unit.find(function (err, units) {
   
        for(var i = 0; i < units.length; i++){
            vm.allunits[i] = units[i];

        }
 
       var unitsvalue = JSON.stringify(units);
       var jsonObjectunits = JSON.parse(unitsvalue);

       Website.find(function (err, website) {

       for(var i = 0; i < website.length; i++){
              vm.allwebsites[i] = website[i];

        }
     
        var productvaluewebsite = JSON.stringify(website);
        var jsonObjectwebsite = JSON.parse(productvaluewebsite); 
    
        res.render('manageunit.ejs', {unitdata: jsonObjectunits, loadwebsitedata : jsonObjectwebsite , user : req.user });

      });

    });    

  });  


  app.get('/managecoupon',isLoggedIn, function(req, res) {
     var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];
        vm.coupon = null;
        vm.allcoupons = [];
        vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');


       var Coupon       = require('../app/models/coupon');


     Coupon.find(function (err, coupon) {
      // docs is an array
        for(var i = 0; i < coupon.length; i++){
            vm.allcoupons[i] = coupon[i];


        }
 
       var couponvalue = JSON.stringify(coupon);
       var jsonObjectcoupon = JSON.parse(couponvalue);

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }
     
       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

    var Consult       = require('../app/models/consults');
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

           //console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
     
        res.render('managecoupon.ejs', { coupondata: jsonObjectcoupon, loadwebsitedata : jsonObjectwebsite ,loadconsultdata: jsonObject, user : req.user ,totalreadconsultstatus : finalstatusconsult,});

      });

    });   
  
  });

  });    


  app.get('/managesize',isLoggedIn, function(req, res) {
     var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];
        vm.size = null;
        vm.allsize = [];


         vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

    var Size       = require('../app/models/size');

     Size.find(function (err, size) {
      // docs is an array
        for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];


        }
 
       var sizevalue = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(sizevalue);

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }
     
       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

    var Consult       = require('../app/models/consults');
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

           //console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
     
        res.render('managesize.ejs', { sizedata: jsonObjectsize, loadwebsitedata : jsonObjectwebsite ,loadconsultdata: jsonObject, user : req.user ,totalreadconsultstatus : finalstatusconsult,});

      });

    });   
  
  });

  });    

   app.get('/managecolor',isLoggedIn, function(req, res) {
     var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];
        vm.color = null;
        vm.allcolor = [];


         vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

    var Color       = require('../app/models/color');

     Color.find(function (err, color) {
      // docs is an array
        for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];


        }
 
       var colorvalue = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(colorvalue);

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }
     
       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

    var Consult       = require('../app/models/consults');
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

           //console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
     
        res.render('managecolor.ejs', { colordata: jsonObjectcolor, loadwebsitedata : jsonObjectwebsite ,loadconsultdata: jsonObject, user : req.user ,totalreadconsultstatus : finalstatusconsult,});

      });

    });   
  
  });

  });    


   app.get('/managesubcategory',isLoggedIn, function(req, res) {

      var i = 0;

        var vm = this;
        vm.category = null;
        vm.allcategorys = [];
        vm.subcategory = null;
        vm.allsubcategorys = [];
        vm.website = null;
        vm.allwebsites = [];

      var Website = require('../app/models/website');

      var Category = require('../app/models/category');

      var SubCategory = require('../app/models/subcategory');

         Category.find(function (err, categorys) {
     
        for(var i = 0; i < categorys.length; i++){
            vm.allcategorys[i] = categorys[i];

        }
 
       var categorysvalue = JSON.stringify(categorys);
       var jsonObjectcategorys = JSON.parse(categorysvalue);

        SubCategory.find(function (err, subcategorys) {
  
        for(var i = 0; i < subcategorys.length; i++){
            vm.allsubcategorys[i] = subcategorys[i];

        }
 
          var subcategorysvalue = JSON.stringify(subcategorys);
          var jsonObjectsubcategorys = JSON.parse(subcategorysvalue);

        Website.find(function (err, website) {

        for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }
     
       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 
     
     
        res.render('managesubcategory.ejs', { subcategorydata : jsonObjectsubcategorys , categorydata: jsonObjectcategorys, loadwebsitedata : jsonObjectwebsite, user : req.user });

        });

      });   
  
    });

  });
   


   app.get('/managesecondsubcategory',isLoggedIn, function(req, res) {

     var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];
        vm.category = null;
        vm.allcategorys = [];
        vm.subcategory = null;
        vm.allsubcategorys = [];
        vm.website = null;
        vm.allwebsites = [];
        vm.secondSubCategory = null;
        vm.allsecondSubCategorys = [];

       var Website = require('../app/models/website');

       var Category = require('../app/models/category');

       var SubCategory  = require('../app/models/subcategory');

       var secondSubCategory  = require('../app/models/secondsubcategory');

        Category.find(function (err, categorys) {
     
        for(var i = 0; i < categorys.length; i++){
            vm.allcategorys[i] = categorys[i];

        }
 
       var categorysvalue = JSON.stringify(categorys);
       var jsonObjectcategorys = JSON.parse(categorysvalue);

        SubCategory.find(function (err, subcategorys) {
      // docs is an array
        for(var i = 0; i < subcategorys.length; i++){
            vm.allsubcategorys[i] = subcategorys[i];


        }
 
       var subcategorysvalue = JSON.stringify(subcategorys);
       var jsonObjectsubcategorys = JSON.parse(subcategorysvalue);

        secondSubCategory.find(function (err, secondSubCategory) {
      // docs is an array
        for(var i = 0; i < secondSubCategory.length; i++){
            vm.allsecondSubCategorys[i] = secondSubCategory[i];


        }
 
       var secondSubCategorysvalue = JSON.stringify(secondSubCategory);
       var jsonObjectsecondSubCategorys = JSON.parse(secondSubCategorysvalue);


        Website.find(function (err, website) {

        for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }
     
        var productvaluewebsite = JSON.stringify(website);
        var jsonObjectwebsite = JSON.parse(productvaluewebsite); 
  
        res.render('managesecondsubcategory.ejs', {secondsubcategorydata : jsonObjectsecondSubCategorys , subcategorydata : jsonObjectsubcategorys , categorydata: jsonObjectcategorys, loadwebsitedata : jsonObjectwebsite , user : req.user });


          });   
  
        });

      });

    }); 

  });    


/////////////////////////////submodel //////////////////////////

   app.get('/managesubmodel',isLoggedIn, function(req, res) {
     var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];
        vm.make = null;
        vm.allmakes = [];
        vm.model = null;
        vm.allmodels = [];
        vm.website = null;
        vm.allwebsites = [];
        vm.submodel = null;
        vm.allsubmodels = [];

     var Website       = require('../app/models/website');

    var Make       = require('../app/models/make');

    var Model       = require('../app/models/model');

   var Submodel       = require('../app/models/submodel');


     Make.find(function (err, makes) {
      // docs is an array
        for(var i = 0; i < makes.length; i++){
            vm.allmakes[i] = makes[i];


        }
 
       var makesvalue = JSON.stringify(makes);
       var jsonObjectmakes = JSON.parse(makesvalue);

        Model.find(function (err, models) {
      // docs is an array
        for(var i = 0; i < models.length; i++){
            vm.allmodels[i] = models[i];


        }
 
       var modelsvalue = JSON.stringify(models);
       var jsonObjectmodels = JSON.parse(modelsvalue);

        Submodel.find(function (err, submodel) {
      // docs is an array
        for(var i = 0; i < submodel.length; i++){
            vm.allsubmodels[i] = submodel[i];


        }
 
       var submodelsvalue = JSON.stringify(submodel);
       var jsonObjectsubmodels = JSON.parse(submodelsvalue);

       // console.log(jsonObjectsubcategorys);

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }
     
       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

    var Consult       = require('../app/models/consults');
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

           //console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
     
        res.render('managesubmodel.ejs', {submodeldata : jsonObjectsubmodels , modeldata : jsonObjectmodels , makedata: jsonObjectmakes, loadwebsitedata : jsonObjectwebsite ,loadconsultdata: jsonObject, user : req.user ,totalreadconsultstatus : finalstatusconsult,});

      });

    });   
  
  });

  });

 }); 

 });    



app.post('/submodel-insert', isLoggedIn, function(req, res) {
   console.log(res);
   var User       = require('../app/models/user');
   var Make       = require('../app/models/make');
   var Model       = require('../app/models/model');
   var SubModel       = require('../app/models/submodel');

   var i = 0;
  var vm = this;
        vm.website = null;
        vm.allwebsites = [];

     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);  

      

         var set = {
              makeId :req.body.makeId,
              modelId : req.body,modelname,
              submodelName :req.body.submodelName,
              status : "0"
             };

           var submodel = new SubModel(set);
           submodel.save(set,
            function (err, doc) {
             //console.log(doc);
            });
        
        
     return res.redirect('managesubmodel');

      });


   });  
  
  app.get('/managethirdsubcategory',isLoggedIn, function(req, res) {

     var i = 0;
       var vm = this;
        vm.category = null;
        vm.allcategorys = [];
        vm.subcategory = null;
        vm.allsubcategorys = [];
        vm.website = null;
        vm.allwebsites = [];
        vm.secondSubCategory = null;
        vm.allsecondSubCategorys = [];
        vm.thirdSubCategory = null;
        vm.allthirdSubCategorys = [];

     var Website = require('../app/models/website');

      var Category = require('../app/models/category');

      var SubCategory = require('../app/models/subcategory');

      var secondSubCategory = require('../app/models/secondsubcategory');

      var thirdSubCategory = require('../app/models/thirdsubcategory');

      Category.find(function (err, categorys) {
     
        for(var i = 0; i < categorys.length; i++){
            vm.allcategorys[i] = categorys[i];


        }
 
       var categorysvalue = JSON.stringify(categorys);
       var jsonObjectcategorys = JSON.parse(categorysvalue);

        SubCategory.find(function (err, subcategorys) {
      // docs is an array
        for(var i = 0; i < subcategorys.length; i++){
            vm.allsubcategorys[i] = subcategorys[i];


        }
 
       var subcategorysvalue = JSON.stringify(subcategorys);
       var jsonObjectsubcategorys = JSON.parse(subcategorysvalue);

        secondSubCategory.find(function (err, secondSubCategory) {
      // docs is an array
        for(var i = 0; i < secondSubCategory.length; i++){
            vm.allsecondSubCategorys[i] = secondSubCategory[i];


        }
 
       var secondSubCategorysvalue = JSON.stringify(secondSubCategory);
       var jsonObjectsecondSubCategorys = JSON.parse(secondSubCategorysvalue);

       // console.log(jsonObjectsubcategorys);
        thirdSubCategory.find(function (err, thirdSubCategory) {
      // docs is an array
        for(var i = 0; i < thirdSubCategory.length; i++){
            vm.allthirdSubCategorys[i] = thirdSubCategory[i];


        }
 
       var thirdSubCategorysvalue = JSON.stringify(thirdSubCategory);
       var jsonObjectthirdSubCategorys = JSON.parse(thirdSubCategorysvalue);


          Website.find(function (err, website) {

          for(var i = 0; i < website.length; i++){
              vm.allwebsites[i] = website[i];

          }
     
          var productvaluewebsite = JSON.stringify(website);
          var jsonObjectwebsite = JSON.parse(productvaluewebsite); 
          
        res.render('managethirdsubcategory.ejs', {thirdsubcategorydata : jsonObjectthirdSubCategorys , secondsubcategorydata : jsonObjectsecondSubCategorys , subcategorydata : jsonObjectsubcategorys , categorydata: jsonObjectcategorys, loadwebsitedata : jsonObjectwebsite , user : req.user });

            });

          });   
    
        });

      });

    }); 

  });    

  
  app.get('/thirdsubcategoryStatus', isLoggedIn, function(req, res) {

   var thirdSubCategory       = require('../app/models/thirdsubcategory');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

         var _id = req.query.id;
         var statusval = req.query.status;

         console.log(_id);
    var i = 0;
    var vm = this;
        vm.thirdSubCategory = null;
        vm.allthirdSubCategorys = [];
   thirdSubCategory.find(function (err, thirdSubCategorys) {
     // docs is an array
       for(var i = 0; i < thirdSubCategorys.length; i++){
            vm.allthirdSubCategorys[i] = thirdSubCategorys[i];
       }
        var thirdSubCategoryvalue = JSON.stringify(thirdSubCategorys);
        var jsonObjectthirdSubCategorys = JSON.parse(thirdSubCategoryvalue);

         thirdSubCategory.findById({_id : req.query.id}, function (err, thirdSubCategorys) {
              
             console.log(thirdSubCategorys._id);

               thirdSubCategory.update(
                 { _id: _id },
                 { $set: { 
                   status: req.query.status} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

    return res.redirect('/managethirdsubcategory');
 });

 }); 



 app.get('/productStatus', isLoggedIn, function(req, res) {
   var product       = require('../app/models/products');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

         var _id = req.query.id;
         var statusval = req.query.productStatus;

         console.log(_id);
    var i = 0;
    var vm = this;
        vm.products = null;
        vm.allproducts = [];
   product.find(function (err, products) {
     // docs is an array
       for(var i = 0; i < products.length; i++){
            vm.allproducts[i] = products[i];
       }
        var productsvalue = JSON.stringify(products);
        var jsonObjectproducts = JSON.parse(productsvalue);

         product.findById({_id : req.query.id}, function (err, products) {
              
             console.log(products._id);

               product.update(
                 { _id: _id },
                 { $set: { 
                   productStatus: req.query.productStatus} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

    return res.redirect('/product-list');
 });

 }); 

  app.get('/status-supplier', isLoggedIn, function(req, res) {
   var product       = require('../app/models/products');
   var Supplier       = require('../app/models/suppliers');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

         var _id = req.query.id;
         var supplier_status = req.query.supplier_status;

         console.log(_id);
    var i = 0;
    var vm = this;
        vm.suppliers = null;
        vm.allsuppliers = [];
   Supplier.find(function (err, suppliers) {
     // docs is an array
       for(var i = 0; i < suppliers.length; i++){
            vm.allsuppliers[i] = suppliers[i];
       }
        var suppliersvalue = JSON.stringify(suppliers);
        var jsonObjectsuppliers = JSON.parse(suppliersvalue);

         Supplier.findById({_id : req.query.id}, function (err, suppliers) {
              
             console.log(suppliers._id);

               Supplier.update(
                 { _id: _id },
                 { $set: { 
                   supplier_status: req.query.supplier_status} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

         });

    return res.redirect('/supplier-list');
 });

 }); 



 app.get('/viewStatus', isLoggedIn, function(req, res) {
   var product       = require('../app/models/products');
   var dates=  new Date();
   var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

         var _id = req.query.id;
         var statusval = req.query.viewStatus;

         console.log(_id);
    var i = 0;
    var vm = this;
        vm.products = null;
        vm.allproducts = [];
   product.find(function (err, products) {
     // docs is an array
       for(var i = 0; i < products.length; i++){
            vm.allproducts[i] = products[i];
       }
        var productsvalue = JSON.stringify(products);
        var jsonObjectproducts = JSON.parse(productsvalue);

         product.findById({_id : req.query.id}, function (err, products) {
              
             console.log(products._id);

               product.update(
                 { _id: _id },
                 { $set: { 
                   viewStatus: req.query.viewStatus} 
              },
                 function (err, doc) {
                    console.log(doc);        
                 });  

          });

        return res.redirect('/product-list');

    });

 }); 


  app.get('/managewebsite',isLoggedIn, function(req, res) {
     var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];
        vm.website = null;
        vm.allwebsites = [];

    var Consult       = require('../app/models/consults');
     var Website       = require('../app/models/website');

        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;

          Website.find(function (err, website) {
        // docs is an array
          for(var i = 0; i < website.length; i++){
              vm.allwebsites[i] = website[i];

          }

         var productvaluewebsite = JSON.stringify(website);
         var jsonObjectwebsite = JSON.parse(productvaluewebsite);
     
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

           //console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
     
        res.render('managewebsite.ejs', {loadwebsitedata : jsonObjectwebsite , loadconsultdata: jsonObject, user : req.user ,totalreadconsultstatus : finalstatusconsult,});

      });

     });   
  
  });

   app.post('/profile-update', isLoggedIn, function(req, res) {
     
    var User  = require('../app/models/user');
    var Website  = require('../app/models/website');
    var dates = new Date();
    var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();
    var _id = req.body.userid;
      var a = 0;
      var vm = this ;
        vm.consult = null;
        vm.allconsults = [];
        vm.allusers = [];
        var answerstatuscount = 0;
        var readstatuscount = 0;
        var totalreadconsultstatus = 0;
     
      Website.find(function (err, website) {
     
        for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

        var productvaluewebsite = JSON.stringify(website);
        var jsonObjectwebsite = JSON.parse(productvaluewebsite);
        var user_role = req.body.user_role;

        User.update({ _id: _id },{ $set: {  

                                          firstName: req.body.firstName.trim(),
                                          companyName : req.body.companyName.trim(),
                                          phone :req.body.phone.trim(),
                                          user_role : req.body.user_role.trim(),
                                          address : req.body.address.trim(),
                                          city : req.body.city.trim(),
                                          state : req.body.state.trim(),
                                          pinCode : req.body.pinCode.trim(),
                                          datetime : strDate

                                        }},function (err, doc) {

                                           console.log(doc);        
                                        });

          setTimeout(() => {

        User.findById(_id, function (err, user) {

          for(var i = 0; i < user.length; i++){

              vm.allusers[i] = user[i];

          }
   
          var uservalue = JSON.stringify(user);
          var jsonObjectuser = JSON.parse(uservalue);

        res.render('account.ejs', {

          loadwebsitedata : jsonObjectwebsite , user : jsonObjectuser , succesproupdate: 'Profile-Updated!!!'

          });

        });

      }, 2000);

    });

  });

 app.post('/website-update', isLoggedIn, function(req, res) {

    var User       = require('../app/models/user');
    var Website       = require('../app/models/website');
    var dates=  new Date();
    var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

    var _id = req.body.userid;
    console.log(_id);
      var a = 0;
      var vm = this ;
        vm.consult = null;
        vm.allconsults = [];
        var answerstatuscount = 0;
        var readstatuscount = 0;
        var totalreadconsultstatus = 0;


      var Consult       = require('../app/models/consults');
   
       Consult.find(function (err, consult) {
        // docs is an array
          for(var a = 0; a < consult.length; a++){

              vm.allconsults[a] = consult[a];
               if(req.user.user_role == 'user'){

                if(req.user._id == consult[a].user_id){

                      var totalreadstatusc = consult[a].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }
          }
          var finalstatusconsult = readstatuscount ;
   
         var productvalueconsult = JSON.stringify(consult);
         var jsonObjectconsult = JSON.parse(productvalueconsult);
   

    var user_role = req.body.user_role;

         Website.findById("5b77c7b025294a06e84b0b2c", function (err, web) {
              
            // console.log(web._id);

         // var sets = {  webName: req.body.webName,
         //                 webEmail :req.body.webEmail,
         //                 webPhone : req.body.webPhone,
         //                 webAddress : req.body.webAddress,
         //                 datetime : strDate};
         //              var website = new Website(sets);
         //              website.save(function(error, dataconsult){
         //                  if(error){
         //                      res.json(error);
         //                  }
         //                  else{
         //                    console.log(dataconsult);  
         //                  }
         //                });

              Website.update(
                  { _id: "5b77c7b025294a06e84b0b2c" },
                  { $set: {  webName: req.body.webName,
                         webEmail :req.body.webEmail,
                         webPhone : req.body.webPhone,
                         webAddress : req.body.webAddress,
                         datetime : strDate} 
               },
                  function (err, web) {
                     console.log(web);        
                  });  

         });

    // res.render('managewebsite.ejs', {
    //   user : req.user , loadconsultdata : jsonObjectconsult, totalreadconsultstatus : finalstatusconsult, succesproupdate: 'Profile-Updated!!!' , loadwebsitedata : web ,
    // });
    return res.redirect("/managewebsite");
  });
 });     

  app.get('/edit-user', isLoggedIn, function(req, res) {

    var User       = require('../app/models/user');
    var Website       = require('../app/models/website');
    var dates=  new Date();
        var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

    var userid = req.query.id;
    //console.log(userid);

    var i = 0;
    var vm = this;
        vm.user = null;
        vm.allusers = [];
        vm.website = null;
        vm.allwebsites = [];

    var user_role = req.body.user_role;

     User.find(function (err, user) {
      // docs is an array
        for(var i = 0; i < user.length; i++){
            vm.allusers[i] = user[i];


        }
 
       var uservalue = JSON.stringify(user);
       var jsonObjectuser = JSON.parse(uservalue);

         Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 


    res.render('edit-user.ejs', {
      user : req.user , data :jsonObjectuser ,loadwebsitedata : jsonObjectwebsite , userid : req.query.id ,
    });

  });

  });       

  });  



  app.post('/updateuser', isLoggedIn, function(req, res) {

var User = require('../app/models/user');
    var Website = require('../app/models/website');

var dates=  new Date();
    var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();
    var _id = req.body.userid;

var i = 0;
var vm = this;
   vm.user = null;
        vm.allusers = [];
         vm.website = null;
        vm.allwebsites = [];

        Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite);

    User.find(function (err, user) {
  // docs is an array
   for(var i = 0; i < user.length; i++){
            vm.allusers[i] = user[i];


        }
 
       var uservalue = JSON.stringify(user);
       var jsonObjectuser = JSON.parse(uservalue);


         User.findById({_id : req.body.userid}, function (err, user) {
       
    // console.log(user);
    // console.log(_id);

    console.log(req.body.username);
         User.update(
           { _id: _id },
           { $set: {  
                          firstName: req.body.firstName,
                          lastName: req.body.lastName,
                          phone :req.body.phone,
                          user_role : req.body.user_role,
                          username : req.body.username,
                          cod:req.body.cod,
                          address : req.body.address,
                          companyName : req.body.companyName,
                          city : req.body.city,
                          state : req.body.state,
                          pinCode : req.body.pinCode,
                          bankDetail : req.body.bankDetail,
                          panNo : req.body.panNo,
                          registerType : req.body.registerType,
                          gsit : req.body.gsit,
                          discount : req.body.discount,
                          registerSet : req.body.registerSet,
                          bankName : req.body.bankName,
                          holderName : req.body.holderName,
                          accountNumber : req.body.accountNumber,
                          ifscCode : req.body.ifscCode,
               datetime : strDate
                       }
    },
           function (err, doc) {
              console.log(doc);        
           });  

   });

res.render('admin.ejs', {
user : req.user , datauser : jsonObjectuser , loadwebsitedata : jsonObjectwebsite ,
});
  });
     
   });

  });      


    var fs =  require('fs');
    var express = require('express');
    var multer = require('multer');
    var upload = multer({ dest: 'public/uploads'});
    app.post('/image-upload', upload.single('avatar'), function(req, res) {
      console.log(req.files);
      var _id = req.user._id;
      // console.log(_id);
      var file = 'public/uploads/' + req.files.avatar.originalFilename;
        // console.log(file);
        var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];
    var User       = require('../app/models/user');
    var Consult       = require('../app/models/consults');
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

         //  console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
      fs.rename(req.files.avatar.path, file, function(err) {
        if (err) {
          console.log(err);
          res.send(500);
        } else {
     var imageName = req.files.avatar.originalFilename;
     
     var imagepath = {};
     imagepath['originalname'] = imageName;

     var set = {img:imagepath};
     var _id = req.user._id;
     console.log(_id);

         User.findById(_id, function (err, user) {
              
              console.log(user);

                 User.update(
                  { _id: _id },
                  { $set: {img:imagepath} 
               },
                  function (err, doc) {
                     console.log(doc); 
        
                  });  

      //  res.render('account.ejs', {
      //   loadconsultdata: jsonObject, user : req.user ,totalreadconsultstatus : finalstatusconsult,
      // });

      


      });


       }

       });
   

    app.get('/account', function (req, res) {

     console.log("logggg");

      res.sendFile( "public/uploads/" + "/" + "account" );

    });

    // res.render('account.ejs', {
    //  loadconsultdata: jsonObject, user : req.user ,totalreadconsultstatus : finalstatusconsult,
    // });

          return res.redirect("account");

  });

  
   });



      var fs =  require('fs');
    var express = require('express');
    var multer = require('multer');
    var upload = multer({ dest: 'public/uploads/logo'});
    app.post('/website-logo-upload', upload.single('avatar'), function(req, res) {
      var _id = req.user._id;
      console.log(_id);
       var Website       = require('../app/models/website');
      var file = 'public/uploads/logo/' + req.files.avatar.originalFilename;
        var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];
    var User       = require('../app/models/user');
    var Consult       = require('../app/models/consults');
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

         //  console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
        //  console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
      fs.rename(req.files.avatar.path, file, function(err) {
        if (err) {
          console.log(err);
          res.send(500);
        } else {
     var imageName = req.files.avatar.originalFilename;
     
     var imagepath = {};
     imagepath['originalname'] = imageName;

     var set = {img:imagepath};
     // var _id = req.user._id;
     // console.log(_id);

         Website.findById("5b77c7b025294a06e84b0b2c", function (err, user) {
      

                 Website.update(
                  { _id: "5b77c7b025294a06e84b0b2c" },
                  { $set: {img:imagepath} 
               },
                  function (err, doc) {
                     console.log(doc); 
        
                  });  


      });


        }

      });


          return res.redirect("managewebsite");

  });

  
   });



  app.get('/checkouts',isLoggedIn, function(req, res) {
  
     var i = 0;
       var vm = this;
        vm.consult = null;
        vm.allconsults = [];

    var Consult       = require('../app/models/consults');
        var readstatuscount = 0; 
        var answerstatuscount = 0;  
        var totalreadconsultstatus = 0 ;
     
       Consult.find(function (err, consult) {
        // docs is an array
          for(var i = 0; i < consult.length; i++){
              vm.allconsults[i] = consult[i];

              if(req.user.user_role == 'user'){

                if(req.user._id == consult[i].user_id){

                      var totalreadstatusc = consult[i].read_status;

                      //console.log(totalreadstatusc);

                      if(totalreadstatusc != '1')
                         {
                        readstatuscount += 1;

                      
                         }
                    }


              }

          }

           console.log(readstatuscount);       

          var finalstatusconsult = readstatuscount ;
          console.log(finalstatusconsult);
         var productvalue = JSON.stringify(consult);
         var jsonObject = JSON.parse(productvalue);
     
        res.render('checkouts.ejs', {newfinalvalcoup :  "0", loadconsultdata: jsonObject, user : req.user ,totalreadconsultstatus : finalstatusconsult,});

      });
    
  });


// //////////////////////thankuguest///////////////////
app.get('/thankyou', isLoggedIn, function(req, res) {
        var i = 0;
       var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.user = null;
        vm.allusers = [];
        vm.product = null;
        vm.allproduct = [];
        vm.slider = null;
        vm.allslider = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.review = null;
        vm.allreviews = [];

     var User       = require('../app/models/user');    

     var Review       = require('../app/models/review');

     var Website       = require('../app/models/website');

      var Slider       = require('../app/models/slider');

      var Category       = require('../app/models/category');

      var Subcategory       = require('../app/models/subcategory');

       var Product       = require('../app/models/products');

       var Size       = require('../app/models/size');

       var Color       = require('../app/models/color');

        Review.find(function (err, review) {

     for(var i = 0; i < review.length; i++){
            vm.allreviews[i] = review[i];

        }
           var productvaluereview = JSON.stringify(review);
       var jsonObjectreview = JSON.parse(productvaluereview);  


         Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 


        Product.find(function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

       var productvalueproduct = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvalueproduct); 


       Product.find({productStatus : "1" , viewStatus : "0"},function (err, productt) {
         
           console.log(productt.length);


         Product.find({addProRole : "manager" ,productStatus : "0" , viewStatus : "0"},function (err, producttdds) {
         
           console.log(producttdds.length);
   

      

      Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 


       Subcategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 


     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

       Slider.find(function (err, slider) {

     for(var i = 0; i < slider.length; i++){
            vm.allslider[i] = slider[i];

        }

       var productvalueslider = JSON.stringify(slider);
       var jsonObjectslider = JSON.parse(productvalueslider);   

    res.render('thankyou.ejs',{ftotalprod : producttdds.length,totalprod : productt.length, newfinalvalcoup : "0" ,user: req.user,loadreviewdata : jsonObjectreview ,loadcolordata:jsonObjectcolor, loadsizedata:jsonObjectsize, loadwebsitedata : jsonObjectwebsite , loadsliderdata : jsonObjectslider , loadcategorydata : jsonObjectcategory , loadsubcategorydata  : jsonObjectsubcategory ,   loadproductdata : jsonObjectproduct , productid : req.query.id});

  });

  });

  });

  });   

   }); 

  });

   });   

   });

   });   

   }); 

  });  


// //////////////////////thankuguest///////////////////

  app.get('/thnks', function(req, res) {
        var i = 0;
       var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.product = null;
        vm.allproduct = [];
        vm.slider = null;
        vm.allslider = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.size = null;
        vm.allsize = [];
        vm.color = null;
        vm.allcolor = [];
        vm.review = null;
        vm.allreviews = [];

     var Review       = require('../app/models/review');

     var Website       = require('../app/models/website');

      var Slider       = require('../app/models/slider');

      var Category       = require('../app/models/category');

      var Subcategory       = require('../app/models/subcategory');

       var Product       = require('../app/models/products');

       var Size       = require('../app/models/size');

       var Color       = require('../app/models/color');

        Review.find(function (err, review) {

     for(var i = 0; i < review.length; i++){
            vm.allreviews[i] = review[i];

        }
           var productvaluereview = JSON.stringify(review);
       var jsonObjectreview = JSON.parse(productvaluereview);  


         Size.find(function (err, size) {

     for(var i = 0; i < size.length; i++){
            vm.allsize[i] = size[i];

        }

       var productvaluesize = JSON.stringify(size);
       var jsonObjectsize = JSON.parse(productvaluesize); 

      Color.find(function (err, color) {

     for(var i = 0; i < color.length; i++){
            vm.allcolor[i] = color[i];

        }

       var productvaluecolor = JSON.stringify(color);
       var jsonObjectcolor = JSON.parse(productvaluecolor); 


        Product.find(function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

       var productvalueproduct = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvalueproduct); 


       Product.find({productStatus : "1" , viewStatus : "0"},function (err, productt) {
         
           console.log(productt.length);


         Product.find({addProRole : "manager" ,productStatus : "0" , viewStatus : "0"},function (err, producttdds) {
         
           console.log(producttdds.length);
   

      Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 


       Subcategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 


     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

       Slider.find(function (err, slider) {

     for(var i = 0; i < slider.length; i++){
            vm.allslider[i] = slider[i];

        }

       var productvalueslider = JSON.stringify(slider);
       var jsonObjectslider = JSON.parse(productvalueslider);   

    res.render('thnks.ejs',{ftotalprod : producttdds.length,totalprod : productt.length,newfinalvalcoup : "0" , loadreviewdata : jsonObjectreview ,loadcolordata:jsonObjectcolor, loadsizedata:jsonObjectsize, loadwebsitedata : jsonObjectwebsite , loadsliderdata : jsonObjectslider , loadcategorydata : jsonObjectcategory , loadsubcategorydata  : jsonObjectsubcategory ,   loadproductdata : jsonObjectproduct , productid : req.query.id});

  });

  });

  });

  });   

   }); 

  });

   });   

   });

   });    


   }); 

  });   



// ///////////////////////////////////////////////////////////

  app.post('/checkout-function', function(req, res) {
 
    var Order = require('../app/models/orders');
    var Product = require('../app/models/products');
    var Orderitem = require('../app/models/orderitem');
    var newvalue = 0;
    var cartarray = req.body.cartvalue;
    var jsonObject = JSON.parse(cartarray);
    var Razorpay = require("razorpay");
    var userid =  req.body.userid;
    var dates = new Date();
    var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

      var set = {  

            userid : req.body.userid,
            shipfname: req.body.shipfname,
            shiplname: req.body.shiplname,
            companyname : req.body.companyname,
            shipemail: req.body.shipemail,
            shipc_email: req.body.shipc_email,
            shipphone: req.body.shipphone,
            country: req.body.country,
            autocomplete: req.body.autocomplete,
            locality: req.body.locality,
            administrative_area_level_1: req.body.administrative_area_level_1,
            postal_code: req.body.postal_code,
            billfname: req.body.billfname,
            billname: req.body.billname,
            companyBillname : req.body.companyBillname,
            billemail: req.body.billemail,
            billc_email: req.body.billc_email,
            billphone : req.body.billphone,
            billaddress: req.body.billaddress,
            billcity: req.body.billcity,
            billstate: req.body.billstate,
            order_note : req.body.order_note,
            gsitno : req.body.gsitno,
            billcode: req.body.billcode,
            hdnCartTotal : req.body.hdnCartTotal,
            o_status :"Pending",
            payment_method: req.body.payment_method,
            datetime: strDate
      };

    var orders = new Order(set);

    orders.save(function(error, data){
 
      if(error){

          res.json(error);

      } else {

        var objectId = data._id; // this will return the id of object inserted
        newvalue = objectId;  

      Order.findOne({'_id': newvalue},function(err,docs){
   
        var i = 0;
        var _id=docs._id;
        var shipfname =docs.shipfname;
        var autocomplete =docs.autocomplete;
        var postal_code = docs.postal_code ;
        var locality = docs.locality ;
        var country = "India" ;
        var administrative_area_level_1 = docs.administrative_area_level_1 ;
        var country = docs.country ;
        var shipphone = docs.shipphone;
        var finalval= newvalue;

        var jsonObject = JSON.parse(cartarray);
   
        var total = 0;

        for(var i = 0; i < jsonObject.length; i++){
       
          var total = total + (jsonObject[i].Price * jsonObject[i].Qty);
       
          var sets = {    
                        orderid : finalval,
                        productid: jsonObject[i].ProductIds,
                        productstocks : jsonObject[i].ProductStocks,
                        productname: jsonObject[i].Product,
                        productprice: jsonObject[i].Price,
                        productqty: jsonObject[i].Qty,
                        datetime: strDate
                   
                    };

          var orderitem = new Orderitem(sets);
         
          orderitem.save(function(error, datas){

              if(error){

                res.json(error);

              } else {
               
                // console.log('This is Else Part ');              
               
              }

            });
 
        }


        for(var n = 0; n < jsonObject.length ; n++){

          var id = jsonObject[n].ProductIds;

            var quantity = jsonObject[n].Qty;
       
          Product.findOne({_id : id}, (err, userfindOne) => {

              var userquantity = userfindOne.quantity;
            
            for(var k = 0; k < n ; k++){      

              var quantitytes = jsonObject[k].Qty;              

              var reqQuatity = parseInt(userquantity) - parseInt(quantitytes);
           
                  var pID =  userfindOne.partId                     

              Product.updateMany({ partId: pID},{ $set: { quantity  : reqQuatity }}, function (err, doc) {
                  
               
             
              });

            }
  
          });

        }

       

        if(req.body.payment_method == "Online"){
        
      

          res.render('razorpay.ejs',{finaldata : docs});

        } else {

      

          return res.redirect("/razorpayupdate?orderid="+newvalue+"&email="+req.body.shipemail);

          }
             
        });

      }

    });

  });

  app.post('/checkoutApi', function(req, res) {
   
    var Order = require('../app/models/orders');
    var Orderitem = require('../app/models/orderitem');
    var Product = require('../app/models/products');
    var newvalue = 0;
    var Razorpay = require("razorpay");
    var userid =  req.body.userid;
    var dates=  new Date();
    var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

      var set = {    
                userid : req.body.userid,
               shipfname: req.body.shipfname,
               shiplname: req.body.shiplname,
               companyname : req.body.companyname,
               shipemail: req.body.shipemail,
               shipc_email: req.body.shipc_email,
               shipphone: req.body.shipphone,
               country: req.body.country,
               autocomplete: req.body.autocomplete,
               locality: req.body.locality,
               administrative_area_level_1: req.body.administrative_area_level_1,
               postal_code: req.body.postal_code,
               billfname: req.body.billfname,
               billname: req.body.billname,
               companyBillname : req.body.companyBillname,
               billemail: req.body.billemail,
               billc_email: req.body.billc_email,
               billphone : req.body.billphone,
               billaddress: req.body.billaddress,
               billcity: req.body.billcity,
               billstate: req.body.billstate,
               billcountry: req.body.billcountry,
               order_note : req.body.order_note,
               billcode: req.body.billcode,
               hdnCartTotal : req.body.hdnCartTotal,
               o_status :"Pending",
               payment_method: req.body.payment_method,
               datetime: strDate
        };  



      var orders = new Order(set);

      orders.save(function(error, data){
          if(error){
              res.json(error);
          }
          else{
           
       
             var objectId = data._id; // this will return the id of object inserted
           newvalue = objectId;  



        Order.findOne({'_id': newvalue},function(err,docs){
        var i = 0;
        var _id=docs._id;
        var shipfname =docs.shipfname;
        var autocomplete =docs.autocomplete;
        var postal_code = docs.postal_code ;
        var locality = docs.locality ;
        var administrative_area_level_1 = docs.administrative_area_level_1 ;
        var country = docs.country ;
        var shipphone = docs.shipphone ;  
        var finalval= newvalue;
       // var cartarrays = ;
       var datas = JSON.parse(JSON.stringify(req.body));
        var datakss = datas.cartarray;
        console.log(datakss.length);
       // var cartvaluee = req.body.cartarray;
       var jsonObject = datakss.length;
       // console.log(jsonObject);
       // console.log(datakss[0].length);
       // console.log(jsonObject.Product);
        var total = 0 ;
                  for(var i = 0; i < datakss.length; i++){

                       var total = total + (datakss[i].Price * datakss[i].Qty);
         
                       // console.log(jsonObject.Product);
           var sets =
                {    
                        orderid : finalval,
                        productid: datakss[i].ProductIds,
                        productstocks : datakss[i].ProductStocks,
                        productname: datakss[i].Product,
                        productprice: datakss[i].Price,
                        productqty: datakss[i].Qty ,
                        datetime: strDate

                    };

        var orderitem = new Orderitem(sets);
        orderitem.save(function(error, datas){
          if(error){
              console.log(error);
          }
          else{
            console.log(datas);
               }
          });
   
         //   return res.redirect('app/#/index' );

       }

       for(var n = 0; n < datakss.length ; n++){

          var id = datakss[n].ProductIds;

            var quantity = datakss[n].Qty;

            var userquantity = datakss[n].ProductQantitys;

               // console.log("id " + id);

               // console.log("quantity " + quantity);

                // console.log("user quantity " + userquantity);

               var reqQuatity = parseInt(userquantity) - parseInt(quantity);

               // console.log(reqQuatity);
           
                Product.update({ _id: datakss[n].ProductIds},{ $set: { quantity  : reqQuatity }}, function (err, doc) {

                 // console.log("doc " + doc);
     
            });

       }      
        // console.log(docs);


          return res.redirect("/razorpaydataupdate?orderid="+newvalue+"&email="+req.body.shipemail);

        
             
          // res.render('razorpay.ejs',{finaldata : docs});

          // return res.redirect("/razorpaydataupdate?orderid="+newvalue);

        });

      }

    });

  });
  
//////////////////////////////////////////////////////////////////////

app.post('/checkoutSubmit', function(req, res) {
  // console.log(req.body);
    var Order       = require('../app/models/orders');
    var Orderitem       = require('../app/models/orderitem');
    var newvalue = 0;
    var jsonObject = JSON.stringify(req.body.cartvalue);
    var Razorpay = require("razorpay");
    var userid =  req.body.userid;
    var dates=  new Date();
    var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();
    var set = {    
               userid : req.body.userid,
               shipfname: req.body.shipfname,
               shiplname: req.body.shiplname,
               companyname : req.body.companyname,
               shipemail: req.body.shipemail,
               shipc_email: req.body.shipc_email,
               shipphone: req.body.shipphone,
               country: req.body.country,
               autocomplete: req.body.autocomplete,
               locality: req.body.locality,
               administrative_area_level_1: req.body.administrative_area_level_1,
               postal_code: req.body.postal_code,
               billfname: req.body.billfname,
               billname: req.body.billname,
               companyBillname : req.body.companyBillname,
               billemail: req.body.billemail,
               billc_email: req.body.billc_email,
               billphone : req.body.billphone,
               billaddress: req.body.billaddress,
               billcity: req.body.billcity,
               billstate: req.body.billstate,
               billcountry: req.body.billcountry,
               order_note : req.body.order_note,
               billcode: req.body.billcode,
               hdnCartTotal : req.body.hdnCartTotal,
               o_status :"Pending",
               payment_method: "RazorPay",
               datetime: strDate
        };  

      var orders = new Order(set);
      orders.save(function(error, data){
          if(error){
              res.json(error);
          }
          else{
       
       var objectId = data._id; // this will return the id of object inserted
           newvalue = objectId;   
        Order.findOne({'_id': newvalue},function(err,docs){
        var i = 0;
        var _id=docs._id;
        var shipfname =docs.shipfname;
        var autocomplete =docs.autocomplete;
        var postal_code = docs.postal_code ;
        var locality = docs.locality ;
        var country = docs.country ;
        var administrative_area_level_1 = docs.administrative_area_level_1 ;
        var country = docs.country ;
        var shipphone = docs.shipphone ;    
        var finalval= newvalue;
        var jsonObject = JSON.stringify(req.body.cartvalue);
        var total = 0 ;
              
              for(var i = 0; i < jsonObject.length; i++){

        var total = total + (jsonObject[i].Price * jsonObject[i].Qty);
         var sets = {    
                        orderid : finalval,
                        productid: jsonObject[i].ProductIds,
                        productstocks : jsonObject[i].ProductStocks,
                        productname: jsonObject[i].Product,
                        productprice: jsonObject[i].Price,
                        productqty: jsonObject[i].Qty ,
                        datetime: strDate
                    };

        var orderitem = new Orderitem(sets);
        orderitem.save(function(error, datas){
          if(error){
              res.json(error);
          }
          else{
            // res.send(datas);
           }
          });

      }

        // res.send({orderdetail : docs});


        // console.log(newvalue);

        return res.redirect("/razorpaydataupdate?orderid="+newvalue);

    });



      }

    });

    });




  // LOGOUT ==============================
  app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
  });

 

// =============================================================================
// AUTHENTICATE (FIRST LOGIN) ==================================================
// =============================================================================

  // locally --------------------------------
    // LOGIN ===============================
    // show the login form
    app.get('/login', function(req, res) {
      res.render('login.ejs', { message: req.flash('loginMessage') , loadwebsitedata : jsonObjectwebsite , });
    });

    // app.get('/wronglogin', function(req, res) {
    //   res.render('wronglogin.ejs', { message: req.flash('loginMessage') });
    // });

    app.get('/razorpay',isLoggedIn, function(req, res) {
      // console.log(req.query.finaldata);
      // console.log("get");
      // console.log(res);
      res.render('razorpay.ejs');
    });

   app.get('/razorpaydataupdate', function(req, res) {
     console.log(req.query.email);
    
     var Order   = require('../app/models/orders');
      var Orderitem   = require('../app/models/orderitem');
                var _id = req.query.orderid;
                 Order.findById(_id, function (err, order) {
                     Order.update(
                        { _id:req.query.orderid },
                        { $set: {paymentId: req.query.paymentId,token : req.query.token,o_status :"Done" } },
                        function (err, doc) {
                              res.send({ status:200 , doc});
                                var aws = require('aws-sdk');
                    aws.config.loadFromPath('newconfig.json');
                    aws.config.correctClockSkew = true
                    var ses = new aws.SES({apiVersion: '2010-12-01'});
                        var to = ['bagapauto@gmail.com',req.query.email]
                        var from = 'info@poycard.com'
                        console.log(to);  
                        ses.sendEmail( 
                          { 
                            Source: from, 
                            Destination: { ToAddresses: to },
                            Message: {
                              Subject: {
                                Data: 'Order confirmation'
                              },
                              Body: {
                                Html: {
                                Data: "<b>"+ "<p>One order is done on website. Order Id is "+req.query.orderid+"</p>" +"</b>" // html body',
                                }
                              }
                            }
                          }).promise();         
                        }); 

     
    });

    });

     app.get('/razorpayupdate',isLoggedIn, function(req, res) {

        // console.log(req.query.email);

        var Order = require('../app/models/orders');
        var Orderitem = require('../app/models/orderitem');
        var _id = req.query.orderid;
        
        Order.findById(_id, function (err, order) {
          
          Order.update({ _id:req.query.orderid },{ $set: {paymentId: req.query.paymentId,token : req.query.token,o_status :"Done" } },
                        function (err, doc) {
                             console.log(doc);
                              var aws = require('aws-sdk');
                    aws.config.loadFromPath('newconfig.json');
                    aws.config.correctClockSkew = true
                    var ses = new aws.SES({apiVersion: '2010-12-01'});
                        var to = ['bagapauto@gmail.com',req.query.email]
                        var from = 'info@poycard.com'
                        console.log(to);  
                        ses.sendEmail( 
                          { 
                            Source: from, 
                            Destination: { ToAddresses: to },
                            Message: {
                              Subject: {
                                Data: 'Order confirmation'
                              },
                              Body: {
                                Html: {
                                Data: "<b>"+ "<p>One order is done on website. Order Id is "+req.query.orderid+"</p>" +"</b>" // html body',
                                }
                              }
                            }
                          }).promise();   
                    }); 

      return res.redirect("/finalsubmit");

    });

  });



 app.get('/finalsubmit',isLoggedIn, function(req, res) {
    var i = 0;
      var vm = this;
        vm.product = null;
        vm.allproduct = [];
        vm.thirdsubcategory = null;
        vm.allthirdsubcategory = [];
        vm.category = null;
        vm.allcategory = [];
        vm.subcategory = null;
        vm.allsubcategory = [];
        vm.secondSubCategory = null;
        vm.allsecondSubCategorys = [];
        vm.productcategory = null;
        vm.allproductcategory = [];
        vm.website = null;
        vm.allwebsites = [];
        var productddata1 = []; 
        var result=[];
        var jsonObjectproduct = [];
        var partId;
        var Product = require('../app/models/products');
        var ProductCategory = require('../app/models/productCategory');
        var thirdSubCategory = require('../app/models/thirdsubcategory');
        var Category = require('../app/models/category');
        var Subcategory = require('../app/models/subcategory');
        var secondSubCategory = require('../app/models/secondsubcategory');
        var Website = require('../app/models/website');

    
         ProductCategory.find(function(err, productCategory){

         Product.find(function(err, product){

        Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

        var productvaluewebsite = JSON.stringify(website);
        var jsonObjectwebsite = JSON.parse(productvaluewebsite);

      Category.find().sort({categoryName: 1}).exec(function(err, category) {      

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

   Subcategory.find().sort({subcategoryName: 1}).exec(function(err, subcategory) {      

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 

  
     secondSubCategory.find().sort({SecondsubcategoryName: 1}).exec(function(err, secondSubCategorys) {      

       for(var i = 0; i < secondSubCategorys.length; i++){
            vm.allsecondSubCategorys[i] = secondSubCategorys[i];
       }
        var secondSubCategoryvalue = JSON.stringify(secondSubCategorys);
        var jsonObjectsecondSubCategorys = JSON.parse(secondSubCategoryvalue);

       thirdSubCategory.find().sort({CategoryName: 1}).exec(function(err, thirdsubcategory) {      


     for(var i = 0; i < thirdsubcategory.length; i++){
            vm.allthirdsubcategory[i] = thirdsubcategory[i];

        }

        var productvaluethirdsubcategory = JSON.stringify(thirdsubcategory);
        var jsonObjectthirdsubcategory = JSON.parse(productvaluethirdsubcategory); 
        
      res.render('finalsubmit.ejs' , { user : req.user , loadwebsitedata : jsonObjectwebsite , secondsubcategorydata : jsonObjectsecondSubCategorys , loadsubcategorydata  : jsonObjectsubcategory ,  loadcategorydata : jsonObjectcategory , loadthirdsubcategory : jsonObjectthirdsubcategory, loadproductdata : product , loadproductcategory : productCategory,});
              
                });

              });

            });   

          });

        });

      }); 

    }); 

  });


  app.get('/stockqantity', isLoggedIn, function(req, res) {
   
    var i = 0;
      var vm = this;

        vm.user = null;
        vm.allusers = [];

        vm.website = null;
        vm.allwebsites = [];

        vm.purchasedata = null;
        vm.allpurchase = [];

        vm.salesitem = null;
        vm.allsalesitem = [];

        vm.salesItemOpen = null;
        vm.allsalesItemOpen = [];

        vm.purchaseOpen = null;
        vm.allpurchaseOpen = [];

        vm.purchaseitem = null;      
        vm.allpurchaseitem = [];  

        vm.purchaseReturnOpen = null;
        vm.allpurchaseReturnOpen = [];

        vm.salesitemreturn = null;
        vm.allsalesitemreturn = [];

        vm.salesReturnOpen = null;
        vm.allsalesReturnOpen = [];    

        var PurchaseItem = require('../app/models/purchaseItem');
        var User = require('../app/models/user');
        var Website = require('../app/models/website');
        var SalesItem  = require('../app/models/salesitem');
        var PurchaseItemReturn = require('../app/models/purchaseItemreturn');
        var SalesItemReturn = require('../app/models/salesitemreturn');

        var partid = req.query.id;

          var today = new Date();

          var curMonth = today.getMonth();

          var year = today.getFullYear();

            if (curMonth >= 3) {

              var nextYr1 = today.getFullYear().toString();
               
              var nextYr4 = nextYr1 +"-"+ "04" +"-"+ "01";
               
            } else {

              var nextYr2 = today.getFullYear().toString() - 1;
               
              var nextYr4 = nextYr2 +"-"+ "04" +"-"+ "01";
               
            }

            PurchaseItem.find({ $and: [{ "part" : partid },{ "datetime": { $lte : nextYr4 }}]}, function(err, purchaseOpen){

              for(var i = 0; i < purchaseOpen.length; i++){
                  vm.allpurchaseOpen[i] = purchaseOpen[i];

              }
     
            var purchaseOpenvalue = JSON.stringify(purchaseOpen);            
            var jsonObjectpurchaseOpen = JSON.parse(purchaseOpenvalue);
           
            PurchaseItemReturn.find({ $and: [{ "part" : partid },{ "datetime": { $lte : nextYr4 }}]}, function(err, purchaseReturnOpen){

              for(var i = 0; i < purchaseReturnOpen.length; i++){
                  vm.allpurchaseReturnOpen[i] = purchaseReturnOpen[i];

              }
     
            var purchaseReturnOpenvalue = JSON.stringify(purchaseReturnOpen);            
            var jsonObjectpurchaseReturnOpenvalue = JSON.parse(purchaseReturnOpenvalue);


            SalesItem.find({ $and: [{ "part" : partid },{ "datetime": { $lte : nextYr4 }}]}, function(err, salesItemOpen){

                for(var i = 0; i < salesItemOpen.length; i++){
                    vm.allsalesItemOpen[i] = salesItemOpen[i];

                }
     
              var salesItemOpenvalue = JSON.stringify(salesItemOpen);            
              var jsonObjectsalesItemOpenvalue = JSON.parse(salesItemOpenvalue);


            SalesItemReturn.find({ $and: [{ "part" : partid },{ "datetime": { $lte : nextYr4 }}]}, function(err, salesReturnOpen){

              for(var i = 0; i < salesReturnOpen.length; i++){
                  vm.allsalesReturnOpen[i] = salesReturnOpen[i];

              }
     
            var salesReturnOpenvalue = JSON.stringify(salesReturnOpen);            
            var jsonObjectsalesReturnOpenvalue = JSON.parse(salesReturnOpenvalue);


            SalesItem.find({"part" : req.query.id}, function (err, salesitem) {
           
              for(var i = 0; i < salesitem.length; i++){
                  vm.allsalesitem[i] = salesitem[i];

              }
     
            var salesitemvalue = JSON.stringify(salesitem);            
            var jsonObjectsalesitem = JSON.parse(salesitemvalue);

            PurchaseItem.find({"part" : req.query.id}, function (err, purchasedata) {

              for(var i = 0; i < purchasedata.length; i++){
                  vm.allpurchase[i] = purchasedata[i];
              }

            var uservalue = JSON.stringify(purchasedata);
            var jsonpurchase = JSON.parse(uservalue);
       
            User.find(function (err, user) {
       
              for(var i = 0; i < user.length; i++){
                  vm.allusers[i] = user[i];
              }

            var uservalue = JSON.stringify(user);
            var jsonObjectuser = JSON.parse(uservalue);

            Website.find(function (err, website) {

              for(var i = 0; i < website.length; i++){
                  vm.allwebsites[i] = website[i];
               
              }

            var productvaluewebsite = JSON.stringify(website);
            var jsonObjectwebsite = JSON.parse(productvaluewebsite);

            PurchaseItemReturn.find({"part" : req.query.id}, function (err, purchaseitem) {
               
              for(var i = 0; i < purchaseitem.length; i++){
                  vm.allpurchaseitem[i] = purchaseitem[i];
         
              }
         
            var purchaseitemvalue = JSON.stringify(purchaseitem);
            var jsonObjectpurchaseitem = JSON.parse(purchaseitemvalue);

            SalesItemReturn.find({"part" : req.query.id}, function (err, salesitemreturn) {
             
              for(var i = 0; i < salesitemreturn.length; i++){
                  vm.allsalesitemreturn[i] = salesitemreturn[i];

              }
           
            var salesitemvalue = JSON.stringify(salesitemreturn);            
            var salesReturn = JSON.parse(salesitemvalue);

          res.render('stockqantity.ejs', { salesRetOpen : jsonObjectsalesReturnOpenvalue , salesOpen : jsonObjectsalesItemOpenvalue , purRetOpen : jsonObjectpurchaseReturnOpenvalue , purOpen : jsonObjectpurchaseOpen, salesReturndata : salesReturn ,purchaseReturn : jsonObjectpurchaseitem, jsonsales : jsonObjectsalesitem, jsonpurchasedata : jsonpurchase , namee : req.query.name, datauser: jsonObjectuser, loadwebsitedata : jsonObjectwebsite , user : req.user, partid : req.query.id});

                      });

                    });    

                  });

                });

              });

            });

          });

        });
     
      });

    });

  });

  app.get('/monthdata', isLoggedIn, function(req, res) {

    var i = 0;
    var vm = this;

    vm.user = null;
    vm.allusers = [];

    vm.salesItemOpen = null;
    vm.allsalesItemOpen = [];

    vm.website = null;
    vm.allwebsites = [];

    vm.purchaseOpen = null;
    vm.allpurchaseOpen = [];
   
    vm.orderitem = null;
    vm.allorderitem = [];

    vm.purchaseReturnOpen = null;
    vm.allpurchaseReturnOpen = [];

    vm.salesReturnOpen = null;
    vm.allsalesReturnOpen = [];

    vm.purOpen = null;      
    vm.allpurOpen = [];  

    vm.purReOpen = null;
    vm.allpurReOpen = [];

    vm.salesOpen = null;
    vm.allsalesOpen = [];

    vm.salesRe = null;
    vm.allsalesRe = [];

    vm.suppliers = null;
    vm.allsuppliers = [];

    var PurchaseItem = require('../app/models/purchaseItem');
    var SalesItem  = require('../app/models/salesitem');
    var PurchaseItemReturn = require('../app/models/purchaseItemreturn');
    var SalesItemReturn = require('../app/models/salesitemreturn');
    var Order = require('../app/models/orders');
    var Product = require('../app/models/products');
    var Orderitem = require('../app/models/orderitem');
    var Website = require('../app/models/website');
    var User = require('../app/models/user');
    var Supplier = require('../app/models/suppliers');

      var partId = req.query.partId;
   
      var monthdata = req.query.firstdate;

      var lastdate = req.query.lastdate;

      Supplier.find(function (err, suppliers) {
          
        for(var i = 0; i < suppliers.length; i++){
            vm.allsuppliers[i] = suppliers[i];
        }

      var suppliersvalue = JSON.stringify(suppliers);
      var jsonObjectsuppliers = JSON.parse(suppliersvalue);

      User.find(function (err, user) {
       
        for(var i = 0; i < user.length; i++){
            vm.allusers[i] = user[i];
        }

      var uservalue = JSON.stringify(user);
      var jsonObjectuser = JSON.parse(uservalue);


      PurchaseItem.find({ $and: [{ part : req.query.partId },{ "datetime": { $lte : req.query.preDate }}]}, function(err, purOpen){

        for(var i = 0; i < purOpen.length; i++){
         
          vm.allpurOpen[i] = purOpen[i];

        }
         
        var purOpenvalue = JSON.stringify(purOpen);                      
        var jsonPur = JSON.parse(purOpenvalue);
             
      PurchaseItemReturn.find({ $and: [{ part : req.query.partId },{ "datetime": { $lte : req.query.preDate }}]}, function(err, purReOpen){
 
        for(var i = 0; i < purReOpen.length; i++){
         
          vm.allpurReOpen[i] = purReOpen[i];

        }
         
        var purRe = JSON.stringify(purReOpen);            
        var jsonPurRe = JSON.parse(purRe);

      SalesItem.find({ $and: [{ part : req.query.partId },{ "datetime": { $lte : req.query.preDate }}]}, function(err, salesOpen){
         
        for(var i = 0; i < salesOpen.length; i++){
         
          vm.allsalesOpen[i] = salesOpen[i];

        }
         
        var salesvalue = JSON.stringify(salesOpen);            
        var jsonsales = JSON.parse(salesvalue);

     
      SalesItemReturn.find({ $and: [{ part : req.query.partId },{ "datetime": { $lte : req.query.preDate }}]}, function(err, salesRe){

         
        for(var i = 0; i < salesRe.length; i++){
                       
          vm.allsalesRe[i] = salesRe[i];

        }
           
        var salesRevalue = JSON.stringify(salesRe);            
        var jsonsalesRe = JSON.parse(salesRevalue);  
       
      PurchaseItem.find({ $and: [{ part : req.query.partId },{ "datetime": { $gte: req.query.firstdate }}, { "datetime": { $lte: req.query.lastdate }}]}).sort({datetime: 1}).exec(function(err, purchaseOpen) {

        for(var i = 0; i < purchaseOpen.length; i++){

          vm.allpurchaseOpen[i] = purchaseOpen[i];

        }
   
          var purchaseOpenvalue = JSON.stringify(purchaseOpen);  

          var jsonObjectpurchaseOpen = JSON.parse(purchaseOpenvalue);

      SalesItem.find({ $and: [{ part : req.query.partId },{ "datetime": { $gte: req.query.firstdate }}, { "datetime": { $lte: req.query.lastdate }}]}).sort({datetime: 1}).exec(function(err, salesItemOpen) {  

        for(var i = 0; i < salesItemOpen.length; i++){
                       
          vm.allsalesItemOpen[i] = salesItemOpen[i];
   
        }

          var salesItemOpenvalue = JSON.stringify(salesItemOpen);            
          var jsonObjectsalesItemOpenvalue = JSON.parse(salesItemOpenvalue);  
                 
      PurchaseItemReturn.find({ $and: [{ part : req.query.partId },{ "datetime": { $gte: req.query.firstdate }}, { "datetime": { $lte: req.query.lastdate }}]}).sort({datetime: 1}).exec(function (err, purchaseReturnOpen) {

        for(var i = 0; i < purchaseReturnOpen.length; i++){
           
          vm.allpurchaseReturnOpen[i] = purchaseReturnOpen[i];

        }

          var purchaseReturnOpenvalue = JSON.stringify(purchaseReturnOpen);            
          var jsonObjectpurchaseReturnOpenvalue = JSON.parse(purchaseReturnOpenvalue);
   
        SalesItemReturn.find({ $and: [{ part : req.query.partId },{ "datetime": { $gte: req.query.firstdate }}, { "datetime": { $lte: req.query.lastdate }}]}).sort({datetime: 1}).exec(function (err, salesReturnOpen) {

            for(var i = 0; i < salesReturnOpen.length; i++){
                     
              vm.allsalesReturnOpen[i] = salesReturnOpen[i];

            }
         
            var salesReturnOpenvalue = JSON.stringify(salesReturnOpen);            
            var jsonObjectsalesReturnOpenvalue = JSON.parse(salesReturnOpenvalue);

        Orderitem.find({ $and: [{ productname : req.query.namee },{ "datetime": { $gte: req.query.firstdate }}, { "datetime": { $lte: req.query.lastdate }}]}, function (err, orderitem) {
           
            for(var i = 0; i < orderitem.length; i++){
                     
              vm.allorderitem[i] = orderitem[i];

            }

            var orderitemvalue = JSON.stringify(orderitem);
            var jsonOrderitem = JSON.parse(orderitemvalue);

        Website.find(function (err, website) {

            for(var i = 0; i < website.length; i++){
                   
              vm.allwebsites[i] = website[i];
               
            }

            var productvaluewebsite = JSON.stringify(website);
            var jsonObjectwebsite = JSON.parse(productvaluewebsite);

        res.render('monthQuantity.ejs', { loadsupplierdata : jsonObjectsuppliers, datauser: jsonObjectuser, salesRet : jsonsalesRe , sales : jsonsales , purchaseRet : jsonPurRe , purchase: jsonPur , loadwebsitedata : jsonObjectwebsite , user : req.user, order : jsonOrderitem, purOpen : jsonObjectpurchaseOpen, salesOpen : jsonObjectsalesItemOpenvalue , purRetOpen : jsonObjectpurchaseReturnOpenvalue , salesRetOpen : jsonObjectsalesReturnOpenvalue , user : req.user, partId : req.query.partId, startD : req.query.firstdate , lastD : lastdate, name : req.query.namee });
                          
                          });

                        });

                      });

                    });

                  });

                });

              });

            });

          });

        });

      });

    });

  });

  app.get('/prodcutInfView', isLoggedIn, function(req, res) {
   
    var i = 0;
      var vm = this;
        vm.order = null;
        vm.allorders = [];
        vm.user = null;
        vm.allusers = [];
        vm.website = null;
        vm.allwebsites = [];

      var Order = require('../app/models/orders');
      var User = require('../app/models/user');
      var Website = require('../app/models/website');

      User.find(function (err, user) {
 
      for(var i = 0; i < user.length; i++){
          vm.allusers[i] = user[i];
      }

      var uservalue = JSON.stringify(user);
      var jsonObjectuser = JSON.parse(uservalue);

      Website.find(function (err, website) {

      for(var i = 0; i < website.length; i++){
         vm.allwebsites[i] = website[i];
   
      }

      var productvaluewebsite = JSON.stringify(website);
      var jsonObjectwebsite = JSON.parse(productvaluewebsite);

      res.render('prodcutInfView.ejs', { datauser: jsonObjectuser, loadwebsitedata : jsonObjectwebsite , user : req.user});

      });

    });    

  });

  app.get('/wronglogin', function(req, res) {
   
    var newmsg = req.flash('loginMessage');

      var i = 0;

       var vm = this;

        vm.website = null;
        vm.allwebsites = [];
        vm.user = null;
        vm.allusers = [];
        vm.product = null;
        vm.allproduct = [];
        vm.slider = null;
        vm.allslider = [];
        vm.category = null;
        vm.allcategory = [];
        vm.thirdsubcategory = null;
        vm.allthirdsubcategory = [];
        var Website       = require('../app/models/website');
        var Slider       = require('../app/models/slider');
        var Category       = require('../app/models/category');
        var User       = require('../app/models/user');

        var thirdSubCategory = require('../app/models/thirdsubcategory');

        var Product = require('../app/models/products');

          Product.find(function (err, product) {

        for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

        var productvalueproduct = JSON.stringify(product);
        var jsonObjectproduct = JSON.parse(productvalueproduct);

          Website.find(function (err, website) {

        for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

        var productvaluewebsite = JSON.stringify(website);
        var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

          Category.find(function (err, category) {

        for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

        var productvaluecategory = JSON.stringify(category);
        var jsonObjectcategory = JSON.parse(productvaluecategory); 

          thirdSubCategory.find(function (err, thirdsubcategory) {

        for(var i = 0; i < thirdsubcategory.length; i++){
            vm.allthirdsubcategory[i] = thirdsubcategory[i];

        }

        var productvaluethirdsubcategory = JSON.stringify(thirdsubcategory);
        var jsonObjectthirdsubcategory = JSON.parse(productvaluethirdsubcategory); 

          Slider.find(function (err, slider) {

        for(var i = 0; i < slider.length; i++){
            vm.allslider[i] = slider[i];

        }

       var productvalueslider = JSON.stringify(slider);
       var jsonObjectslider = JSON.parse(productvalueslider);

          User.find(function (err, user) {

        for(var i = 0; i < user.length; i++){
            vm.alluser[i] = user[i];

        }

       var productvalueuser = JSON.stringify(user);
       var jsonObjectuser = JSON.parse(productvalueuser);


      res.render('wronglogin.ejs', { datauser : jsonObjectuser , messaged : newmsg , loadthirdsubcategory : jsonObjectthirdsubcategory,loadcategorydata : jsonObjectcategory , loadwebsitedata : jsonObjectwebsite ,  loadsliderdata : jsonObjectslider ,loadproductdata : jsonObjectproduct});
  
              });

            });
   
          });      

        });   

      });

    });  

  });      


    // process the login form
    app.post('/login', passport.authenticate('local-login', {
       successRedirect : '/home', // redirect to the secure profile section
       failureRedirect : '/wronglogin', // redirect back to the signup page if there is an error
      failureFlash : true // allow flash messages
    }));



 app.get('/login-stepone', function (req, res , done) {
   // console.log(res);
  var User       = require('../app/models/user');
    
    var username= req.query.user_name;

    var Otpdata = Math.floor(100000 + Math.random() * 900000);

         User.findOne({username : req.query.user_name}, function (err, user) {
              
              // console.log(user);

             if(user == null){

           // req.flash('messaged', 'No User found');
      
             res.send({ status:500 , message : 'No User found'});

             }else{

               var set = { otp: Otpdata,regsitertoken : "" };

                 User.update(
                   { _id : user._id},
                   { $set: set 
                },function (err, doc) {

                   var http = require('http');
   
   http.get("http://api.msg91.com/api/sendhttp.php?country=91&sender=TESTIN&route=4&mobiles="+user.phone+"&authkey=285021AV9QZhdq5d29866b&message=Your OTP is "+Otpdata, function(res) {
   
  // console.log(res.statusCode);
    //this is headers
    // console.log(res.headers);
   res.setEncoding('utf8');
    res.on("data", function(chunk) {
       //this is body
      // console.log(chunk);
  });
}).on('error', function(e) {
   console.log("Got error: " + e.message);


 });


                   res.send({ status:200 , Otpdata});
                       
                    });
                 

                   }

    
                 }); 


});
 

     // process the login form
    app.post('/login-steptwo', function(req, res) {
  
     var User       = require('../app/models/user');

     // console.log(res);

     var otpVal = req.query.Otpdata;

     // console.log(otpVal);


            User.findOne({ 'username' :  req.query.username}, function(err, user) {


                if (user){

                    // console.log(user);

                      if(user.otp == otpVal){

                        // console.log("true");

                        res.status(200).json({status : 200 , loginMessage : user});


                      }else{

                           // console.log("false");

                      res.status(500).json({status : 500 , loginMessage : "OTP is invalid."});


                      }


                } else {

                  
                 res.status(500).json({status : 500 , loginMessage : "No user found."});
     

                }


                
             
            });
                
             
    });


    app.post('/UpdateProfileApi', function(req, res) {

      // console.log(res);

    var User       = require('../app/models/user');
    var dates=  new Date();
    var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();

    var _id = req.body.userid;

         User.findById(_id, function (err, user) {

          if(user){
              User.update(
                  { _id: _id },
                  { $set: {  
                         firstName: req.body.firstName,
                         lastName: req.body.lastName,
                         phone :req.body.phone,
                         // user_role : req.body.user_role,
                         address : req.body.address,
                         datetime : strDate
                      } 
               },
                  function (err, doc) {
                     // console.log(doc);        

          res.status(200).json({status : 200 , loginMessage : "Profile Uploaded Sucessfully !!" , doc});

                  });  


          }else{

            res.status(500).json({status : 500 , loginMessage : "User Not registered !!" });


          }
              
              

          });
   
  });


    var fs =  require('fs');
      var express = require('express');
      var multer = require('multer');
      var uploads = multer({ dest: 'public/uploads/product/'});

  app.post('/partUploadApi', function(req, res) {

    console.log(req.body);

     console.log(req.files.myimage);

    var Product       = require('../app/models/products');
    var ProductCategory       = require('../app/models/productCategory');
    var User       = require('../app/models/user');
    var Category       = require('../app/models/category');
    var SubCategory       = require('../app/models/subcategory');
    var dates=  new Date();
    var strDate = (dates.getMonth()+1)+ "-" + dates.getDate() + "-" + dates.getFullYear();
 
    var token = Math.random();

         var vm = this;
       vm.website = null;
        vm.allwebsites = [];
         vm.category = null;
        vm.allcategory = [];
          vm.subcategory = null;
        vm.allsubcategory = [];

     var Website       = require('../app/models/website');

     Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

      SubCategory.find(function (err, subcategory) {

     for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategory[i] = subcategory[i];

        }

       var productvaluesubcategory = JSON.stringify(subcategory);
       var jsonObjectsubcategory = JSON.parse(productvaluesubcategory); 


     var Website       = require('../app/models/website');

     Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

           var setProductCate = {
            procatName: req.body.procatName,
            prosubcatName: req.body.prosubcatName,
            prosecsubcatName: req.body.prosecsubcatName,
            proparentdata: req.body.proparentdata,
            partId : req.body.partId,
            datetime: dates
        };

       var productCategory = new ProductCategory(setProductCate);
          productCategory.save(setProductCate,
            function (err, doc) {


           if(req.body.addedStatus == "0") {

              
       Product.findOne({partId:req.body.partId}, function (err, productst) {


            console.log(productst.myimage);


       if(req.user.user_role == "admin"){

            var set = {
         
            proCattId : doc._id,
            partId : req.body.partId,
            partName : req.body.partName,
            code: req.body.code,
            mrp: req.body.mrp,
            price:req.body.price,
            stkQty:req.body.stkQty,
            stockLocationOne : req.body.stockLocationOne,
            stockLocationTwo : req.body.stockLocationTwo,
            stockLocationThree : req.body.stockLocationThree,
            tax:req.body.tax,
            brand: req.body.brand,
            umState   : req.body.umState,
            unitMeasure : req.body.unitMeasure,
            altset : req.body.altset,
            firstSet : req.body.firstSet,
            secondSet : req.body.secondSet,
            productdescr : req.body.productdescr,
            datetime: dates,
            myimage: productst.myimage,
            addProRole : req.user.user_role,
            productStatus : "1",
            viewStatus : "1"
        };

       var products = new Product(set);
          products.save(set,
            function (err, doc) {
                console.log(doc);
                console.log(err);
            });


        }else{

            var set = {
            proCattId : doc._id,
            partId : req.body.partId,
            partName : req.body.partName,
            code: req.body.code,
            mrp: req.body.mrp,
            price:req.body.price,
            stkQty:req.body.stkQty,
            stockLocation : req.body.stockLocation,
            tax:req.body.tax,
            brand: req.body.brand,
            productdescr : req.body.productdescr,
            datetime: dates,
            myimage: productst.myimage,
            addProRole : req.user.user_role,
            productStatus : "0",
            viewStatus : "0"
        };

       var products = new Product(set);
          products.save(set,
            function (err, doc) {
                console.log(doc);
                console.log(err);
            });


        }


      });


           } else {




                  var myimage = {} ;


      var file = 'public/uploads/product/' + req.files.myimage.originalFilename;

     if(req.files.myimage.length == undefined){

      var imageName = req.files.myimage.originalFilename;

       var imagePathdata = 'http://poycard.com/uploads/product/'+req.files.myimage.originalFilename;

       var imagepath = {};
       imagepath['originalname'] = imageName;
       imagepath['path'] = imagePathdata;


        if(req.user.user_role == "admin"){

            var set = {
          
            proCattId : doc._id,
            partId : req.body.partId,
            partName : req.body.partName,
            code: req.body.code,
            mrp: req.body.mrp,
            price:req.body.price,
            stkQty:req.body.stkQty,
            stockLocationOne : req.body.stockLocationOne,
            stockLocationTwo : req.body.stockLocationTwo,
            stockLocationThree : req.body.stockLocationThree,
            tax:req.body.tax,
            brand: req.body.brand,
            umState   : req.body.umState,
            unitMeasure : req.body.unitMeasure,
            altset : req.body.altset,
            firstSet : req.body.firstSet,
            secondSet : req.body.secondSet,
            productdescr : req.body.productdescr,
            datetime: dates,
            myimage: imagepath,
            addProRole : req.user.user_role,
            productStatus : "1",
            viewStatus : "1"
        };

       var products = new Product(set);
          products.save(set,
            function (err, doc) {
            });


        }else{

            var set = {
           
            proCattId : doc._id,
            partId : req.body.partId,
            partName : req.body.partName,
            code: req.body.code,
            mrp: req.body.mrp,
            price:req.body.price,
            stkQty:req.body.stkQty,
            stockLocation : req.body.stockLocation,
            tax:req.body.tax,
            brand: req.body.brand,
            productdescr : req.body.productdescr,
            datetime: dates,
            myimage: imagepath,
            addProRole : req.user.user_role,
            productStatus : "0",
            viewStatus : "0"
        };

       var products = new Product(set);
          products.save(set,
            function (err, doc) {
            });


        }


    }else{

      var sub_array = [] ;

   for(var k = 0; k < req.files.myimage.length; k++){

   
      var file = 'public/uploads/product/' + req.files.myimage[k].originalFilename;


      fs.rename(req.files.myimage[k].path, file , function(err) {
        if (err) {
          
          res.send(500);

        } 
       
      });


       var imageName = req.files.myimage[k].originalFilename;

       var imagePathdata = 'http://poycard.com/uploads/product/'+req.files.myimage[k].originalFilename;

       var imagepath = {};
       imagepath['originalname'] = imageName;
       imagepath['path'] = imagePathdata;

        sub_array.push(imagepath);
              
       }    

       if(req.user.user_role == "admin"){

            var set = {
            proCattId : doc._id,
            partId : req.body.partId,
            partName : req.body.partName,
            code: req.body.code,
            mrp: req.body.mrp,
            price:req.body.price,
            stkQty:req.body.stkQty,
            stockLocationOne : req.body.stockLocationOne,
            stockLocationTwo : req.body.stockLocationTwo,
            stockLocationThree : req.body.stockLocationThree,
            tax:req.body.tax,
            brand: req.body.brand,
            umState   : req.body.umState,
            unitMeasure : req.body.unitMeasure,
            altset : req.body.altset,
            firstSet : req.body.firstSet,
            secondSet : req.body.secondSet,
            productdescr : req.body.productdescr,
            datetime: dates,
            myimage: sub_array,
            addProRole : req.user.user_role,
            productStatus : "1",
            viewStatus : "1"
        };

       var products = new Product(set);
          products.save(set,
            function (err, doc) {
            });


        }else{

            var set = {
            proCattId : doc._id,
            partId : req.body.partId,
            partName : req.body.partName,
            code: req.body.code,
            mrp: req.body.mrp,
            price:req.body.price,
            stkQty:req.body.stkQty,
            stockLocation : req.body.stockLocation,
            tax:req.body.tax,
            brand: req.body.brand,
            productdescr : req.body.productdescr,
            datetime: dates,
            myimage: sub_array,
            addProRole : req.user.user_role,
            productStatus : "0",
            viewStatus : "0"
        };

       var products = new Product(set);
          products.save(set,
            function (err, doc) {

            });

          res.status(200).send({status : 200 , msg : "Product Uploaded Sucessfully !!"});


        }


        }


           }  


       

            });

       
    
      });

     });

       });

     });


app.get('/partListApi', function(req, res) {
    var Product       = require('../app/models/products');
    var User       = require('../app/models/user');

         var i = 0;
    var vm = this;
        vm.users = null;
        vm.allusers = [];
        vm.products = null;
        vm.allproducts = [];

 
      Product.find(function (err, products) {
      // docs is an array
        for(var i = 0; i < products.length; i++){
            vm.allproducts[i] = products[i];
       }
       var productvalue = JSON.stringify(products);
        var jsonObject = JSON.parse(productvalue);

       // console.log(jsonObject);

       res.status(200).send({status : 200 , products});


      });


  });


// app.get('/partListbyCatApi', function(req, res) {
//     var Product       = require('../app/models/products');
//     var User       = require('../app/models/user');

//          var i = 0;
//     var vm = this;
//         vm.users = null;
//         vm.allusers = [];
//         vm.products = null;
//         vm.allproducts = [];

 
//       Product.find(function (err, products) {
//       // docs is an array
//         for(var i = 0; i < products.length; i++){
//             vm.allproducts[i] = products[i];
//        }
//        var productvalue = JSON.stringify(products);
//         var jsonObject = JSON.parse(productvalue);

//        // console.log(jsonObject);

//        res.status(200).send({status : 200 , products});


//       });


//   });

   

app.post('/purchaseAddApi', function(req, res) {

  console.log(req.body);


    var Purchase   = require('../app/models/purchase');
    var PurchaseItem   = require('../app/models/purchaseItem');
    var User       = require('../app/models/user');
    var dates=  new Date();
    var strDate = dates.getFullYear() + "-" + (dates.getMonth() < 9 ? '0': '') + (dates.getMonth()+1) + "-" + dates.getDate() ;

            var set = {

               supId    : req.body.supId,
               GrandTotalAmnt : req.body.GrandTotalAmnt,
               SgstTotalAmnt: req.body.SgstTotalAmnt,
               CgstTotalAmnt: req.body.CgstTotalAmnt,
               IgstTotalAmnt: req.body.IgstTotalAmnt,
               FinalTotalAmnt : req.body.FinalTotalAmnt,
               invoiceNo : req.body.invoiceNo,
               voucherNo : req.body.voucherNo,
               SupDate : req.body.SupDate,
               orderId : req.body.orderId,
               purchaseStatus : "0",
               datetime     : strDate

             };

       var purchase = new Purchase(set);
          purchase.save(set,
            function (err, doc) {
               
         for(var n = 0; n < req.body.partId.length; n++){

       var sets = {           

               purchaseId : doc._id, 
               partId   : req.body.partId[n],
               SellPrice : req.body.SellPrice[n],
               TotalAmnt : req.body.TotalAmnt[n],
               Tax     : req.body.Tax[n],
               Discount : req.body.Discount[n],
               Quantity  : req.body.Quantity[n],
               Price   : req.body.Price[n],
               Per     : req.body.Per[n],
               DiscountAmnt : req.body.DiscountAmnt[n],
               Sgst: req.body.Sgst[n],
               Cgst: req.body.Cgst[n],
               Igst: req.body.Igst[n],
               TotalAmntData : req.body.TotalAmntData[n],
               purchaseStatus : "0",
               datetime     : strDate

             };

            var purchaseItem = new PurchaseItem(sets);
             purchaseItem.save(sets,
            function (err, docss) {


            });

                }

            });


       res.status(200).send({status : 200 , msg : "Purchase inserted Sucessfully !!"});


                 
      });

app.get('/purchaseListApi', function(req, res) {
    var Purchase       = require('../app/models/purchase');
        var i = 0;
    var vm = this;
    
        vm.purchase = null;
        vm.allpurchase = [];
        


        Purchase.find(function (err, purchase) {
      // docs is an array
        for(var i = 0; i < purchase.length; i++){
            vm.allpurchase[i] = purchase[i];
       }
       var purchasevalue = JSON.stringify(purchase);
        var jsonObjectpurchase = JSON.parse(purchasevalue);


       res.status(200).send({status : 200 , purchase});

       });

      });

  
app.get('/salesListApi', function(req, res) {
    var Sales       = require('../app/models/sales');
        var i = 0;
    var vm = this;
    
        vm.sales = null;
        vm.allsales = [];
        


        Sales.find(function (err, sales) {
      // docs is an array
        for(var i = 0; i < sales.length; i++){
            vm.allsales[i] = sales[i];
       }
       var salesvalue = JSON.stringify(sales);
        var jsonObjectsales = JSON.parse(salesvalue);


       res.status(200).send({status : 200 , sales});

       });

      });
 
app.get('/supplierListApi', function(req, res) {
    var Supplier       = require('../app/models/suppliers');
        var i = 0;
    var vm = this;
    
        vm.suppliers = null;
        vm.allsuppliers = [];
        


        Supplier.find(function (err, suppliers) {
      // docs is an array
        for(var i = 0; i < suppliers.length; i++){
            vm.allsuppliers[i] = suppliers[i];
       }
       var suppliersvalue = JSON.stringify(suppliers);
        var jsonObjectsuppliers = JSON.parse(suppliersvalue);


       res.status(200).send({status : 200 , suppliers});

       });

      });

  
  app.get('/userListApi', function(req, res) {
    var User       = require('../app/models/user');
        var i = 0;
    var vm = this;
    
        vm.user = null;
        vm.allusers = [];
        


        User.find(function (err, user) {
      // docs is an array
        for(var i = 0; i < user.length; i++){
            vm.allusers[i] = user[i];
       }
       var usersvalue = JSON.stringify(user);
        var jsonObjectuser = JSON.parse(usersvalue);


       res.status(200).send({status : 200 , user});

       });

      });


   app.get('/fetchuserdetailbyId', function(req, res) {
    var User       = require('../app/models/user');
        var i = 0;
    var vm = this;
    
        vm.user = null;
        vm.allusers = [];
        
        var userid = req.query.id;

        User.find({_id : userid},function (err, user) {
         
         if(user){

            res.status(200).send({status : 200 , user});

         } else {

            res.status(500).send({status : 500 , msg : "This User id is not Registered."});


         }




       });

      });



     app.get('/categoryListApi', function(req, res) {
    var Category       = require('../app/models/category');
        var i = 0;
    var vm = this;
    
        vm.category = null;
        vm.allcategorys = [];
        
   Category.find({"status" : 0}).sort({categoryName: 1}).exec(function(err, category) {      
      // docs is an array
        for(var i = 0; i < category.length; i++){
            vm.allcategorys[i] = category[i];
       }
       var categoryvalue = JSON.stringify(category);
        var jsonObjectcategory = JSON.parse(categoryvalue);


       res.status(200).send({status : 200 , category});

       });

      });


     app.post('/fetchsubcatByCatId', function(req, res) {

    var catNamedata = req.body.catname;

    var Subcategory       = require('../app/models/subcategory');
        var i = 0;
    var vm = this;
    
        vm.subcategory = null;
        vm.allsubcategorys = [];
        

   Subcategory.find({ $and: [{categoryId : req.body.catname},{status : "0"}]}).sort({subcategoryName: 1}).exec(function(err, subcategory) {      
      // docs is an array

      // console.log(subcategory);

        for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategorys[i] = subcategory[i];
       }
       var subcategoryvalue = JSON.stringify(subcategory);
        var jsonObjectsubcategory = JSON.parse(subcategoryvalue);


       res.status(200).send({status : 200 , subcategory});

       });

      });


      app.get('/subcategoryListApi', function(req, res) {
    var Subcategory       = require('../app/models/subcategory');
        var i = 0;
    var vm = this;
    
        vm.subcategory = null;
        vm.allsubcategorys = [];
        


        Subcategory.find(function (err, subcategory) {
      // docs is an array
        for(var i = 0; i < subcategory.length; i++){
            vm.allsubcategorys[i] = subcategory[i];
       }
       var subcategoryvalue = JSON.stringify(subcategory);
        var jsonObjectsubcategory = JSON.parse(subcategoryvalue);


       res.status(200).send({status : 200 , subcategory});

       });

      });

    
     app.get('/secondsubcategoryListApi', function(req, res) {
    var Secondsubcategory       = require('../app/models/secondsubcategory');
        var i = 0;
    var vm = this;
    
        vm.secondsubcategory = null;
        vm.allsecondsubcategorys = [];
        


        Secondsubcategory.find(function (err, secondsubcategory) {
      // docs is an array
        for(var i = 0; i < secondsubcategory.length; i++){
            vm.allsecondsubcategorys[i] = secondsubcategory[i];
       }
       var secondsubcategoryvalue = JSON.stringify(secondsubcategory);
        var jsonObjectsecondsubcategory = JSON.parse(secondsubcategoryvalue);


       res.status(200).send({status : 200 , secondsubcategory});

       });

      });


     app.post('/fetchsecondsubcatByCatSubcatId', function(req, res) {
     
     // console.log(res);

    var catNamedata = req.body.catname;
    var subcatnamedata = req.body.subcatname;

    var Secondsubcategory       = require('../app/models/secondsubcategory');
        var i = 0;
    var vm = this;
    
        vm.secondsubcategory = null;
        vm.allsecondsubcategorys = [];


        
       Secondsubcategory.find({ $and: [{categoryId : req.body.catname},{status : "0"},{subcategoryId : req.body.subcatname}]}).sort({SecondsubcategoryName: 1}).exec(function(err, secondsubcategory) {

      // docs is an array
        for(var i = 0; i < secondsubcategory.length; i++){
            vm.allsecondsubcategorys[i] = secondsubcategory[i];
       }
       var secondsubcategoryvalue = JSON.stringify(secondsubcategory);
        var jsonObjectsecondsubcategory = JSON.parse(secondsubcategoryvalue);


       res.status(200).send({status : 200 , secondsubcategory});

       });

      });



 app.get('/thirdsubcategoryListApi', function(req, res) {
    var Thirdsubcategory       = require('../app/models/thirdsubcategory');
        var i = 0;
    var vm = this;
    
        vm.thirdsubcategory = null;
        vm.allthirdsubcategorys = [];
        
        Thirdsubcategory.find(function (err, thirdsubcategory) {
      // docs is an array
        for(var i = 0; i < thirdsubcategory.length; i++){
            vm.allthirdsubcategorys[i] = thirdsubcategory[i];
       }
       var thirdsubcategoryvalue = JSON.stringify(thirdsubcategory);
        var jsonObjectthirdsubcategory = JSON.parse(thirdsubcategoryvalue);


       res.status(200).send({status : 200 , thirdsubcategory});

       });

      });



 app.post('/fetchthirdsubcatByCatSubcatId', function(req, res) {
    
    var catNamedata = req.body.catname;
    var subcatnamedata = req.body.subcatname;
    var secsubcatnamedata = req.body.secondsubcatname;


    var Thirdsubcategory       = require('../app/models/thirdsubcategory');
        var i = 0;
    var vm = this;
    
        vm.thirdsubcategory = null;
        vm.allthirdsubcategorys = [];
              // docs is an array

      Thirdsubcategory.find({ $and: [{catNamedata : req.body.catname},{status : "0"},{subcatNamedata : req.body.subcatname},{secondsubcatNamedata : req.body.secondsubcatname}]}, function(err, thirdsubcategory) {

        for(var i = 0; i < thirdsubcategory.length; i++){
            vm.allthirdsubcategorys[i] = thirdsubcategory[i];
       }
       var thirdsubcategoryvalue = JSON.stringify(thirdsubcategory);
        var jsonObjectthirdsubcategory = JSON.parse(thirdsubcategoryvalue);


       res.status(200).send({status : 200 , thirdsubcategory});

       });

      });



 app.post('/fetchforthsubcatByCatSubcatId', function(req, res) {
    
    var catNamedata = req.body.catname;
    var subcatnamedata = req.body.subcatname;
    var secsubcatnamedata = req.body.secondsubcatname;
    // var thirsubcatnamedata = req.body.thirdsubcatname;


    var Thirdsubcategory       = require('../app/models/thirdsubcategory');
        var i = 0;
    var vm = this;
    
        vm.thirdsubcategory = null;
        vm.allthirdsubcategorys = [];
              // docs is an array

      Thirdsubcategory.find({ $and: [{catNamedata : req.body.catname},{status : "0"},{subcatNamedata : req.body.subcatname},{secondsubcatNamedata : req.body.secondsubcatname},{parentdata : req.body.secondsubcatname}]}).sort({CategoryName: 1}).exec(function(err,thirdsubcategory) {

        for(var i = 0; i < thirdsubcategory.length; i++){
            vm.allthirdsubcategorys[i] = thirdsubcategory[i];
       }
       var thirdsubcategoryvalue = JSON.stringify(thirdsubcategory);
        var jsonObjectthirdsubcategory = JSON.parse(thirdsubcategoryvalue);


       res.status(200).send({status : 200 , thirdsubcategory});

       });

      });


app.post('/fetchfifthsubcatByCatSubcatId', function(req, res) {
    
    var catNamedata = req.body.catname;
    var subcatnamedata = req.body.subcatname;
    var secsubcatnamedata = req.body.secondsubcatname;
    var thirsubcatnamedata = req.body.thirdsubcatname;


    var Thirdsubcategory       = require('../app/models/thirdsubcategory');
        var i = 0;
    var vm = this;
    
        vm.thirdsubcategory = null;
        vm.allthirdsubcategorys = [];
              // docs is an array

   
        Thirdsubcategory.find({ $and: [{catNamedata : req.body.catname},{status : "0"},{subcatNamedata : req.body.subcatname},{secondsubcatNamedata : req.body.secondsubcatname},{parentdata : req.body.thirdsubcatname}]}).sort({CategoryName: 1}).exec(function(err,thirdsubcategory) {

        for(var i = 0; i < thirdsubcategory.length; i++){
            vm.allthirdsubcategorys[i] = thirdsubcategory[i];
       }
       var thirdsubcategoryvalue = JSON.stringify(thirdsubcategory);
        var jsonObjectthirdsubcategory = JSON.parse(thirdsubcategoryvalue);


       res.status(200).send({status : 200 , thirdsubcategory});

       });

      });

app.post('/fetchsixthsubcatByCatSubcatId', function(req, res) {
    
    var catNamedata = req.body.catname;
    var subcatnamedata = req.body.subcatname;
    var secsubcatnamedata = req.body.secondsubcatname;
    var thirsubcatnamedata = req.body.thirdsubcatname;
    var foursubcatnamedata = req.body.fourthsubcatname;


    var Thirdsubcategory       = require('../app/models/thirdsubcategory');
        var i = 0;
    var vm = this;
    
        vm.thirdsubcategory = null;
        vm.allthirdsubcategorys = [];
              // docs is an array

      Thirdsubcategory.find({ $and: [{catNamedata : req.body.catname},{status : "0"},{subcatNamedata : req.body.subcatname},{secondsubcatNamedata : req.body.secondsubcatname},{parentdata : req.body.fourthsubcatname}]}).sort({CategoryName: 1}).exec(function(err,thirdsubcategory) {
        for(var i = 0; i < thirdsubcategory.length; i++){
            vm.allthirdsubcategorys[i] = thirdsubcategory[i];
       }
       var thirdsubcategoryvalue = JSON.stringify(thirdsubcategory);
        var jsonObjectthirdsubcategory = JSON.parse(thirdsubcategoryvalue);


       res.status(200).send({status : 200 , thirdsubcategory});

       });

      });



 app.get('/unitListApi', function(req, res) {
    var Unit       = require('../app/models/unit');
        var i = 0;
    var vm = this;
    
        vm.unit = null;
        vm.allunits = [];
        
        Unit.find(function (err, unit) {
      // docs is an array
        for(var i = 0; i < unit.length; i++){
            vm.allunits[i] = unit[i];
       }
       var unitvalue = JSON.stringify(unit);
        var jsonObjectunit = JSON.parse(unitvalue);


       res.status(200).send({status : 200 , unit});

       });

      });


app.get('/orderListApi', function(req, res) {
    var Order       = require('../app/models/orders');
        var i = 0;
    var vm = this;
    
        vm.order = null;
        vm.allorders = [];
        
        Order.find(function (err, orders) {
      // docs is an array
        for(var i = 0; i < orders.length; i++){
            vm.allorders[i] = orders[i];
       }
       var ordervalue = JSON.stringify(orders);
        var jsonObjectorder = JSON.parse(ordervalue);


       res.status(200).send({status : 200 , orders});

       });

      });



 var fs =  require('fs');
    var express = require('express');
    var multer = require('multer');
    var upload = multer({ dest: 'public/uploads'});
    app.post('/profileImageUploadApi', upload.single('avatar'), function(req, res) {
         console.log(req.query);
         console.log(req.files);
            var User       = require('../app/models/user');

      var _id = req.query.userid;
      console.log(_id);
      var file = 'public/uploads/' + req.files.avatar.originalFilename;
        var i = 0;
   

      fs.rename(req.files.avatar.path, file, function(err) {
        if (err) {

    
    res.status(500).json({ status : 500 , message : "Not Uploaded Image" });
         

        } else {
     var imageName = req.files.avatar.originalFilename;
     var imagePathdata = 'http://poycard.com/uploads/'+req.files.avatar.originalFilename;
     
     var imagepath = {};
     imagepath['originalname'] = imageName;
     imagepath['path'] = imagePathdata;

     var set = {img:imagepath};
     var _id = req.query.userid;
     // console.log(_id);

         User.findById({_id : req.query.userid}, function (err, user) {
              
              console.log(user);

                 User.update(
                  { _id : req.query.userid },
                  { $set: {img:imagepath} 
               },
                  function (err, doc) {
                     // console.log(doc); 

       res.status(200).json({ status : 200 , message : "Image Uploaded Sucessfully !!" , path : imagePathdata});

        
                  });  
      });

        }

      });
   
  });




    app.post('/purchasedateSortApi', function(req, res) {
    var Purchase   = require('../app/models/purchase');
    var PurchaseItem   = require('../app/models/purchaseItem');
    var i = 0;
    var vm = this;
        vm.purchase = null;
        vm.allpurchase = [];

    Purchase.find({$or: [{"datetime": {$gte: req.body.startDate, $lte: req.body.endDate}}]},function (err, purchase) {

 
        res.status(200).json({ status : 200 , purchase});

            });
                              
      });

    app.post('/salesdateSortApi', function(req, res) {
     var Sales   = require('../app/models/sales');
    var SalesItem   = require('../app/models/salesitem');
    var i = 0;
    var vm = this;
        vm.sales = null;
        vm.allsales = [];

    Sales.find({$or: [{"datetime": {$gte: req.body.startDate, $lte: req.body.endDate}}]},function (err, sales) {


        res.status(200).json({ status : 200 , sales});

                                        
           });
                        
        });


   app.post('/orderbyId', function(req, res) {

    console.log(res);

   
  var Order       = require('../app/models/orders');

  Order.find({userid:req.query.userid}, function (err, orders){

    console.log(orders);

    if (err) {

    res.status(500).json({ status :500, error: "Failed to get all order"});

    } else { 
    
    res.status(200).json({ status :200, orders});


    }

  });


});


 app.post('/orderdetailbyId', function(req, res) {
   
  var Order       = require('../app/models/orders');
  var Orderitem       = require('../app/models/orderitem');

  Orderitem.find({orderid:req.query.orderid}, function (err, orderitem){

    console.log(orderitem);

    if (err) {

        res.status(500).json({ status :500, error: "Failed to get all product of order"});


    } else { 
    
     res.status(200).json({ status :200, orderitem});

    }

  });


});


 app.post('/purchasedetailbyId', function(req, res) {

  console.log(req.body.purchaseid);
   
  var Purchase       = require('../app/models/purchase');

  Purchase.find({"_id":req.body.purchaseid}, function (err, purchase){

    console.log(purchase);

    if (err) {

        res.status(500).json({ status :500, error: "Failed to get all product of order"});


    } else { 
    
     res.status(200).json({ status :200, purchase});

    }

  });

});


 app.post('/salesdetailbyId', function(req, res) {

  console.log(req.body.salesid);
   
  var Sales       = require('../app/models/sales');

  Sales.find({"_id":req.body.salesid}, function (err, sales){

    console.log(sales);

    if (err) {

        res.status(500).json({ status :500, error: "Failed to get all product of order"});


    } else { 
    
     res.status(200).json({ status :200, sales});

    }

  });


});

  

    app.post('/registerfacebook', function(req, res) {
   
          var User       = require('../app/models/user'); 

         User.findOne({ $or: [{'local.email': req.body.email},{'google.email': req.body.email},{'facebook.email': req.body.email}]}, function(err, existingUser) {
             
           if(existingUser){

        
                res.status(200).json({ status :200, existingUser});


               } else {

                res.status(500).json({ status :500, error: "Sorry , User not registered..Please consult the website !!!"});

                //res.send({ status : 500 , err : 'Sorry , User not registered..Please consult the website !!!'});  

               }


           });           
   
  });  


    // SIGNUP =================================
    // show the signup form
    app.get('/signup', function(req, res) {
      res.render('signup.ejs', { message: req.flash('signupMessage') });
    });

    app.get('/wrongsignup', function(req, res) {
      var i = 0;
       var vm = this;
        vm.website = null;
        vm.allwebsites = [];
        vm.product = null;
        vm.allproduct = [];
        vm.slider = null;
        vm.allslider = [];
        vm.category = null;
        vm.allcategory = [];
        vm.thirdsubcategory = null;
        vm.allthirdsubcategory = [];
        var Website       = require('../app/models/website');
        var Slider       = require('../app/models/slider');
        var Category       = require('../app/models/category');

      var thirdSubCategory       = require('../app/models/thirdsubcategory');

      var Product       = require('../app/models/products');

         Product.find(function (err, product) {

     for(var i = 0; i < product.length; i++){
            vm.allproduct[i] = product[i];

        }

       var productvalueproduct = JSON.stringify(product);
       var jsonObjectproduct = JSON.parse(productvalueproduct);


      Website.find(function (err, website) {

     for(var i = 0; i < website.length; i++){
            vm.allwebsites[i] = website[i];

        }

       var productvaluewebsite = JSON.stringify(website);
       var jsonObjectwebsite = JSON.parse(productvaluewebsite); 

         Category.find(function (err, category) {

     for(var i = 0; i < category.length; i++){
            vm.allcategory[i] = category[i];

        }

       var productvaluecategory = JSON.stringify(category);
       var jsonObjectcategory = JSON.parse(productvaluecategory); 

        thirdSubCategory.find(function (err, thirdsubcategory) {

     for(var i = 0; i < thirdsubcategory.length; i++){
            vm.allthirdsubcategory[i] = thirdsubcategory[i];

        }

       var productvaluethirdsubcategory = JSON.stringify(thirdsubcategory);
       var jsonObjectthirdsubcategory = JSON.parse(productvaluethirdsubcategory); 

      Slider.find(function (err, slider) {

     for(var i = 0; i < slider.length; i++){
            vm.allslider[i] = slider[i];

        }

       var productvalueslider = JSON.stringify(slider);
       var jsonObjectslider = JSON.parse(productvalueslider);

      res.render('wrongsignup.ejs', {loadthirdsubcategory : jsonObjectthirdsubcategory,loadcategorydata : jsonObjectcategory , message: req.flash('signupMessage') , loadwebsitedata : jsonObjectwebsite ,  loadsliderdata : jsonObjectslider ,loadproductdata : jsonObjectproduct});
    });

   });
   
   });      

   });   

   });

 });        

    // process the signup form
    app.post('/signup', passport.authenticate('local-signup', {
      successRedirect : '/', // redirect to the secure profile section
      failureRedirect : '/wrongsignup', // redirect back to the signup page if there is an error
      failureFlash : true // allow flash messages
    }));

  // facebook -------------------------------


      // send to facebook to do the authentication
    app.get('/auth/facebook', passport.authenticate('facebook', { scope : ['email'] }));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
      passport.authenticate('facebook', {
        successRedirect : '/home',
        failureRedirect : '/'
      }));



  // twitter --------------------------------

    // send to twitter to do the authentication
    app.get('/auth/twitter', passport.authenticate('twitter', { scope : 'email' }));

    // handle the callback after twitter has authenticated the user
    app.get('/auth/twitter/callback',
      passport.authenticate('twitter', {
        successRedirect : '/profile',
        failureRedirect : '/'
      }));


  // google ---------------------------------

    // send to google to do the authentication
    app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));

    // the callback after google has authenticated the user
    app.get('/auth/google/callback',
      passport.authenticate('google', {
        successRedirect : '/home',
        failureRedirect : '/'
      }));

// =============================================================================
// AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
// =============================================================================

  // locally --------------------------------
    app.get('/connect/local', function(req, res) {
      res.render('connect-local.ejs', { message: req.flash('loginMessage') });
    });
    app.post('/connect/local', passport.authenticate('local-signup', {
      successRedirect : '/home', // redirect to the secure profile section
      failureRedirect : '/connect/local', // redirect back to the signup page if there is an error
      failureFlash : true // allow flash messages
    }));

  // facebook -------------------------------

    // send to facebook to do the authentication
    app.get('/connect/facebook', passport.authorize('facebook', { scope : 'email' }));

    // handle the callback after facebook has authorized the user
    app.get('/connect/facebook/callback',
      passport.authorize('facebook', {
        successRedirect : '/home',
        failureRedirect : '/'
      }));

  // twitter --------------------------------

    // send to twitter to do the authentication
    app.get('/connect/twitter', passport.authorize('twitter', { scope : 'email' }));

    // handle the callback after twitter has authorized the user
    app.get('/connect/twitter/callback',
      passport.authorize('twitter', {
        successRedirect : '/profile',
        failureRedirect : '/'
      }));


  // google ---------------------------------

    // send to google to do the authentication
    app.get('/connect/google', passport.authorize('google', { scope : ['profile', 'email'] }));

    // the callback after google has authorized the user
    app.get('/connect/google/callback',
      passport.authorize('google', {
        successRedirect : '/home',
        failureRedirect : '/'
      }));

// =============================================================================
// UNLINK ACCOUNTS =============================================================
// =============================================================================
// used to unlink accounts. for social accounts, just remove the token
// for local account, remove email and password
// user account will stay active in case they want to reconnect in the future

  // local -----------------------------------
  app.get('/unlink/local', function(req, res) {
    var user            = req.user;
    user.local.email    = undefined;
    user.local.password = undefined;
    user.save(function(err) {
      res.redirect('/profile');
    });
  });

  // facebook -------------------------------
  app.get('/unlink/facebook', function(req, res) {
    var user            = req.user;
    user.facebook.token = undefined;
    user.save(function(err) {
      res.redirect('/home');
    });
  });

  // twitter --------------------------------
  app.get('/unlink/twitter', function(req, res) {
    var user           = req.user;
    user.twitter.token = undefined;
    user.save(function(err) {
      res.redirect('/profile');
    });
  });

  // google ---------------------------------
  app.get('/unlink/google', function(req, res) {
    var user          = req.user;
    user.google.token = undefined;
    user.save(function(err) {
      res.redirect('/home');
    });
  });


};

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
  if (req.isAuthenticated())
    return next();

  res.redirect('/');
}