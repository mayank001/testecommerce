// load all the things we need
var LocalStrategy    = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy  = require('passport-twitter').Strategy;
var GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;

// load up the user model
var User       = require('../app/models/user');

var Addsymptoms       = require('../app/models/addsymptom');

var Addsymptomsfemales       = require('../app/models/addsymptomfemale');

var Product       = require('../app/models/products');

var Order       = require('../app/models/orders');

var Orderitem       = require('../app/models/orderitem');

var Consult       = require('../app/models/consults');

var Transactions       = require('../app/models/transaction');

var UserSubscription       = require('../app/models/usersubscription');

var Symptom       = require('../app/models/symptoms');

var Refund       = require('../app/models/refunds');

var Consultreplys       = require('../app/models/consultreply');

var md5 = require('md5');



// load the auth variables
var configAuth = require('./auth'); // use this one for testing

module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'username',
        passwordField : 'ottp',
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, username, ottp, done) {

        // asynchronous

        console.log(ottp);
        console.log(username);

        process.nextTick(function() {

            User.findOne({ 'username' :  username }, function(err, user) {
                // if there are any errors, return the error
                console.log(user.otp);

                console.log(ottp);

                if (!user){
                    return done(null, false, req.flash('loginMessage', 'No user found.'));                   
                }

                if (user.regsitertoken == ""){

                        if(user.first_time_login === 'True'){

                         return done(null, false, req.flash('loginMessage', 'Admin cannot activate your account.'));

                        } else {

                            if (err)
                                return done(null, false, req.flash('loginMessage', 'No user found.'));

                            // if no user is found, return the message
                            if (!user)
                                return done(null, false, req.flash('loginMessage', 'No user found.'));

                            if (user.otp != ottp)
                                return done(null, false, req.flash('loginMessage', 'OTP not verified'));

                            // all is well, return user
                            else
                                return done(null, user);

                             }

                }else{

                     return done(null, false, req.flash('loginMessage', 'First Confirm your Email through your account.'));
 

                }



                


                
             
            });

        });

    }));


    //    passport.use('android-local-login', new LocalStrategy({
    //     // by default, local strategy uses username and password, we will override with email
    //     usernameField : 'email',
    //     passwordField : 'password',
    //     passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    // },
    // function(req, email, password, done) {

    //     // asynchronous

    //      // asynchronous

    //     process.nextTick(function() {

    //         User.findOne({ 'local.email' :  email }, function(err, user) {
    //             // if there are any errors, return the error
    //            // console.log(user);

    //             if (!user){
    //                 return done(null, false, req.flash('loginMessage', 'No user found.'));                   
    //             }


    //             if(user.user_role === 'health-provider'){

    //                 if(user.first_time_login === 'True'){

    //                  return done(null, false, req.flash('loginMessage', 'Admin cannot activate your account.'));

    //                 } else {

    //                         if (err)
    //                             return done(null, false, req.flash('loginMessage', 'No user found.'));

    //                         // if no user is found, return the message
    //                         if (!user)
    //                             return done(null, false, req.flash('loginMessage', 'No user found.'));

    //                         if (!user.validPassword(password))
    //                             return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));

    //                         // all is well, return user
    //                         else
    //                             return done(null, user);

    //                 }

    //             } else {

    //             // if no user is found, return the message
    //             if (!user)
    //                 return done(null, false, req.flash('loginMessage', 'No user found.'));                   

    //             if (!user.validPassword(password))
    //                 return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));

    //             // all is well, return user
    //             else
    //                 return done(null, user);

    //             }
             
    //         });

    //     });
    // }));

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
      //  user_roleField : 'user_role',
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, email, password, done) {
      //  console.log(req.body.password);
      //  console.log(req.body.cpassword);
        // asynchronous
        if(req.body.password === req.body.cpassword){

            process.nextTick(function() {

            //  Whether we're signing up or connecting an account, we'll need
            //  to know if the email address is in use.
            User.findOne({ $or: [{'local.email': email},{'google.email' : email},{'facebook.email' : email}]}, function(err, existingUser) {
                // if there are any errors, return the error
            //   console.log(existingUser);

                if (err)
                    return done(err);
                  //  console.log("err");
                    //console.log(email);        

                // check to see if there's already a user with that email
                if (existingUser) 
                    return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                   // console.log("existingUser");
                //  If we're logged in, we're connecting a new local account.
                if(req.user) {
                   // console.log(req.user);
                    var user            = req.user;
                    user.user_role   = req.body.user_role;
                    user.firstName   = req.body.firstName;
                    user.local.email    = email;
                    user.local.password = md5(password);
                    user.regsitertoken = req.body.regsitertoken;
                    user.img.originalname = "usericon.jpg";
                    user.licenseimg.originalname = "new-brunswick-drivers-licence.jpg";
                    user.first_time_login = req.body.first_time_login;
                    user.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, user);
                       // console.log("user");
                    });
                } 
                //  We're not logged in, so we're creating a brand new user.
                else {
                    // create the user
                    var newUser            = new User();
                    newUser.user_role      = req.body.user_role;
                    newUser.firstName      = req.body.firstName;
                    newUser.local.email    = email;
               //     bcrypt.hashSync("bacon")
                    newUser.local.password =  md5(password);
                    newUser.regsitertoken = req.body.regsitertoken;
                    newUser.img.originalname = "usericon.jpg";
                    newUser.licenseimg.originalname = "new-brunswick-drivers-licence.jpg";
                    newUser.first_time_login = req.body.first_time_login;


                    newUser.save(function(err) {
                     //   console.log(newUser);
                        if (err)
                           // console.log(err);
                            throw err;


                        return done(null, newUser);
                       // console.log("newUser");
                
                    });
                }

            });
        });

        } else {
         
       return done(null, false, req.flash('signupMessage', 'Password Not Match !!!'));

        }
        

    }));

    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({

        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL,
        profileFields   : configAuth.facebookAuth.profileFields,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, refreshToken, profile, done) {

        // asynchronous
        process.nextTick(function() {

            // check if the user is already logged in
            if (!req.user) {

                User.findOne({ $or: [{'local.email': profile.emails[0].value},{'google.email': profile.emails[0].value},{ 'facebook.id' : profile.id }]}, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        // if there is a user id already but no token (user was linked at one point and then removed)
                        if (!user.facebook.token) {
                            user.facebook.token = token;
                            user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                            user.facebook.email = profile.emails[0].value;

                            user.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });
                        }

                        return done(null, user); // user found, return that user
                    } else {
                        // if there is no user, create them
                        var newUser            = new User();

                        newUser.facebook.id    = profile.id;
                        newUser.facebook.token = token;
                        newUser.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                        newUser.facebook.email = profile.emails[0].value;

                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });

            } else {
                // user already exists and is logged in, we have to link accounts
                var user            = req.user; // pull the user out of the session

                user.facebook.id    = profile.id;
                user.facebook.token = token;
                user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                user.facebook.email = profile.emails[0].value;

                user.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, user);
                });

            }
        });

    }));

    // =========================================================================
    // TWITTER =================================================================
    // =========================================================================
    // passport.use(new TwitterStrategy({

    //     consumerKey     : configAuth.twitterAuth.consumerKey,
    //     consumerSecret  : configAuth.twitterAuth.consumerSecret,
    //     callbackURL     : configAuth.twitterAuth.callbackURL,
    //     passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    // },
    // function(req, token, tokenSecret, profile, done) {

    //     // asynchronous
    //     process.nextTick(function() {

    //         // check if the user is already logged in
    //         if (!req.user) {

    //             User.findOne({ 'twitter.id' : profile.id }, function(err, user) {
    //                 if (err)
    //                     return done(err);

    //                 if (user) {
    //                     // if there is a user id already but no token (user was linked at one point and then removed)
    //                     if (!user.twitter.token) {
    //                         user.twitter.token       = token;
    //                         user.twitter.username    = profile.username;
    //                         user.twitter.displayName = profile.displayName;

    //                         user.save(function(err) {
    //                             if (err)
    //                                 throw err;
    //                             return done(null, user);
    //                         });
    //                     }

    //                     return done(null, user); // user found, return that user
    //                 } else {
    //                     // if there is no user, create them
    //                     var newUser                 = new User();

    //                     newUser.twitter.id          = profile.id;
    //                     newUser.twitter.token       = token;
    //                     newUser.twitter.username    = profile.username;
    //                     newUser.twitter.displayName = profile.displayName;

    //                     newUser.save(function(err) {
    //                         if (err)
    //                             throw err;
    //                         return done(null, newUser);
    //                     });
    //                 }
    //             });

    //         } else {
    //             // user already exists and is logged in, we have to link accounts
    //             var user                 = req.user; // pull the user out of the session

    //             user.twitter.id          = profile.id;
    //             user.twitter.token       = token;
    //             user.twitter.username    = profile.username;
    //             user.twitter.displayName = profile.displayName;

    //             user.save(function(err) {
    //                 if (err)
    //                     throw err;
    //                 return done(null, user);
    //             });
    //         }

    //     });

    // }));

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy({

        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, refreshToken, profile, done) {

        // asynchronous
        process.nextTick(function() {

            // check if the user is already logged in
            if (!req.user) {

      //  User.findOne({'google.id' : profile.id }, function(err, user) {
 
        User.findOne({ $or: [{'local.email': profile.emails[0].value},{ 'google.id' : profile.id },{ 'facebook.email' : profile.emails[0].value }]}, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        // if there is a user id already but no token (user was linked at one point and then removed)
                        if (!user.google.token) {
                            user.google.token = token;
                            user.google.name  = profile.displayName;
                            user.google.email = profile.emails[0].value; // pull the first email

                            user.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });
                        }

                        return done(null, user);
                    } else {
                        var newUser          = new User();

                        newUser.google.id    = profile.id;
                        newUser.google.token = token;
                        newUser.google.name  = profile.displayName;
                        newUser.google.email = profile.emails[0].value; // pull the first email

                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });

            } else {
                // user already exists and is logged in, we have to link accounts
                var user               = req.user; // pull the user out of the session

                user.google.id    = profile.id;
                user.google.token = token;
                user.google.name  = profile.displayName;
                user.google.email = profile.emails[0].value; // pull the first email

                user.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, user);
                });

            }

        });

    }));

};
